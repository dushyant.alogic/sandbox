<?php
require dirname(__DIR__) . '/app/bootstrap.php';

$bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $_SERVER);
$_objectManager = $bootstrap->getObjectManager();
$state = $_objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('fronend');
$registry = $_objectManager->get('Magento\Framework\Registry');
$registry->register('isSecureArea', true);

//Store id of exported products, This is useful when we have multiple stores.
$store_id = 1;



$fp = fopen("products_AUS.csv", "w+");
$collection = $_objectManager
        ->create('Magento\Catalog\Model\ResourceModel\Product\CollectionFactory')
        ->create()
        //->addAttributeToFilter('type_id', array('neq' => 'configurable'))
        ->addAttributeToSelect('*')     
        ->setOrder('created_at', 'desc')
        ->load();

fputcsv($fp,
        [
                'id',
                'sku',
                'type_id',

        ]);
foreach ($collection as $product) {
        $data = array();
        $data[] = $product->getId();
        $data[] = $product->getSku();
        $data[] = $product->getTypeId();
        fputcsv($fp, $data);

}
fclose($fp);
?>

