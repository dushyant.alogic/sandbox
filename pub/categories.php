<?php
// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=orderset.csv');

require dirname(__DIR__) . '/app/bootstrap.php';

$bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $_SERVER);
$_objectManager = $bootstrap->getObjectManager();
$state = $_objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');
$registry = $_objectManager->get('Magento\Framework\Registry');
$registry->register('isSecureArea', true);



$orderset= $_objectManager->get('Magento\Sales\Model\ResourceModel\Order\CollectionFactory');
//Store id of exported products, This is useful when we have multiple stores.
$storeId = 35;

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');



$collection = $orderset->create()
         ->addAttributeToSelect('*')->addFieldToFilter('store_id', $storeId);

fputcsv($output ,
        [
                'order_id',
                'sku',
                'product_name',
                'total',
                'city',
                'country'

        ]);
foreach ($collection as $order) {
        foreach ($order->getAllVisibleItems() as $item) {
            $data = [];
            $city = 'N/A';
            $data[] = $order->getIncrementId();
            $data[] = $item->getSku();
            $data[] = $item->getName();
            $data[] = $order->getGrandTotal();
            if($billingAddress = $order->getBillingAddress()){
                $city = $billingAddress->getCity();
                $countrycode = $billingAddress->getData("country_id");
                $country = $_objectManager->create('\Magento\Directory\Model\Country')->load($countrycode)->getName();

            }
            $data[] = $city;
            $data[] = $country;
            fputcsv($output, $data);
        }
}

