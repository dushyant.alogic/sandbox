<?php

namespace StripeIntegration\Payments\Test\Integration\Mock\Events;

class PaymentIntentSucceeded extends AbstractEvent
{
    public function getPaymentIntentSucceeded($totalAmount, $captureAmount, $order)
    {
        $paymentIntentId = $order->getPayment()->getLastTransId();
        $orderIncrementId = $order->getIncrementId();
        $refundedAmount = $totalAmount - $captureAmount;

        return $this->object('payment_intent.succeeded', '{
{
    "id": "pi_1JEuuKHLyfDWKHBq3Ni5rSbF",
    "object": "payment_intent",
    "amount": '.$totalAmount.',
    "amount_capturable": 0,
    "amount_received": '.$captureAmount.',
    "application": null,
    "application_fee_amount": null,
    "canceled_at": null,
    "cancellation_reason": null,
    "capture_method": "manual",
    "charges": {
      "object": "list",
      "data": [
        {
          "id": "ch_1JEuuLHLyfDWKHBqT4KmX2NC",
          "object": "charge",
          "amount": '.$totalAmount.',
          "amount_captured": '.$captureAmount.',
          "amount_refunded": '.$refundedAmount.',
          "application": null,
          "application_fee": null,
          "application_fee_amount": null,
          "balance_transaction": "txn_1JEuw1HLyfDWKHBqq3B0Komt",
          "billing_details": {
            "address": {
              "city": null,
              "country": null,
              "line1": null,
              "line2": null,
              "postal_code": null,
              "state": null
            },
            "email": null,
            "name": null,
            "phone": null
          },
          "calculated_statement_descriptor": "Stripe",
          "captured": true,
          "created": 1626695113,
          "currency": "usd",
          "customer": "cus_JsgGfvESumZvuD",
          "description": "Order #'.$orderIncrementId.' by Flint Jerry",
          "destination": null,
          "dispute": null,
          "disputed": false,
          "failure_code": null,
          "failure_message": null,
          "fraud_details": {
          },
          "invoice": null,
          "livemode": false,
          "metadata": {
            "Module": "Magento2 v{{VERSION}}",
            "Order #": "'.$orderIncrementId.'",
            "Guest": "Yes"
          },
          "on_behalf_of": null,
          "order": null,
          "outcome": {
            "network_status": "approved_by_network",
            "reason": null,
            "risk_level": "normal",
            "risk_score": 31,
            "seller_message": "Payment complete.",
            "type": "authorized"
          },
          "paid": true,
          "payment_intent": "pi_1JEuuKHLyfDWKHBq3Ni5rSbF",
          "payment_method": "pm_1JEuuJHLyfDWKHBqBgavNmWi",
          "payment_method_details": {
            "card": {
              "brand": "visa",
              "checks": {
                "address_line1_check": null,
                "address_postal_code_check": null,
                "cvc_check": null
              },
              "country": "US",
              "exp_month": 7,
              "exp_year": 2022,
              "fingerprint": "ynxnOeYNwAaUJJjA",
              "funding": "credit",
              "installments": null,
              "last4": "4242",
              "network": "visa",
              "three_d_secure": null,
              "wallet": null
            },
            "type": "card"
          },
          "receipt_email": null,
          "receipt_number": null,
          "receipt_url": "https://pay.stripe.com/receipts/acct_1Ig7MJHLyfDWKHBq/ch_1JEuuLHLyfDWKHBqT4KmX2NC/rcpt_JsgG9SGmGkoRYpXl4EXTIkcL77GQmVl",
          "refunded": false,
          "refunds": {
            "object": "list",
            "data": [
              {
                "id": "re_1JEuw1HLyfDWKHBqSbmfZob3",
                "object": "refund",
                "amount": '.$refundedAmount.',
                "balance_transaction": "txn_1JEuw1HLyfDWKHBqT3srJonT",
                "charge": "ch_1JEuuLHLyfDWKHBqT4KmX2NC",
                "created": 1626695217,
                "currency": "usd",
                "metadata": {
                },
                "payment_intent": "pi_1JEuuKHLyfDWKHBq3Ni5rSbF",
                "reason": null,
                "receipt_number": null,
                "source_transfer_reversal": null,
                "status": "succeeded",
                "transfer_reversal": null
              }
            ],
            "has_more": false,
            "total_count": 1,
            "url": "/v1/charges/ch_1JEuuLHLyfDWKHBqT4KmX2NC/refunds"
          },
          "review": null,
          "shipping": {
            "address": {
              "city": "New York",
              "country": "US",
              "line1": "1255 Duncan Avenue",
              "line2": null,
              "postal_code": "10013",
              "state": "Armed Forces Middle East"
            },
            "carrier": null,
            "name": "Flint Jerry",
            "phone": "917-535-4022",
            "tracking_number": null
          },
          "source": null,
          "source_transfer": null,
          "statement_descriptor": null,
          "statement_descriptor_suffix": null,
          "status": "succeeded",
          "transfer_data": null,
          "transfer_group": null
        }
      ],
      "has_more": false,
      "total_count": 1,
      "url": "/v1/charges?payment_intent=pi_1JEuuKHLyfDWKHBq3Ni5rSbF"
    },
    "client_secret": "pi_1JEuuKHLyfDWKHBq3Ni5rSbF_secret_DVcde0JuLlojbyAtMZQQStbYJ",
    "confirmation_method": "manual",
    "created": 1626695112,
    "currency": "usd",
    "customer": "cus_JsgGfvESumZvuD",
    "description": "Order #'.$orderIncrementId.' by Flint Jerry",
    "invoice": null,
    "last_payment_error": null,
    "livemode": false,
    "metadata": {
      "Module": "Magento2 v{{VERSION}}",
      "Order #": "'.$orderIncrementId.'",
      "Guest": "Yes"
    },
    "next_action": null,
    "on_behalf_of": null,
    "payment_method": "pm_1JEuuJHLyfDWKHBqBgavNmWi",
    "payment_method_options": {
      "card": {
        "installments": null,
        "network": null,
        "request_three_d_secure": "automatic"
      }
    },
    "payment_method_types": [
      "card"
    ],
    "receipt_email": null,
    "review": null,
    "setup_future_usage": "on_session",
    "shipping": {
      "address": {
        "city": "New York",
        "country": "US",
        "line1": "1255 Duncan Avenue",
        "line2": null,
        "postal_code": "10013",
        "state": "Armed Forces Middle East"
      },
      "carrier": null,
      "name": "Flint Jerry",
      "phone": "917-535-4022",
      "tracking_number": null
    },
    "source": null,
    "statement_descriptor": null,
    "statement_descriptor_suffix": null,
    "status": "succeeded",
    "transfer_data": null,
    "transfer_group": null
  }');
    }
}
