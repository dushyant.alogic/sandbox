<?php
/**
 * Copyright © 2020 Biztech . All rights reserved.
 */

namespace Biztech\Geoip\Model;

class Analyze extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG = 'geoip_analyze';

	protected $_cacheTag = 'geoip_analyze';

	protected $_eventPrefix = 'geoip_analyze';

	protected function _construct()
	{
		$this->_init('Biztech\Geoip\Model\ResourceModel\Analyze');
	}
	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}
	public function getDefaultValues()
	{
		$values = [];
		return $values;
	}
}
