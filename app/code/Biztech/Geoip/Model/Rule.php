<?php
/**
 * Copyright © 2020 Biztech . All rights reserved.
 */

namespace Biztech\Geoip\Model;

class Rule extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG = 'geoip_rule';

	protected $_cacheTag = 'geoip_rule';

	protected $_eventPrefix = 'geoip_rule';

	protected function _construct()
	{
		$this->_init('Biztech\Geoip\Model\ResourceModel\Rule');
	}
	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}
	public function getDefaultValues()
	{
		$values = [];
		return $values;
	}
}
