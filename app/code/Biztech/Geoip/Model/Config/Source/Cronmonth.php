<?php
/** 
 * Copyright © 2020 Biztech . All rights reserved. 
 **/

namespace Biztech\Geoip\Model\Config\Source;

class Cronmonth implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        $data = [];
        for ($i = 1; $i <= 31; ++$i) {
            $data[] = ['value' => $i, 'label' => $i];
        }
        return $data;
    }
}
