<?php
/**
 * Copyright © 2020 Biztech . All rights reserved.
 */
 
namespace Biztech\Geoip\Model\Config\Source\Rule;

use Biztech\Geoip\Helper\Data;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

class Store implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var array
     */
    protected $_options;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

 
    public function __construct(
        StoreManagerInterface $storeManager,
        Data $helper,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->helper = $helper;
        $this->_storeManager = $storeManager;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    { 
        $websites = $this->helper->getAllWebsites();
        foreach ($this->_storeManager->getWebsites() as $website) {
            $storeView = [];
            foreach ($website->getGroups() as $group) {
                $stores = $group->getStores();
                foreach ($stores as $store) {
                    if ($store && in_array($store->getId(), $websites)) {
                        $id = $store->getId();
                        $name = $store->getName();
                        $storeView[] = ['value' => $id, 'label' => __($name)];
                    }
                }
            }
            if (!empty($storeView)) {
                $this->_options[] = [
                    'label' => __($website->getName()),
                    'value' => $storeView
                ];
            }
        }
        return $this->_options;
    }
}
