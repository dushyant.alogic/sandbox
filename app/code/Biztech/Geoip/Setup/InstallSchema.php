<?php
/**
 * Copyright © 2020 Biztech . All rights reserved.
 */

namespace Biztech\Geoip\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Biztech\Geoip\Helper\GeoipDb;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{

    /**
     * @var $_geoIpHelper; 
     */
    private $_geoIpHelper;

    /**
     * InstallSchema constructor.
     * @param GeoipDb $geoIpHelper
     */
    public function __construct(
        GeoipDb $geoIpHelper
    ) {
        $this->_geoIpHelper = $geoIpHelper;
    }

     /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {

        $installer = $setup;

        $installer->startSetup();

        /* Install Setup No Initial Tables */

        /**
         *  Create table 'geoip_rule'
         */
        if (!$installer->tableExists('geoip_rule')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('geoip_rule')
            )
                    ->addColumn('rule_id', Table::TYPE_SMALLINT, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true])
                    ->addColumn('rule_store_id', Table::TYPE_SMALLINT, null, ['nullable' => false, 'unsigned' => true])
                    ->addColumn('country', Table::TYPE_TEXT, null, ['nullable' => true])
                    ->addColumn('priority', Table::TYPE_SMALLINT, null, ['nullable' => false, 'default' => '3'])
                    ->addColumn('status', Table::TYPE_SMALLINT, null, ['nullable' => false, 'default' => '0'])
                    ->addColumn('currency', Table::TYPE_TEXT, null, ['nullable' => false])
                    ->addColumn('created_at',Table::TYPE_TIMESTAMP,null,['nullable' => false, 'default' => Table::TIMESTAMP_INIT])
                    ->addColumn('updated_at',Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE])
                    ->addForeignKey(
                        $installer->getFkName('geoip_rule', 'rule_store_id', 'store', 'store_id'),
                        'rule_store_id',
                        $installer->getTable('store'),
                        'store_id',
                        Table::ACTION_CASCADE
                    )
                    ->setComment(
                        'Biztech GeoIP rull will be added. Table: geoip_rule'
                    );
            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
    /**
     * Download GeoIP database
     *
     * @return $this
     */
    private function downloadDatabase()
    {
        try {
            $this->_geoIpHelper->downloadDatabase();
        } catch (\Exception $e) {}
        return $this;
    }
}
