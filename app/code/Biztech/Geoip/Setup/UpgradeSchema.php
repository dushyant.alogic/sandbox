<?php
/**
 * Copyright © 2020 Biztech . All rights reserved.
 */

namespace Biztech\Geoip\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Biztech\Geoip\Helper\GeoipDb;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        /**
         * update table 'geoip_rule'
         */
        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            if ($installer->tableExists('geoip_rule')) {
                $tableName = $installer->getTable('geoip_rule');

                $columnName = 'ip_exception';
                if ($installer->getConnection()->tableColumnExists($tableName, $columnName) === false) {
                    $installer->getConnection()->addColumn(
                        $tableName,
                        $columnName,
                        ['type' => Table::TYPE_TEXT,
                        'nullable' => true,
                        'default' => null,
                        'comment' => 'Exceptional IP']
                    );
                }

                $columnName = 'url_exception';
                if ($installer->getConnection()->tableColumnExists($tableName, $columnName) === false) {
                    $installer->getConnection()->addColumn(
                        $tableName,
                        $columnName,
                        ['type' => Table::TYPE_TEXT,
                        'nullable' => true,
                        'default' => null,
                        'comment' => 'Exceptional URL']
                    );
                }
            }
            /**
             *  Create table 'geoip_analyze'
             */
            if (!$installer->tableExists('geoip_analyze')) {
                $table = $installer->getConnection()->newTable(
                    $installer->getTable('geoip_analyze')
                )
                ->addColumn('analyze_id', Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true])
                ->addColumn('analyze_rule_id', Table::TYPE_SMALLINT, null, ['nullable' => true])
                ->addColumn('visitor_country', Table::TYPE_TEXT, null, ['nullable' => false])
                ->addColumn('visitor_ip', Table::TYPE_BIGINT, null, ['nullable' => false, 'default' => '0'])
                ->addColumn('redirect_to', Table::TYPE_TEXT, null, ['nullable' => false])
                ->addColumn('blocked_request', Table::TYPE_SMALLINT, null, ['nullable' => false, 'default' => '0'])
                ->addColumn('created_at',Table::TYPE_TIMESTAMP,null,['nullable' => false, 'default' => Table::TIMESTAMP_INIT])
                ->setComment(
                    'Biztech GeoIP Analyze data will be stored. Table: geoip_analyze'
                );
                $installer->getConnection()->createTable($table);
            }
        }
        $setup->endSetup();
    }
}
