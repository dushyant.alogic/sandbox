<?php
/**
 * Copyright © 2020 Biztech . All rights reserved.
 */

namespace Biztech\Geoip\Block\Adminhtml\Rule\Edit;

class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * @var $_priority
     */
    protected $_priority;
    /**
     * @var $_status
     */
    protected $_status;
    /**
     * @var $_country
     */
    protected $_country;
    /**
     * @var $_store
     */
    protected $_store; /**
    
    * @var $_store
    */
    protected $_currency;
    
    public function __construct(
    \Magento\Backend\Block\Template\Context $context,
    \Magento\Framework\Registry $registry,
    \Magento\Framework\Data\FormFactory $formFactory,
    \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
    \Biztech\Geoip\Model\Config\Source\Rule\Priority $priority,
    \Biztech\Geoip\Model\Config\Source\Rule\Status $status,
    \Magento\Directory\Model\Config\Source\Country $country,
    \Biztech\Geoip\Model\Config\Source\Rule\Store $store,
    \Magento\Config\Model\Config\Source\Locale\Currency $currency,
    array $data = []
    ) {
        $this->_priority = $priority;
        $this->_status = $status;
        $this->_country = $country;
        $this->_store = $store;
        $this->_currency = $currency;
        $this->_wysiwygConfig = $wysiwygConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }

     protected function _prepareForm() {
        $dateFormat = $this->_localeDate->getDateFormat(\IntlDateFormatter::SHORT);
        $model = $this->_coreRegistry->registry('row_data');
        $form = $this->_formFactory->create(
                ['data' => [
                        'id' => 'edit_form',
                        'enctype' => 'multipart/form-data',
                        'action' => $this->getData('action'),
                        'method' => 'post'
                    ]
                ]
        );

        $form->setHtmlIdPrefix('wkgrid_');
        if ($model->getRuleId()) {
            $fieldset = $form->addFieldset(
                    'base_fieldset', ['legend' => __('Edit GeoIP Rule'), 'class' => 'fieldset-wide']
            );
            $fieldset->addField('rule_id', 'hidden', ['name' => 'rule_id']);
        } else {
            $fieldset = $form->addFieldset(
                    'base_fieldset', ['legend' => __('Add GeoIP Rule'), 'class' => 'fieldset-wide']
            );
        }

        $fieldset->addField(
            'country', 'multiselect', [
            'name' => 'country',
            'label' => __('Country'),
            'required' => true,
            'id' => 'country',
            'title' => __('Country'),
            'values' => $this->_country->toOptionArray(true)
            ]
        );
        $fieldset->addField(
            'rule_store_id', 'select', [
            'name' => 'rule_store_id',
            'label' => __('Store'),
            'required' => true,
            'id' => 'rule_store_id',
            'title' => __('Store'),
            'values' => $this->_store->toOptionArray(true)
            ]
        );
        $fieldset->addField(
            'currency', 'select', [
            'name' => 'currency',
            'label' => __('Currency'),
            'id' => 'currency',
            'title' => __('Currency'),
            'values' => $this->_currency->toOptionArray()
            ]
        );
        $fieldset->addField(
            'ip_exception', 'textarea', [
            'name' => 'ip_exception',
            'label' => __('Exceptional IP'),
            'id' => 'ip_exception',
            'title' => __('Exceptional IP'),
            'note' => 'Specify just one IP address in a line, second in new line.'
            ]
        );
        $fieldset->addField(
            'url_exception', 'textarea', [
            'name' => 'url_exception',
            'label' => __('Exceptional URL'),
            'id' => 'url_exception',
            'title' => __('Exceptional URL'),
            'note' => 'Specify just one URL address in a line, second in new line.Write URL path except base URL (Like: contact,customer/account/create).'
            ]
        );
        $fieldset->addField(
            'priority', 'select', [
            'name' => 'priority',
            'label' => __('Priority'),
            'id' => 'priority',
            'title' => __('Priority'),
            'values' => $this->_priority->toOptionArray()
            ]
        );
        $fieldset->addField(
                'status', 'select', [
            'name' => 'status',
            'label' => __('Enable Rule'),
            'id' => 'status',
            'title' => __('Enable Rule'),
            'values' => $this->_status->toOptionArray()
            ]
        );
        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }

}
