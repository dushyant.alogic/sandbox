<?php

namespace Biztech\Geoip\Block\Adminhtml;

use \Magento\Framework\View\Element\Template\Context;
use \Biztech\Geoip\Helper\Data;
use \Biztech\Geoip\Helper\GeoipDb;
use Magento\Store\Model\StoreManagerInterface;

class Popuppreview extends \Magento\Framework\View\Element\Template
{

	/**
     * @var Biztech\Geoip\Helper\Data
     */
    protected $_dataHelper;

   /**
     * @var Biztech\Geoip\Helper\GeoipDb
     */
    protected $_geoipHelper;

    /**
    * @var \Magento\Store\Model\StoreManagerInterface
    */
    protected $_storeManager;

    /**
     * @param Context                  $context
     * @param Data                     $helperData
     * @param array                    $data
	 * @param StoreManagerInterface	   $store
     */
    public function __construct(
        Context $context,
        Data $helper,
        GeoipDb $geoipHelper,
        StoreManagerInterface $store,
        array $data = []
    ) {
        $this->_dataHelper = $helper;
        $this->_geoipHelper = $geoipHelper;

        $this->_storeManager = $store;
        parent::__construct($context, $data);
    }
    /**
     * Get the Store name in which auto store switch action.
     *
     */
    public function getGeoDetectedStoreName()
    {     
        $locationData = $this->_geoipHelper->getCountryLoction();
        if(!isset($locationData['country']['iso_code'])) return;;
        $locationCountryCode = $locationData['country']['iso_code'];
        $getStoreCurrencyData = $this->_geoipHelper->getStoreCurrency($locationCountryCode);
        if(empty($getStoreCurrencyData)) return;
        foreach ($getStoreCurrencyData as $key => $value) {
           $store_id = $value['store_id'];
           $currency = $value['currency_code'];
        }
        return $this->_storeManager->getStore($store_id)->getName();
    }

    /**
     * Get popup title.
     */
    public function getTitle()
    {
        $title = $this->_dataHelper->pupupTitle();
        if($title == '' || $title == null) {
            return 'Redirect Store?';
        } else {
            return $title;
        }
    }

    /**
     * Get popup message.
     */
    public function getMessage()
    {
        $message = $this->_dataHelper->pupupMessage();
        if($message == '' || $message == null) {
            return 'Based on your location, you should redirect to other store.
                    Click below to get redirected.';
        } else {
            return $message;
        }
    }

    /**
     * Get popup button text.
     */
    public function getButtonText()
    {
        $button = $this->_dataHelper->pupupButtonText();
        if($button == '' || $button == null) {
            return 'Redirect';
        } else {
            return $button;
        }
    }

    /**
     * is Manual store Swich enable.
     */
    public function manualstoreSwich()
    {
        return $this->_dataHelper->isManualstoreSwich();   
    }

    /**
     * Get redirect back url.
     */
    public function getBackUrl()
    {
        return $this->getUrl('adminhtml/system_config/edit');
    }

    /**
     * Get configure style url.
     */
    public function configureStyleUrl()
    {
        return $this->getUrl('geoip/style/configure');
    }

}
