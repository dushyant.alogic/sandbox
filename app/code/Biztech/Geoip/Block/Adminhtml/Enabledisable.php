<?php

namespace Biztech\Geoip\Block\Adminhtml;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class Enabledisable extends Field
{

    const XML_PATH_ACTIVATION = 'geoip/activation/key';

    protected $_scopeConfig;
    protected $_helper;
    protected $_resourceConfig;
    protected $_web;
    protected $_store;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Biztech\Geoip\Helper\Data $helper,
        \Magento\Config\Model\ResourceModel\Config $resourceConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\Website $web,
        \Magento\Store\Model\Store $store,
        array $data = []
    ) {
        $this->_helper = $helper;
        $this->storeManager = $storeManager;
        $this->_web = $web;
        $this->_resourceConfig = $resourceConfig;
        $this->_store = $store;
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        $websites = $this->_helper->getAllWebsites();
        if (!empty($websites)) {
            $websiteId = $this->getRequest()->getParam('website', 0);
            if ($websiteId === 0) {
                $storeid = $this->getRequest()->getParam('store', 0);
                if ($storeid === 0) {
                    return $html = $element->getElementHtml();
                }
                $store = $this->_store->load($storeid);
                if ($store && in_array($store->getStoreId(), $websites)) {
                    $html = $element->getElementHtml();
                } else {
                    $this->_resourceConfig->saveConfig('geoip/general/active',0,\Magento\Store\Model\ScopeInterface::SCOPE_STORE,$storeid);
                    $html = '<strong class="required" style="color:red;">' . __('Please select website from the activation tab, If website is not listed then Please buy an additional domains') . '</strong>';
                    return $html;
                }
            } else {
                $website = $this->_web->load($websiteId);
                foreach ($website->getGroups() as $group) {
                    $stores = $group->getStores();
                    foreach ($stores as $store) {
                        if (in_array($store->getStoreId(), $websites)) {
                            $html = $element->getElementHtml();
                            return $html;
                        }
                    }
                }
                $this->_resourceConfig->saveConfig('geoip/general/active',0,\Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE,$websiteId);
                $html = '<strong class="required" style="color:red;">' . __('Please select website from the activation tab, If website is not listed then Please buy an additional domains') . '</strong>';
                return $html;
            }
        } else {
            $isSetKey = $this->_scopeConfig->getValue(
            self::XML_PATH_ACTIVATION,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
            if ($isSetKey != null || $isSetKey != '') {
                $html = sprintf('<strong class="required" style="color:red;">%s</strong>', __('Please select website from the activation tab, If website is not listed then Please buy an additional domains.'));
                 $this->_resourceConfig->saveConfig('geoip/general/active', 0, 'default', 0);
            } else {
                $html = sprintf('<strong class="required" style="color:red;">%s</strong>', __('Please enter a valid key'));
                $this->_resourceConfig->saveConfig('geoip/general/active', 0, 'default', 0);
            }
        }
        return $html;
    }
}
