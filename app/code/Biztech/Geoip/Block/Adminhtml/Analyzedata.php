<?php

namespace Biztech\Geoip\Block\Adminhtml;

use Magento\Backend\Block\Widget\Context;
use Biztech\Geoip\Helper\Data;

class Analyzedata extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected $helper;

    public function __construct(
        Context $context,
        Data $helper
    ) {
        $this->helper = $helper;
        parent::__construct($context);
    }

    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_analyzedata';/*block grid.php directory*/
        $this->_blockGroup = 'Biztech_Geoip';
        $this->_headerText = __('GeoIP Analyze Data');
        parent::_construct();
        $this->removeButton('add');
    }
}
