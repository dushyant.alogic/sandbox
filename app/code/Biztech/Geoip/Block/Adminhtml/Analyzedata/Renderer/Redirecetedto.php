<?php

namespace Biztech\Geoip\Block\Adminhtml\Analyzedata\Renderer;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\DataObject;
use Magento\Backend\Block\Context;

class Redirecetedto extends AbstractRenderer
{
    /**
    * @var \Biztech\Geoip\Model\Rule
    */
    protected $_rule;

    /**
    * @var \Magento\Store\Model\StoreManagerInterface
    */
    protected $_storeManager;


    /**
     * @param Context    $context
     */
    public function __construct(
        Context $context,
        \Biztech\Geoip\Model\Rule $rule,
        \Magento\Store\Model\StoreManagerInterface $store

    ) {
        $this->_rule = $rule;
        $this->_storeManager = $store;
        parent::__construct($context);
    }

    public function render(DataObject $row)
    {
        $ruleId = $row->getAnalyzeRuleId();
        if (!$ruleId) {
            return;
        }
        $ruleData = $this->_rule->load($ruleId);
        if($ruleId != $ruleData['rule_id']) return;
        if(!isset($ruleData['rule_store_id'])) return;
        return $this->_storeManager->getStore()->load($ruleData['rule_store_id'])->getName();
    }
}