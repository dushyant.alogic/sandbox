<?php

namespace Biztech\Geoip\Block\Adminhtml\Analyzedata;

use Biztech\Geoip\Helper\Data as BizHelper;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Biztech\Geoip\Model\ResourceModel\Analyze\Collection
     */
    protected $_collectionFactory;

    /**
    * @var \Magento\Store\Model\StoreManagerInterface
    */
    protected $_storeManager;

    /**
    * @var \Biztech\Geoip\Model\Rule
    */
    protected $_rule;

    /**
     * @var \Biztech\Geoip\Helper\Data
     */
    protected $helper;

    /**
     * @param \Magento\Backend\Block\Template\Context                     $context
     * @param \Magento\Backend\Helper\Data                                $backendHelper
     * @param \Biztech\Geoip\Model\ResourceModel\Analyze\Collection $collectionFactory
     * @param \Magento\Framework\Module\Manager                           $moduleManager
     * @param BizHelper                                                   $helper
     * @param array                                                       $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Biztech\Geoip\Model\ResourceModel\Analyze\Collection $collectionFactory,
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\Store\Model\StoreManagerInterface $store,
        \Biztech\Geoip\Model\Rule $rule,
        BizHelper $helper,
        array $data = []
    ) {
        $this->_logger = $context->getLogger();
        $this->_collectionFactory = $collectionFactory;
        $this->moduleManager = $moduleManager;
        $this->_storeManager = $store;
        $this->_rule = $rule;
        $this->helper = $helper;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('geoip/analyze/index', ['_current' => true]);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        if ($this->helper->isEnabled()) {
            parent::_construct();
        }

        $this->setId('analyzeGrid');
        $this->setDefaultDir('DESC');
        $this->setDefaultSort('analyze_id');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
    }


    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        try {
            $collection = $this->_collectionFactory->load();
            $this->setCollection($collection);
            parent::_prepareCollection();
            return $this;
        } catch (\Exception $e) {
            $this->_logger->debug($e->getMessage());
        }
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'analyze_id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'analyze_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
                'filter' => false,
            ]
        );
        $this->addColumn(
            'analyze_rule_id',
            [
                'header' => __('Redirected To'),
                'index' => 'analyze_rule_id',
                'class' => 'analyze_rule_id',
                'type' => 'options',
                'options' => $this->Ridirectto(),
                'renderer' => 'Biztech\Geoip\Block\Adminhtml\Analyzedata\Renderer\Redirecetedto'
            ]
        );
        $this->addColumn(
            'visitor_country',
            [
                'header' => __('Visitor Country'),
                'index' => 'visitor_country',
                'class' => 'visitor_country',
                'type' => 'options',
                'options' => $this->countryOptions(),
            ]
        );

        $this->addColumn(
            'visitor_ip',
            [
                'header' => __('Visitor IP'),
                'index' => 'visitor_ip',
                'class' => 'visitor_ip',
                'type' => 'text',
                'filter' => false,
                'sortable' => false,
                'renderer' => 'Biztech\Geoip\Block\Adminhtml\Analyzedata\Renderer\Ipview'
            ]
        );

        $this->addColumn(
            'redirect_to',
            [
                'header' => __('Redirect Action'),
                'index' => 'redirect_to',
                'class' => 'redirect_to',
                'type' => 'text',
                'renderer' => 'Biztech\Geoip\Block\Adminhtml\Analyzedata\Renderer\Redirectaction'
            ]
        );
        $this->addColumn(
            'created_at',
            [
                'header' => __('Action Date'),
                'index' => 'created_at',
                'class' => 'created_at',
                'type' => 'date',
            ]
        );
        
        $this->addColumn(
            'blocked_request',
            [
                'header' => __('Blocked Request'),
                'index' => 'blocked_request',
                'class' => 'blocked_request',
                'type' => 'options',
                'options' => $this->blockeStatus(),
                'renderer' => 'Biztech\Geoip\Block\Adminhtml\Analyzedata\Renderer\Blocked'
            ]
        );

        $this->addColumn(
            'analyze_data_detail',
            [
                'header' => __('Detail'),
                'index' => 'analyze_data_detail',
                'class' => 'analyze_data_detail',
                'filter' => false,
                'sortable' => false,
                'renderer' => 'Biztech\Geoip\Block\Adminhtml\Analyzedata\Renderer\Detailview'
            ]
        );

        // $this->addExportType('*/*/exportCsv', __('CSV'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('analyze_id');
        $this->getMassactionBlock()->setFormFieldName('analyze_id');
        $this->getMassactionBlock()->addItem(
            'delete',
            [
            'label' => __('Delete'),
            'url' => $this->getUrl('geoip/*/massDelete'),
            'confirm' => __('Are you sure want to delete selected analyze data?')
                ]
        );
        return $this;
    }

    /* get redirected store name*/
    public function Ridirectto()
    {
        $collectionData = $this->_collectionFactory->getData();
        $stores= array();
        foreach ($collectionData as $key => $value) {
            if($value['analyze_rule_id']==0) continue;
            $ruleData = $this->_rule->load($value['analyze_rule_id']);
            if(!isset($ruleData['rule_store_id'])) continue;
            $StoreName = $this->_storeManager->getStore()->load($ruleData['rule_store_id'])->getName();
            $stores[$ruleData['rule_id']] = $StoreName;
        }
        return $stores;
    }

    /* get customer country name*/
    public function countryOptions()
    {
        $collectionData = $this->_collectionFactory->getData();
        $country= array();
        foreach ($collectionData as $key => $value) {
            $country[$value['visitor_country']] = $value['visitor_country'];
        }
        return $country;
    }

    /* get redirection status*/
    public function blockeStatus()
    {
         return ["1" => __('Blocked') , "0" => __('UnBlocked') ];
    }
}
