<?php

/**
 * Copyright © 2020 Biztech . All rights reserved.
 */

namespace Biztech\Geoip\Block\Index;

use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Biztech\Geoip\Helper\Data;
use \Magento\Framework\App\Response\Http;
use \Magento\Framework\UrlInterface;
use \Magento\Framework\App\Config\ReinitableConfigInterface;
class BlockedCountryPopup extends Template
{
    /**
     * @var Biztech\Geoip\Helper\Data
     */
    protected $_dataHelper;

    /**
     * @var Magento\Framework\App\Response\Http
     */
    public $_http;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_urlInterface;

    /**
    * @var \Magento\Framework\App\Config\ReinitableConfigInterface
    */
    protected $_coreConfig;

	 /**
     * @param Context $context
     * @param Data $dataHelper
     */
   	public function __construct(
        Context $context,
        Data $dataHelper,
        Http $http,
        UrlInterface $urlInterface,
        ReinitableConfigInterface $coreConfig,
        array $data = []
    ) {
        $this->_dataHelper = $dataHelper;
        $this->_http = $http;
        $this->_urlInterface = $urlInterface;
        $this->_coreConfig = $coreConfig;
        parent::__construct($context, $data);
    }

    public function getTitle()
    {
        return $this->_dataHelper->BlockedCountryPopupTitle();   
    }

    public function getMessage()
    {
        return $this->_dataHelper->BlockedCountryPopupMessage();
    }

    public function redirectTo()
    {
        $redirect_url =  $this->_coreConfig->getValue('geoip/general/redirect_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if($redirect_url) {
            return $this->_urlInterface->getUrl($redirect_url);
        } else {
            return $this->_urlInterface->getUrl("/");
        }
    }

}
