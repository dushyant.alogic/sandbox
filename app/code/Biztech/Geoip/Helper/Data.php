<?php
/**
 * Copyright © 2020 Biztech . All rights reserved.
 */

namespace Biztech\Geoip\Helper;

use Magento\Store\Model\ScopeInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
	const XML_PATH_DATA = "geoip/activation/data";
	const XML_PATH_INSTALLED = 'geoip/activation/installed';
    const XML_PATH_WEBSITES = 'geoip/activation/websites';
    const XML_PATH_ENABLED = 'geoip/general/active';
    const XML_PATH_KEY = 'geoip/activation/key';
    const XML_PATH_EN = 'geoip/activation/en';
    const XML_PATH_POPUP = 'geoip/general/enable_popup';
    const GEOIP_ACTIVATION_KEY = 'geoip/general/geoip_maxmind_key';


    /**
    * @var \Magento\Store\Model\StoreManagerInterface
    */
    protected $_storeManager;

    /**
    * @var \Magento\Framework\Encryption\EncryptorInterface
    */
    protected $_encryptor;

    /**
    * @var \Magento\Framework\App\Config\ReinitableConfigInterface
    */
    protected $_coreConfig;

    /**
    * @var \Magento\Framework\App\Cache\TypeListInterface
    */
    protected $_cacheTypeList;
    /**
    * @var \Framework\App\Cache\Frontend\Pool
    */
    protected $_cacheFrontendPool;
     /**
     * @param Context $context
     * @param EncryptorInterface $encryptor
     * @param StoreManagerInterface $storeManager
     * @param ReinitableConfigInterface $coreConfig
     * @param TypeListInterface $cacheTypeList
     * @param Pool $cacheFrontendPool
     */

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ReinitableConfigInterface $coreConfig,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
        ) {
        $this->_storeManager = $storeManager;
        $this->_encryptor = $encryptor;
        $this->_coreConfig = $coreConfig;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        parent::__construct($context);
    }
    /**
     * @return mixed
     */
    public function getDataInfo()
    {
        $data = $this->_coreConfig->getValue(self::XML_PATH_DATA, ScopeInterface::SCOPE_STORE);
        return json_decode(base64_decode($this->_encryptor->decrypt($data)));
    }

    /**
     * @param $url
     * @return mixed
     */
    public function getFormatUrl($url)
    {
        $input = trim($url, '/');
        if (!preg_match('#^http(s)?://#', $input)) {
            $input = 'http://' . $input;
        }
        $urlParts = parse_url($input);
        if (isset($urlParts['path'])) {
            $domain = preg_replace('/^www\./', '', $urlParts['host'] . $urlParts['path']);
        } else {
            $domain = preg_replace('/^www\./', '', $urlParts['host']);
        }
        return $domain;
    }

    /**
     * @return array
     */
	public function getAllStoreDomains()
    {
        $domains = [];
        foreach ($this->_storeManager->getWebsites() as $website) {
            foreach ($website->getGroups() as $group) {
                $stores = $group->getStores();
                foreach ($stores as $store) {
                    $url = $store->getConfig('web/unsecure/base_url');
                    if ($domain = trim(preg_replace('/^.*?\/\/(.*)?\//', '$1', $url))) {
                        $domains[] = $domain;
                    }
                    $url = $store->getConfig('web/secure/base_url');
                    if ($domain = trim(preg_replace('/^.*?\/\/(.*)?\//', '$1', $url))) {
                        $domains[] = $domain;
                    }
                }
            }
        }
        return array_unique($domains);
    }

    /**
     * @return array
     */
    public function getAllWebsites()
    {
        $value = $this->_coreConfig->getValue(self::XML_PATH_INSTALLED, ScopeInterface::SCOPE_STORE);
        if (!$value) {
            return [];
        }
        $data = $this->_coreConfig->getValue(self::XML_PATH_DATA, ScopeInterface::SCOPE_STORE);
        $web = $this->_coreConfig->getValue(self::XML_PATH_WEBSITES, ScopeInterface::SCOPE_STORE);
        $websites = explode(',', str_replace($data, '', $this->_encryptor->decrypt($web)));
        $websites = array_diff($websites, [""]);
        return $websites;
    }

    

    /**
     * @return bool
     */
    public function isEnabled()
    {
        $isenabled = $this->_coreConfig->getValue(self::XML_PATH_ENABLED, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $allStores = $this->getAllWebsites();
        if(empty($allStores) || $isenabled==0) {
            return false;
        } else return true;
    }

    /**
     * @return key
     */
    public function maxmindKey()
    {
        return $this->_coreConfig->getValue(self::GEOIP_ACTIVATION_KEY, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return bool
     */
    public function isPopupEnabled()
    {
        return $this->_coreConfig->getValue(self::XML_PATH_POPUP, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* manual store switch is enable or not*/
    public function downloadDatabaseByCron()
    {
       return $this->_coreConfig->getValue('geoip/general/download_maxmind_db', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    /* manual store switch is enable or not*/
    public function deleteDatabaseByCron()
    {
       return $this->_coreConfig->getValue('geoip/general/delete_maxmind_db', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }


    /* manual store switch is enable or not*/
    public function isManualstoreSwich()
    {
        if(!$this->isEnabled()) return 1;
        return $this->_coreConfig->getValue('geoip/general/enable_manual_store_switch', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* manual currency switch is enable or not*/
    public function isManualCurrencySwich()
    {
        if(!$this->isEnabled()) return 1;
        return $this->_coreConfig->getValue('geoip/general/enable_manual_currency_switch', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* popup title text. */
    public function pupupTitle()
    {
       return $this->_coreConfig->getValue('geoip/general/popup_title', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    /* popup message text. */
    public function pupupMessage()
    {
       return $this->_coreConfig->getValue('geoip/general/popup_content', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    /* popup Button text. */
    public function pupupButtonText()
    {
       return $this->_coreConfig->getValue('geoip/general/popup_button', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* Blocked visitor page view. */
    public function blockedView()
    {
       return $this->_coreConfig->getValue('geoip/general/Blockedview', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* get blocked country popup title*/
    public function BlockedCountryPopupTitle()
    {
       return $this->_coreConfig->getValue('geoip/general/blocked_country_popup_title', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* get blocked country popup message*/
    public function BlockedCountryPopupMessage()
    {
       return $this->_coreConfig->getValue('geoip/general/blocked_country_popup_message', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* get last synched cron time*/
    public function cronLastSynched()
    {
       return $this->_coreConfig->getValue('geoip/cron/last_synched', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    public function flushCache($cacheTypes)
    {
        $_types = $cacheTypes;
     
        foreach ($_types as $type) {
            $this->_cacheTypeList->cleanType($type);
        }
        foreach ($this->_cacheFrontendPool as $cacheFrontend) {
            $cacheFrontend->getBackend()->clean();
        }
    }
}