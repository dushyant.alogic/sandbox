<?php
/**
 * Copyright © 2020 Biztech . All rights reserved.
 */

namespace Biztech\Geoip\Helper;

use \Magento\Framework\App\Helper\Context;
use \Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Framework\App\Request\Http;
use \Biztech\Geoip\Model\Rule;
use \Magento\Store\Model\StoreManagerInterface;
use \Magento\Framework\App\Config\ReinitableConfigInterface;

class GeoipDb extends AbstractHelper
{
    const GEOIP_ACTIVATION_KEY = 'geoip/general/geoip_maxmind_key';
    const GEOIP_BLOCKED_COUNTRY = 'geoip/general/country_block';
    const GEOIP_BLOCKED_IP = 'geoip/general/ip_block';
    const POPUP_REGENERATE_TIME = 'geoip/general/popup_regenerate';
	/**
    * @var \Magento\Framework\App\Request\Http
    */
    Protected $_httpRequest;

    /**
    * @var \Biztech\Geoip\Model\Rule
    */
    protected $_rule;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\App\Config\ReinitableConfigInterface
     */
    protected $_coreConfig;
	/**
     * GeoIP DB constructor.
     * @param Context $context
     * @param Http $httpRequest
     */
    public function __construct(
        Context $context,
        Http $httpRequest,
        Rule $rule,
        StoreManagerInterface $storeManager,
        ReinitableConfigInterface $coreConfig
    ) {
        $this->_httpRequest = $httpRequest;
        $this->_rule = $rule;
        $this->_storeManager = $storeManager;
        $this->_coreConfig = $coreConfig;
        parent::__construct($context);
    }
    public function downloadDatabase()
	{
        try {
            $CITY_DATABASE = 'https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-City&license_key=YOUR_LICENSE_KEY&suffix=tar.gz';
            $COUNTRY_DATABASE = 'https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-Country&license_key=YOUR_LICENSE_KEY&suffix=tar.gz';
            $COUNTRY_CSV = 'https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-Country-CSV&license_key=YOUR_LICENSE_KEY&suffix=zip';
            $CITY_CSV = 'https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-City-CSV&license_key=YOUR_LICENSE_KEY&suffix=zip';
            $geoIP_key = $this->_coreConfig->getValue(self::GEOIP_ACTIVATION_KEY, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            if($geoIP_key =='' || $geoIP_key == null)
            {
                $result['success'] = false;
                $result['message'] = "Please Enter MaxMind license key for the download GeoIP Database. If you have not license key then please <a href='https://www.maxmind.com/en/geolite2/signup'><b>Sign-Up</a> to MaxMind </b> for create account.";
                return $result;
            }
    		$DBdir = $this->getDatabaseFolder();
    		if(!file_exists($DBdir))
    		{
    			$check_Database_Folder = $this->createDatabaseFolder();
    			if(!$check_Database_Folder['success'])
    				return $check_Database_Folder;
    		}
            $COUNTRY_DATABASE = str_replace("YOUR_LICENSE_KEY", $geoIP_key,  $COUNTRY_DATABASE);
    		$Maxmind_City_DB_Path = $COUNTRY_DATABASE;
    		$DBdata = curl_init($Maxmind_City_DB_Path);
    		if(!$DBdata) {
    			$result['success'] = false;
    			$result['message'] = "<b>GeoIP Dtatabase temporary unavailable, please check later.</b>";
    	    	return $result;
    		}
            $city_destination = $this->getCityDatabasePath();
            $createDatabaseFile = fopen($city_destination, "wb");
            curl_setopt($DBdata, CURLOPT_FILE, $createDatabaseFile);
            $data = curl_exec($DBdata);
            curl_close($DBdata);
            fclose($createDatabaseFile);
            stream_wrapper_restore('phar');
            $archive = new \PharData($city_destination);
            if ($archive) {
                $this->backupDatabase();
                $destination = str_replace('.tar.gz','.mmdb',$city_destination);
                foreach($archive as $file) {
                    $fileName  = $file->getFileName();
                    $pathName = $file->getPath();
                    $dir =$pathName.'/'.$fileName;
                    if (is_dir($dir)){
                        if ($dh = opendir($dir)){
                            while (($file = readdir($dh)) !== false){
                                if(strpos($file, "mmdb") !== false) {
                                    copy( $dir."/".$file,$destination);
                                }
                            }
                            closedir($dh);
                        }
                    }
                }
            }
        	unlink($city_destination);
    		$result['success'] = true;
    		$result['message'] = "Database Downloaded successfully.";
    		return $result;
        } catch ( \Exception $e) {
            $result['success'] = false;
            $result['message'] = __("Something went wrong. Can you please check your MaxMind license key?");
            return $result;

        }
	}

    /**
    * Create backup old GeoIP Database
    *
    * @return bool
    */
	public function backupDatabase()
	{
		$oldDatabasePath = $this->getCityDatabasePath();
		$oldDatabasePath = str_replace('.tar.gz', '.mmdb', $oldDatabasePath);
        if (!file_exists($oldDatabasePath)) {
            return true;
        }
        return copy($oldDatabasePath,$oldDatabasePath. '_backup_' . time());

	}
	/**
     * Check the database folder is exist or not in which GeoIP database should be placed
     *
     * @return bool
     */
	public function getDatabaseFolder()
	{
		$path = BP . '/app/code/Biztech/Geoip/etc/database/';
        if (file_exists($path)) {
            return $path;
	    } else {
            $path = BP . '/vendor/biztech/module-geoip/etc/database/';
            if (file_exists($path)) {
            	return $path;
        	}
        }
	}
	/**
     * Create database folder in which GeoIP database should be placed
     *
     * @return bool
     */
	public function createDatabaseFolder()
	{
		$path = BP . '/app/code/Biztech/Geoip/etc/';
        if (file_exists($path)) {
        	if(!is_writable($path)) {
            		$result['success'] = false;
			    	$result['message'] = "Permission denied for the create database folder. Please give permission to Geoip module <b>Biztech/Geoip/etc</b>";
			    	return $result;
            }
           	mkdir($path . "database");
            $result['success']=true;
            return $result;
        } else {
            $path = BP . '/vendor/biztech/module-geoip/etc/';
            if (file_exists($path)) {
            	if(!is_writable($path)) {
            		$result['success'] = false;
			    	$result['message'] = "Permission denied for the create database folder. Please give permission to Geoip module <b>Biztech/Geoip/etc</b>";
			    	return $result;
            	}
            	mkdir($path . "database");
            	$result['success']=true;
            	return $result;
        	}
        }
	}
	/**
	* get city database path
	*
	* @return string
	*/
    public function getCityDatabasePath()
    {
        return $this->getDatabaseFolder() . 'cityDatabase.' . 'tar.gz' ;
    }

    /**
    * get customer ip address
    *
    * @return string
    */
    public function getCustomerIP()
    {
        return $this->_httpRequest->getClientIp();
    }

    /**
    * check DB exist or not
    *
    * @return bool
    */
    public function checkDatabaseExist()
    {
       $database = $this->getDatabaseFolder().'cityDatabase.mmdb';
        if (!is_readable($database)) {
            return false;
        }
        return $database;
    }
    /**
    * get country code by site visiter's ip
    *
    * @return string
    */
    public function getCountryLoction($value='')
    {
        $customerIp = $this->getCustomerIP();
        $databasePath = $this->checkDatabaseExist();
        if(!$databasePath) return;
        $reader = new \Biztech\Geoip\Model\MaxMind\Db\Reader($databasePath);
        $locationData = $reader->get($customerIp);
        $reader->close();
        return $locationData;
    }

    /**
     * Get Blocked ip address
     *
     * @return array
     */
    public function getBlockedIp()
    {
        $Blocked_ip = $this->_coreConfig->getValue(self::GEOIP_BLOCKED_IP, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $ip_list =  preg_split('/\r?\n/', $Blocked_ip);
        return $ip_list;
    }

    /**
     * Get Blocked countries
     *
     * @return array
     */
    public function getBlockedCounries()
    {
        $blocked_countries = $this->_coreConfig->getValue(self::GEOIP_BLOCKED_COUNTRY, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $country_list = explode(',', $blocked_countries);
        return $country_list;
    }

    /**
     * Get cookie time for popup regenerate
     *
     * @return time(seconds)
     */
    public function getPopupRegenerateTime()
    {
        return 1200;
        $popup_regenerate_time = $this->_coreConfig->getValue(self::POPUP_REGENERATE_TIME, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $time = explode(",", $popup_regenerate_time);
        /*                 Hour               Minute          Second */
        $seconds = (($time[0] * 60) * 60) + ($time[1] * 60) + $time[2];
        if ($seconds==0) {
            $seconds = 1200;
        } 
        return $seconds;
    }

    /**
    * get Store code by site visiter's Country Code
    *
    * @return array
    */
    public function getStoreCurrency($countryCode)
    {
        if($this->_coreConfig->getValue('geoip/general/block_visitor', \Magento\Store\Model\ScopeInterface::SCOPE_STORE)) {
            $redirect_url = $this->_coreConfig->getValue('geoip/general/redirect_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $blockedCountries = $this->getBlockedCounries();
            if (in_array($countryCode,$blockedCountries)) {
                if($redirect_url) {
                    return array("redirect_to"=>$redirect_url,"action"=>"country",'blocked'=>true);
                }
                return array("redirect_to"=>"/","action"=>"country",'blocked'=>true);
            }
            $blockedIp = $this->getBlockedIp();
            $customerIp = $this->getCustomerIP();
            
            if (in_array($customerIp,$blockedIp)) {
                if($redirect_url) {
                    return array("redirect_to"=>$redirect_url,"action"=>"ip",'blocked'=>true);
                }
                return array("redirect_to"=>"/","action"=>"ip",'blocked'=>true);
            }
        }
        $ruleCollection = $this->_rule->getCollection()->load();
        $ruleData = $ruleCollection->getData();
        $store_currency_data = array();

        foreach ($ruleData as $id => $data) {
            $current_website_id = $this->_storeManager->getWebsite()->getId();
            $rule_website_id = $this->_storeManager->getStore($data['rule_store_id'])->getWebsiteId();
            if($current_website_id !== $rule_website_id) continue;
            if($data['status'] == 0) continue;
            $_countryCode = explode(',', $data['country']);
            if(in_array($countryCode, $_countryCode))
            {
                $store_currency_data[$id]['rule_id'] = $data['rule_id'];
                $store_currency_data[$id]['store_id'] = $data['rule_store_id'];
                $store_currency_data[$id]['currency_code'] = $data['currency'];
                $store_currency_data[$id]['priority'] = $data['priority'];
            }
        }
        if(count($store_currency_data)>1) {
            $priority = max(array_column($store_currency_data, 'priority'));
        }
        if(count($store_currency_data)==1) return $store_currency_data;
        foreach ($store_currency_data as $key => $value) {
            if($value['priority'] == $priority)
            {
                $store_currencyData = array();
                $store_currencyData[0] = $value;
                return $store_currencyData;
            }
        }
        return array();
    }
}
