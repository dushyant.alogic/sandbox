<?php
/**
 * Copyright © 2020 Biztech . All rights reserved.
 */

namespace Biztech\Geoip\Controller\Adminhtml\Rule;

use Magento\Backend\App\Action;

class Deleterule extends Action
{
    private $gridFactory;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Biztech\Geoip\Model\RuleFactory $gridFactory
    ) {
        parent::__construct($context);
        $this->gridFactory = $gridFactory;
    }
    public function execute()
    {
        try {
            $id = $this->_request->getParam('id');  
            $rule = $this->gridFactory->create();
            $result = $rule->setRuleId($id);            
            $result->delete();       
            $this->messageManager->addSuccess(__('GeoIP Rule have been deleted.'));
         } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        return $this->_redirect('*/*/index'); 
    }
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Biztech_Geoip::rule');
    }
}
