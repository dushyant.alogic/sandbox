<?php

namespace Biztech\Geoip\Controller\Adminhtml\Rule;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Biztech\Geoip\Helper\Data;

class Index extends Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\Page
     */
    protected $resultPage;
    /**
     * @var \Biztech\Geoip\Helper\Data
     */
    protected $_data;
    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Data $dataHelper
    ) {
        parent::__construct($context);
        $this->_data = $dataHelper;
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        $this->resultPage = $this->resultPageFactory->create();
        $this->resultPage ->getConfig()->getTitle()->set((__('GeoIP Manage Rules')));
        if(!$this->_data->isEnabled()) {
            $this->messageManager->addError(__("Extension- GeoIP is not enabled. Please enable it from Store > Configuration > AppJetty Extensions > GeoIP Redirect"));
        }
        return $this->resultPage;
    }
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Biztech_Geoip::rule');
    }
}
