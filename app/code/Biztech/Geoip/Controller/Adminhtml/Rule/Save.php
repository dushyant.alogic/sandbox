<?php
/**
 * Copyright © 2020 Biztech . All rights reserved.
 */

namespace Biztech\Geoip\Controller\Adminhtml\Rule;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Biztech\Geoip\Model\RuleFactory
     */
    protected $gridFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
    * @var \Biztech\Geoip\Model\Rule
    */
    protected $_rule;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Biztech\Geoip\Model\RuleFactory $gridFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Biztech\Geoip\Model\RuleFactory $gridFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Biztech\Geoip\Model\Rule $rule
    ) {
        parent::__construct($context);
        $this->gridFactory = $gridFactory;
        $this->_storeManager = $storeManager;
        $this->_rule = $rule;
    }

    /**
     * Save and Edit GeoIP rule.
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        if (!$data) {
            $this->_redirect('geoip/rule/addrule');
            return;
        }
        
        try {
            $ruleData = $this->gridFactory->create();
            if (!isset($data['rule_id'])) {
                $ruleStoreData = $ruleData->getCollection()->getData();
                foreach ($ruleStoreData as $key => $value) {
                    if($value['rule_store_id'] == $data['rule_store_id'])
                    {
                        $this->messageManager->addError(__("You have already created the rule for the Storeview <b>".$this->_storeManager->getStore($data['rule_store_id'])->getName()."</b>. You can edit that rule."));
                        $this->_redirect('geoip/rule/addrule');
                        return;
                    }
                    $countries = explode(',', $value['country']);
                    $match_Country = array_intersect($countries, $data['country']);
                    if(!empty($match_Country) && $value['priority'] == $data['priority']) {
                        $this->messageManager->addError(__("There already exists a rule for the same country and with same priority."));
                        $this->_redirect('geoip/rule/addrule');
                        return;
                    }
                }
            } elseif(isset($data['rule_id'])) {
                $item = $this->_rule->load($data['rule_id']);
                $ruleData = $this->gridFactory->create();
                $ruleStoreData = $ruleData->getCollection()->getData();
                foreach ($ruleStoreData as $key => $value) {
                    if($item->getRuleStoreId() != $data['rule_store_id'] && $value['rule_store_id'] == $data['rule_store_id'])
                    {
                        $this->messageManager->addError(__("You have already created the rule for the Storeview <b>".$this->_storeManager->getStore($data['rule_store_id'])->getName()."</b>. You can edit that rule."));
                        $this->_redirect('geoip/rule/addrule/id/'.$data['rule_id']);
                        return;
                    }
                    $current_country = explode(',',$item->getCountry());
                    $current_country_status = array_intersect($current_country, $data['country']);
                    $countries = explode(',', $value['country']);
                    $match_Country = array_intersect($countries, $data['country']);

                    if((empty($current_country_status) || $item->getPriority() != $data['priority']) && (!empty($match_Country) && $value['priority'] == $data['priority'] ))
                    {
                        $this->messageManager->addError(__("There already exists a rule for the same country and with same priority."));
                        $this->_redirect('geoip/rule/addrule/id/'.$data['rule_id']);
                        return;
                    }
                }
            }
            if (isset($data['rule_id'])) {
                $ruleData->setRuleId($data['rule_id']);
            }
            $ruleData->setData($data);
            $countries = implode(',', $data['country']);
            $ruleData->setCountry($countries);
            $ruleData->save();
            if (isset($data['rule_id'])) {
                $this->messageManager->addSuccess(__('GeoIP Rule has been successfully updated for the <b>'.$this->_storeManager->getStore($data['rule_store_id'])->getName().'</b> Storeview.'));
            } else {
                $this->messageManager->addSuccess(__('GeoIP Rule has been successfully created for the <b>'.$this->_storeManager->getStore($data['rule_store_id'])->getName().'</b> Storeview.'));
            }
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        $this->_redirect('geoip/rule/index');
    }
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Biztech_Geoip::rule');
    }
}