<?php

/**
 *
 * Copyright © 2020 Biztech. All rights reserved.
 */

namespace Biztech\Geoip\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{

    const GEOIP_ENABLE = 'Geoip_store_switch';
    const GEOIP_BACK_REDIRECT = 'Geoip_back_redirect';

    /**
     * @var \Magento\Framework\Stdlib\CookieManagerInterface
     */
    protected $_cookieInterface;

    /**
     * @var Magento\Framework\Stdlib\Cookie\PublicCookieMetadata
     */
    protected $_publiccookieMetadata;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieInterface,
        \Magento\Framework\Stdlib\Cookie\PublicCookieMetadata $publiccookieMetadata
    ) {
        parent::__construct($context);
        $this->_cookieInterface = $cookieInterface;
        $this->_publiccookieMetadata = $publiccookieMetadata;
        $this->_publiccookieMetadata->setDuration(null)->setPath('/');
        $this->_cookieInterface->setPublicCookie(self::GEOIP_ENABLE, 'enabled', $this->_publiccookieMetadata);
        $geoipBackRedirect = $this->_cookieInterface->getCookie(self::GEOIP_BACK_REDIRECT);
        $this->_redirect($geoipBackRedirect);
        /*
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath($this->_redirect->getRedirectUrl());*/
    }

    public function execute()
    {
        /*$this->_publiccookieMetadata->setDuration(14400)->setPath('/');
        $this->_cookieInterface->setPublicCookie(self::GEOIP_ENABLE, 'enabled', $this->_publiccookieMetadata);
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath($this->_redirect->getRedirectUrl());*/
        // $this->_redirect('');
    }
}
