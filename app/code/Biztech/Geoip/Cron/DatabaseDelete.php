<?php

namespace Biztech\Geoip\Cron;
use \Biztech\Geoip\Helper\Data;
use \Biztech\Geoip\Helper\GeoipDb;

class DatabaseDelete
{
	/**
     * @var Biztech\Geoip\Helper\Data
     */
    protected $_helper;

    /**
     * @var Biztech\Geoip\Helper\GeoipDb
     */
    protected $_geoipHelper;

	/**
     * @param Data $helper
     * @param GeoipDb $geoipHelper
     * @param Logger $logger
     */
    public function __construct(
        Data $helper,
        GeoipDb $geoipHelper
    ) {
        $this->_helper = $helper;
        $this->_geoipHelper = $geoipHelper;
    }

    public function execute()
    {
    	try {
            if($this->_helper->deleteDatabaseByCron()) {
    			if($this->_helper->isEnabled()) {
                    $database_path = $this->_geoipHelper->getDatabaseFolder();
                    $files=scandir($database_path);
                    foreach ($files as $key => $file) 
                    {
                        if($key==0 or $key==1)
                            continue;
                        if (strpos($file, 'backup') !== false) {
                            $filepath =$database_path.$file;
                            if(file_exists($filepath))  {
                            unlink($filepath);
                            }                       
                        }
                    }
                }
            }
	    } catch (\Exception $e) { }
    }
}