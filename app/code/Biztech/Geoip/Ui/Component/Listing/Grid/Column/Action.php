<?php
namespace Biztech\Geoip\Ui\Component\Listing\Grid\Column;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;
 
class Action extends Column
{
    const ROW_EDIT_URL = 'geoip/rule/addrule';
    const ROW_DELETE_URL = 'geoip/rule/deleterule';
    protected $_urlBuilder;
    private $_editUrl;
    private $_deleteUrl;
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = [],
        $editUrl = self::ROW_EDIT_URL,
        $deleteUrl = self::ROW_DELETE_URL
    ) 
    {
        $this->_urlBuilder = $urlBuilder;
        $this->_editUrl = $editUrl;
        $this->_deleteUrl= $deleteUrl;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $name = $this->getData('name');
                if (isset($item['rule_id'])) {
                    $item[$name]['edit'] = [
                        'href' => $this->_urlBuilder->getUrl(
                            $this->_editUrl, 
                            ['id' => $item['rule_id']]
                        ),
                        'label' => __('Edit'),
                    ];
                    $item[$name]['delete'] = [
                    'href' => $this->_urlBuilder->getUrl(
                    $this->_deleteUrl, 
                    ['id' => $item['rule_id']]
                    ),
                    'confirm' => [
                            'title' => __('Delete'),
                            'message' => __('Are you sure to delete this GeoIP rule?')
                        ],
                    'label' => __('Delete'),
                    ];
                }
            }
        }
        return $dataSource;
    }
}
