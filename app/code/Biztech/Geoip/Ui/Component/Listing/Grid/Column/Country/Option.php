<?php
/**
 * Copyright © 2020 Biztech . All rights reserved.
 */
    
namespace Biztech\Geoip\Ui\Component\Listing\Grid\Column\Country;

class Option implements \Magento\Framework\Data\OptionSourceInterface
{

    /**
     * @var $_country
     */
    protected $_country;

    public function __construct(
        \Magento\Directory\Model\Config\Source\Country $country
    ){
       $this->_country = $country;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
       return $this->_country->toOptionArray();
    }
}