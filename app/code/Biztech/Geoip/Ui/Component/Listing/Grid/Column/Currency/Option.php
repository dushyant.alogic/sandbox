<?php
/**
 * Copyright © 2020 Biztech . All rights reserved.
 */
    
namespace Biztech\Geoip\Ui\Component\Listing\Grid\Column\Currency;

class Option implements \Magento\Framework\Data\OptionSourceInterface
{

    /**
     * @var $_country
     */
    protected $_country;

    public function __construct(
        \Magento\Config\Model\Config\Source\Locale\Currency $currency
    ){
       $this->_currency = $currency;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
       return $this->_currency->toOptionArray();
    }
}