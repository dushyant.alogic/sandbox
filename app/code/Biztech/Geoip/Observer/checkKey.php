<?php

namespace Biztech\Geoip\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Config\Model\Config\Factory;
use Biztech\Geoip\Helper\Data;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\App\RequestInterface;
use Zend\Json\Json;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Framework\App\Config\ValueFactory;
use Magento\Framework\Event\Observer;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Cache\Frontend\Pool;

class checkKey implements ObserverInterface
{
    const XML_PATH_ACTIVATIONKEY = 'geoip/activation/key';
    const XML_PATH_DATA = 'geoip/activation/data';

    protected $scopeConfig;
    protected $encryptor;
    protected $helper;
    protected $request;
    protected $resourceConfig;
    protected $zend;
    protected $_cacheTypeList;
    protected $_cacheFrontendPool;

    /**
     * @param ScopeConfigInterface                           $scopeConfig
     * @param EncryptorInterface                             $encryptor
     * @param Data                                           $helper
     * @param RequestInterface                               $request
     * @param Json                                           $zend
     * @param Config                                         $resourceConfig
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\Frontend\Pool     $cacheFrontendPool
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        EncryptorInterface $encryptor,
        Data $helper,
        RequestInterface $request,
        Json $zend,
        Config $resourceConfig,
        TypeListInterface $cacheTypeList,
        Pool $cacheFrontendPool
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->encryptor = $encryptor;
        $this->helper = $helper;
        $this->request = $request;
        $this->zend = $zend;
        $this->resourceConfig = $resourceConfig;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $params = $this->request->getParam('groups');
        if(!isset($params['activation'])) return;
        $k = $params['activation']['fields']['key']['value'];
        /*$k = $this->scopeConfig->getValue(
            self::XML_PATH_ACTIVATIONKEY,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );*/
        $s = '';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, sprintf('https://www.appjetty.com/extension/licence.php'));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'key=' . urlencode($k) . '&domains=' . urlencode(implode(
            ',',
            $this->helper->getAllStoreDomains()
        )) . '&sec=magento2-geoip-redirect');
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $content = curl_exec($ch);
        $resArray = $this->zend->decode($content);
        $res = (array)$resArray;
        $moduleStatus = $this->resourceConfig;
        if (empty($res)) {
            $moduleStatus->saveConfig('geoip/activation/key', "");
            $moduleStatus->saveConfig('geoip/general/active', 0, 'default', 0);
            $data = $this->scopeConfig('geoip/activation/data');
            $this->resourceConfig->saveConfig('geoip/activation/data', $data, 'default', 0);
            $this->resourceConfig->saveConfig('geoip/activation/websites', '', 'default', 0);
            $this->resourceConfig->saveConfig('geoip/activation/website', '', 'default', 0);
            return;
        }
        $data = '';
        $web = '';
        $en = '';
        if (isset($res['dom']) && intval($res['c']) > 0 && intval($res['suc']) == 1) {
            $data = $this->encryptor->encrypt(base64_encode($this->zend->encode($resArray)));
            if (!$s) {
                if (isset($params['activation']['fields']['website']['value'])) {
                    $s = $params['activation']['fields']['website']['value'];
                }
            }
            $en = $res['suc'];
            if (isset($s) && $s != null) {
                $web = $this->encryptor->encrypt($data . implode(',', $s) . $data);
            } else {
                $web = $this->encryptor->encrypt($data . $data);
            }
        } else {
            $moduleStatus->saveConfig('geoip/activation/key', "", 'default', 0);
            $moduleStatus->saveConfig('geoip/general/active', 0, 'default', 0);
            $moduleStatus->saveConfig('geoip/activation/website', '', 'default', 0);
        }
        $this->resourceConfig->saveConfig('geoip/activation/data', $data, 'default', 0);
        $this->resourceConfig->saveConfig('geoip/activation/websites', $web, 'default', 0);
        $this->resourceConfig->saveConfig('geoip/activation/en', $en, 'default', 0);
        $this->resourceConfig->saveConfig('geoip/activation/installed', 1, 'default', 0);


        /*set popup style*/
        $style = $this->scopeConfig->getValue(
            "geoip/general/popup_style",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $this->resourceConfig->saveConfig('geoip/popup/style1', 0, 'default', 0);
        $this->resourceConfig->saveConfig('geoip/popup/style2', 0, 'default', 0);
        $this->resourceConfig->saveConfig('geoip/popup/style3', 0, 'default', 0);
        $this->resourceConfig->saveConfig('geoip/popup/style4', 0, 'default', 0);
        if($style==1) {
            $this->resourceConfig->saveConfig('geoip/popup/style1', 1, 'default', 0);
        } elseif ($style==2) {
            $this->resourceConfig->saveConfig('geoip/popup/style2', 1, 'default', 0);
        } elseif ($style==3) {
            $this->resourceConfig->saveConfig('geoip/popup/style3', 1, 'default', 0);
        } elseif ($style==4) {
            $this->resourceConfig->saveConfig('geoip/popup/style4', 1, 'default', 0);
        }
        /*end*/
        //refresh config cache after save
        $types = ['config','full_page'];
        foreach ($types as $type) {
            $this->_cacheTypeList->cleanType($type);
        }
        foreach ($this->_cacheFrontendPool as $cacheFrontend) {
            $cacheFrontend->getBackend()->clean();
        }
    }
}
