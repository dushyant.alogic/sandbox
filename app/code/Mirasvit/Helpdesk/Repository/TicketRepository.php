<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-helpdesk
 * @version   1.1.60
 * @copyright Copyright (C) 2018 Mirasvit (https://mirasvit.com/)
 */

namespace Mirasvit\Helpdesk\Repository;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\Exception\StateException;
use Mirasvit\Helpdesk\Repository;

class TicketRepository implements \Mirasvit\Helpdesk\Api\Repository\TicketRepositoryInterface {
	use \Mirasvit\Helpdesk\Repository\RepositoryFunction\GetList;

	/**
	 * @var \Mirasvit\Helpdesk\Model\Ticket[]
	 */
	protected $instances = [];
	/**
	 * @var \Mirasvit\Helpdesk\Model\ResourceModel\Ticket\CollectionFactory
	 */
	private $jsonResultFactory;


	protected $ticketCollectionFactory;
	public function __construct(
		\Mirasvit\Helpdesk\Model\TicketFactory $ticketFactory,
		\Mirasvit\Helpdesk\Model\ResourceModel\Ticket\CollectionFactory $ticketCollectionFactory,
		\Mirasvit\Helpdesk\Model\ResourceModel\Ticket $ticketResource,
		\Mirasvit\Helpdesk\Api\Data\TicketSearchResultsInterfaceFactory $searchResultsFactory,
		\Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory
	) {
		$this->objectFactory = $ticketFactory;
		$this->ticketCollectionFactory = $ticketCollectionFactory;
		$this->ticketResource = $ticketResource;
		$this->searchResultsFactory = $searchResultsFactory;
		$this->jsonResultFactory = $jsonResultFactory;
	}

	/**
	 * {@inheritdoc}
	 */
	public function save($ticket) {
		$this->ticketResource->save($ticket);

		return $ticket;
	}

	/**
	 * {@inheritdoc}
	 */
	public function get($ticketId) {
		if (!isset($this->instances[$ticketId])) {
			/** @var \Mirasvit\Helpdesk\Model\Ticket $ticket */
			$ticket = $this->objectFactory->create()->load($ticketId);
			if (!$ticket->getId()) {
				throw NoSuchEntityException::singleField('id', $ticketId);
			}
			$this->instances[$ticketId] = $ticket;
		}

		return $this->instances[$ticketId];
	}

	/**
	 * {@inheritdoc}
	 */
	public function getAll($ticketId = null, $fromDate = null) {

		$collection = $this->ticketCollectionFactory->create()->joinFields()->joinMessages();
		if ($ticketId) {
			$collection->addFieldToFilter('main_table.ticket_id', $ticketId);

		}
if($fromDate != null){

$fromDate = date('Y-m-d H:i:s', strtotime($fromDate));
$toDate = date('Y-m-d H:i:s', strtotime("now"));
		$collection->addFieldToFilter('main_table.created_at', array(
			'from' => $fromDate,
			'to' => $toDate,
			'date' => true,
		));
}
		//echo $collection->getSize();
		if (!$collection->getSize()) {
			return [['message' => 'No Records Found.']];
			//throw new NotFoundException(__('No Records Found.'));
		}
		return $collection->getData();

	}
	/**
	 * {@inheritdoc}
	 */
	public function delete($ticket) {
		try {
			$ticketId = $ticket->getId();
			$this->ticketResource->delete($ticket);
		} catch (\Exception $e) {
			throw new StateException(
				__(
					'Cannot delete ticket with id %1',
					$ticket->getId()
				),
				$e
			);
		}
		unset($this->instances[$ticketId]);

		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function deleteById($ticketId) {
		$ticket = $this->get($ticketId);

		return $this->delete($ticket);
	}
}
