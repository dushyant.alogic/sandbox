<?php
namespace Newsroom\Presskit\Model\ResourceModel\Image;

/**
 * Class Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Init
     */
    protected function _construct() // phpcs:ignore PSR2.Methods.MethodDeclaration
    {
        $this->_init(\Newsroom\Presskit\Model\Image::class, \Newsroom\Presskit\Model\ResourceModel\Image::class);
    }
}
