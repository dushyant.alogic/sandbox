<?php
namespace Newsroom\Presskit\Model\ResourceModel;

/**
 * Class Image
 */
class Image extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Init
     */
    protected function _construct() // phpcs:ignore PSR2.Methods.MethodDeclaration
    {
        $this->_init('newsroom_presskit_image', 'image_id');
    }
}
