<?php

namespace Newsroom\Presskit\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Image extends AbstractDb {
	protected function _construct() {
		return $this->_init('presskit_images', 'image_id');
	}
}