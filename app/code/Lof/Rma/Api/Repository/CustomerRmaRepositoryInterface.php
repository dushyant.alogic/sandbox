<?php
/**
 * LandOfCoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://www.landofcoder.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   LandOfCoder
 * @package    Lof_Rma
 * @copyright  Copyright (c) 2020 Landofcoder (http://www.LandOfCoder.com/)
 * @license    http://www.LandOfCoder.com/LICENSE-1.0.html
 */

namespace Lof\Rma\Api\Repository;

use Magento\Framework\Api\SearchCriteriaInterface;

interface CustomerRmaRepositoryInterface
{


    /**
     * Save rma
     * @param int $customerId
     * @param \Lof\Rma\Api\Data\RmaInterface $rma
     * @return \Lof\Rma\Api\Data\RmaInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save($customerId, \Lof\Rma\Api\Data\RmaInterface $rma);

    /**
     * Save Bundle RMA
     * @param int $customerId
     * @param \Lof\Rma\Api\Data\RmaInterface $rma
     * @return \Lof\Rma\Api\Data\RmaInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function saveBundle($customerId, \Lof\Rma\Api\Data\RmaInterface $rma);

    /**
     * Retrieve rma
     * @param int $customerId
     * @param string $rmaId
     * @return \Lof\Rma\Api\Data\RmaInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($customerId, $rmaId);
    
    /**
     * Retrieve rma matching the specified criteria.
     * @param int $customerId
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Lof\Rma\Api\Data\RmaSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        $customerId,
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );
}
