<?php 
/**
 * LandOfCoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://www.landofcoder.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   LandOfCoder
 * @package    Lof_Rma
 * @copyright  Copyright (c) 2020 Landofcoder (http://www.LandOfCoder.com/)
 * @license    http://www.LandOfCoder.com/LICENSE-1.0.html
 */
namespace Lof\Rma\Block\Adminhtml\Report;

class ReasonGrid extends \Magento\Backend\Block\Widget\Grid\Extended {

    /**
    * banner factory
    * @var \Magenhub\Chris\Model\ChrisFactory
    */
    protected $_chrisFactory;

    /**
    * Registry object
    * @var \Magento\Framework\Registry
    */
    protected $_coreRegistry;

    /**
    * [__construct description]
    * @param \Magento\Backend\Block\Template\Context $context [description]
    * @param \Magento\Backend\Helper\Data $backendHelper [description]
    * @param \Magenhub\Chris\Model\ChrisFactory $chrisFactory [description]
    * @param \Magento\Framework\Registry $coreRegistry [description]
    * @param array $data [description]
    */
    public function __construct(
    \Magento\Backend\Block\Template\Context $context, 
    \Magento\Backend\Helper\Data $backendHelper, 
    \Lof\Rma\Model\ResourceModel\Item\Collection $RmaFactory, 
    \Lof\Rma\Model\ResourceModel\Status\Collection $StatusFactory, 
    \Magento\Framework\Registry $coreRegistry,
     array $data = []
    ) {
        $this->_RmaFactory = $RmaFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->_StatusFactory = $StatusFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    protected function _construct() {
        parent::_construct();
        $this->setId('lofGrid');
        $this->setSaveParametersInSession(false);
        $this->setFilterVisibility(false);
        $this->setSortable(false);
        $this->setUseAjax(false);
    }

    protected function _prepareCollection() {
        if($params = $this->getRequest()->getParams()){
            $params['to'] = isset($params['to'])?$params['to']:date("Y-m-d");
            $params['from'] = isset($params['from'])?$params['from']:date("Y-m-d",strtotime("-1 month"));
            $endate =str_replace( '.', '-', $params['to'] ); 
            $startdate =str_replace( '.', '-', $params['from'] );              
            $name = isset($params['pr'])?$params['pr']:'';
        }
        else{
            $startdate = date("Y-m-d",strtotime("-1 month"));
            $endate = date("Y-m-d");
            $name = '';
        }
        $chart_block = $this->getLayout()->getBlock("dashboard.chart");
        if($chart_block){
            $collection = $chart_block->filterRmaItemCollection($name, 'created_at', $startdate, $endate);
        }else {
            $collection = $this->_RmaFactory
                ->addFieldToFilter(
                        'main_table.reason_id',
                        array('notnull' => true)
                    )
                ->setDateColumnFilter('created_at')
                ->addDateFromFilter($startdate)
                ->addDateToFilter( $endate)
                ->_getReasonSelectedColumns($name);
            if($name){
                $collection->addFieldToFilter('product.value', array('like' => '%' . $name . '%'));
            }
        }
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
    * @return $this
    */
    protected function _prepareColumns() {
        $this->addColumn(
        'chris_id', [
        'header' => __('Reason '),
        'type' => 'text',
        'index' => 'reason_name',
        'header_css_class' => 'col-id',
        'column_css_class' => 'col-id',
        'width' => '30px',
        ]
        );
        if(($this->getRequest()->getParam('pr')!='')){
         $this->addColumn(
        'item', [
        'header' => __('Item'),
        'index' => 'product_name',
        'class' => 'xxx',
        'width' => '50px',

        ]
        );
        }
        $this->addColumn(
        'rma', [
        'header' => __('Total Rma'),
        'index' => 'total_rma_cnt',
        'class' => 'xxx',
        'width' => '50px',
        ]
        );
         $this->addColumn(
        'request', [
        'header' => __('Total Request'),
        'index' => 'total_requested_cnt',
        'class' => 'xxx',
        'width' => '50px',
        ]
        );

        $this->addColumn(
        'return', [
        'header' => __('Total Return'),
        'index' => 'total_returned_cnt',
        'class' => 'xxx',
        'width' => '50px',
        ]
        );

        /*$status = $this->_StatusFactory->getData();
        foreach ($status as $value) {
             $this->addColumn(
        $value['name'], [
        'header' => __($value['name']),
        'class' => 'xxx',
        'width' => '100px',
        'index' => $value['status_id'].'_cnt'
        ]
        );
        }*/
       
        
        
         $this->addExportType('*/Report/ExportCsvRS', __('CSV'));
        $this->addExportType('*/Report/ExportExcelRS', __('Excel'));
        return parent::_prepareColumns();
    }
    protected function _toHtml(){
        if(isset($_GET["showchart"]) && $_GET["showchart"]==1){
            return "";
        }
        return parent::_toHtml();
    }
    /**/

}
?>