<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2021 Amasty (https://www.amasty.com)
 * @package Amasty_Preorder
 */


declare(strict_types=1);

namespace Amasty\Preorder\Model\OrderPreorder\Command;

use Amasty\Preorder\Api\Data\OrderInformationInterface;

interface SaveInterface
{
    public function execute(OrderInformationInterface $orderInformation): void;
}
