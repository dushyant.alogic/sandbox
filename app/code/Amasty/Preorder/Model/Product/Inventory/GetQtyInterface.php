<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2021 Amasty (https://www.amasty.com)
 * @package Amasty_Preorder
 */


declare(strict_types=1);

namespace Amasty\Preorder\Model\Product\Inventory;

interface GetQtyInterface
{
    public function execute(string $productSku, string $websiteCode): float;
}
