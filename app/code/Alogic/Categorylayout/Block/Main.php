<?php
namespace Alogic\Categorylayout\Block;
use Magento\Framework\View\Element\Template;

/**
 * Class Main
 */
class Main extends Template {
	/**
	 * @var \Magento\Framework\ObjectManagerInterface
	 */
	protected $_objectManager;

	protected $_categoryFactory;
	/**
	 * @param Template\Context $context
	 * @param array $data
	 */
	public function __construct(
		Template\Context $context, array $data = [],
		\Magento\Catalog\Model\CategoryFactory $categoryFactory,
		\Magento\Framework\ObjectManagerInterface $objectManager
	) {
		parent::__construct($context, $data);
		$this->_categoryFactory = $categoryFactory;
		$this->_objectManager = $objectManager;
	}
	function _prepareLayout() {
		$title = __('Browse By Category');

		$breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs');
		$breadcrumbsBlock->addCrumb(
			'home',
			[
				'label' => __('Home'),
				'title' => __('Go to Home Page'),
				'link' => $this->_storeManager->getStore()->getBaseUrl(),
			]
		);

		$breadcrumbsBlock->addCrumb('alogic_categorylayout', ['label' => $title, 'title' => $title]);

		$this->pageConfig->getTitle()->set($title);
		return parent::_prepareLayout();
	}
	public function catCollection() {
		$category = $this->_categoryFactory->create()->load(428);
		$menu = $category->getChildrenCategories();
		foreach ($menu as $cate) {
			$cate->setLinkCate($cate->getUrl());
		}
		return $menu;
	}
	public function getStore() {

		return $this->_storeManager->getStore();
	}

	public function childCatCollection($id) {
		$category = $this->_categoryFactory->create()->load($id);
		$menu = $category->getChildrenCategories();
		$_menu = [];
		foreach ($menu as $cate) {
			$_menu[] = $this->_categoryFactory->create()->load($cate->getId());
		}
		return $_menu;
	}
}
