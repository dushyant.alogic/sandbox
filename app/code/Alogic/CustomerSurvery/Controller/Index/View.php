<?php

namespace Alogic\CustomerSurvery\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\NotFoundException;
use Alogic\CustomerSurvery\Block\CustomerSurveryView;
use Alogic\CustomerSurvery\Model\CustomerSurveryFactory;

class View extends \Magento\Framework\App\Action\Action
{
    protected $fileFactory;
    protected $csvProcessor;
    protected $directoryList;
    protected $_customerSession;
    protected $_customersurvery;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\File\Csv $csvProcessor,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        CustomerSurveryFactory $customersurvery
    )
    {
        $this->fileFactory = $fileFactory;
        $this->csvProcessor = $csvProcessor;
        $this->_customerSession = $customerSession;
        $this->directoryList = $directoryList;
        $this->_customersurvery = $customersurvery;
        parent::__construct($context);
    }
    public function getCustomerSurveryCollection()
    {
        $customersurvery = $this->_customersurvery->create();
        $collection = $customersurvery->getCollection();
        $collection->addFieldToFilter('status','1');

        return $collection;
    }
    public function execute()
    {
        $fileName = 'visitors.csv';
        $filePath = $this->directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR)
            . "/" . $fileName;

        
        $personalData = $this->getPresonalData();

        $this->csvProcessor
            ->setDelimiter(';')
            ->setEnclosure('"')
            ->saveData(
                $filePath,
                $personalData
            );

        return $this->fileFactory->create(
            $fileName,
            [
                'type' => "filename",
                'value' => $fileName,
                'rm' => true,
            ],
            \Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR,
            'application/octet-stream'
        );
    }

    protected function getPresonalData()
    {
        $result = [];
        
        $result[] = [
            'fullname',
            'email',
            'phone',
            'country',
            'created_at',            
        ];

        foreach ($this->getCustomerSurveryCollection() as $survey) {
            $result[] = [
                
                $survey['fullname'],
                $survey['email'],
                $survey['phone'],
                $survey['country'],
                $survey['created_at'],
            ];
        }

        return $result;
    }
}