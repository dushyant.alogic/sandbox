<?php

namespace Alogic\Stockalerts\Controller\Index;

use Alogic\Stockalerts\Model\SubscriberFactory;
use Magento\Framework\App\Action\Context;

class Save extends \Magento\Framework\App\Action\Action {

	protected $_subscriber;
	private $storeManager;
	protected $_formKeyValidator;
/**
 * @var \Magento\Framework\Controller\Result\JsonFactory
 */
	private $jsonResultFactory;
	public function __construct(
		Context $context,
		SubscriberFactory $subscriber,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
		\Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory

	) {
		$this->_subscriber = $subscriber;
		$this->_formKeyValidator = $formKeyValidator;
		$this->storeManager = $storeManager;
		$this->jsonResultFactory = $jsonResultFactory;

		parent::__construct($context);
	}
	public function execute() {
		$result = $this->jsonResultFactory->create();
		$data = $this->getRequest()->getParams();
		if (!$this->_formKeyValidator->validate($this->getRequest())) {

			$data = ['error' => 'Your session has expired'];
			$result->setData($data);
			return $result;
		}
		$subscriber = $this->_subscriber->create();
		$collection = $subscriber->getCollection();

		$collection->addFieldToFilter('email', $data['email']);
		$collection->addFieldToFilter('product_id', $data['product_id']);
		if ($collection->count()) {
			$data = ['error' => 'You have already subscribed!'];

			$result->setData($data);
			return $result;
		}
		$data['store_id'] = $this->storeManager->getStore()->getId();
		$data['website_id'] = $this->storeManager->getStore()->getWebsiteId();
		$data['website_code'] = $this->storeManager->getWebsite()->getCode();

		$subscriber->setData($data);
		if ($subscriber->save()) {
			$data = ['success' => true];
			$result->setData($data);
			return $result;
		} else {
			$data = ['error' => 'Error saving data. Please try again.'];

			$result->setData($data);
			return $result;
		}

	}
}

