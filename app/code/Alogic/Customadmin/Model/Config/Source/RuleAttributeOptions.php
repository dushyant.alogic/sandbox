<?php
namespace Alogic\Customadmin\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\SalesRule\Model\ResourceModel\Rule\Collection;
class RuleAttributeOptions extends AbstractSource
{
    /**
     * @var \Magento\SalesRule\Model\ResourceModel\Rule\CollectionFactory
    */
    protected $ruleCollectionFactory;

     /**     
     * @param \Magento\SalesRule\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory     
     */
    public function __construct(
        \Magento\SalesRule\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory
    ) {
        $this->ruleCollectionFactory = $ruleCollectionFactory;
    }

    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {
        $collection = $this->ruleCollectionFactory->create();
        $items = $collection->getItems();
        $options = [];
        $options[] =['label' => 'None','value' => 0];
        foreach ($items as $ruleModel) {
            $ruleModel->load($ruleModel->getId());
            
            if($ruleModel->getData('product_rule')){
                $options[] =['label' => $ruleModel->getName(),'value' => $ruleModel->getId()];
            }
            
        }

        if (null === $this->_options) {
            $this->_options = $options;
        }
        return $this->_options;
    }
}
