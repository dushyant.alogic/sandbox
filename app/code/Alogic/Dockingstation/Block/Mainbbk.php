<?php
namespace Alogic\Dockingstation\Block;

/**
 * Class Main
 */
class Main extends \Magento\Framework\View\Element\Template {
	function _prepareLayout() {}
	protected $productCollectionFactory;
	protected $categoryFactory;
	protected $eavConfig;

	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
		\Magento\Catalog\Model\CategoryFactory $categoryFactory,
		\Magento\Eav\Model\Config $eavConfig,
		array $data = []
	) {
		$this->productCollectionFactory = $productCollectionFactory;
		$this->categoryFactory = $categoryFactory;
		$this->eavConfig = $eavConfig;
		parent::__construct($context, $data);
	}
	public function getProductCollection() {

		$storeid = 1;
		$post = $this->getRequest()->getParams();
		/*echo "<pre>";
			print_r($post);
		*/
		$collection = $this->productCollectionFactory->create();
		$collection->addAttributeToSelect('*');
		foreach ($post as $attribute_code => $value) {
			if (is_array($value)) {
				$collection->addAttributeToFilter($attribute_code, array('in' => $value));
			} else {
				$collection->addAttributeToFilter($attribute_code, array('eq' => $value));
			}
		}
		$collection->addAttributeToFilter('is_docking_station', array('eq' => 1));
		$collection->addAttributeToFilter('type_id', array('eq' => 'simple'));
/*		echo $collection->getSelect();
echo $collection->count();
die();
//$collection->addAttributeToFilter('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
//$collection->addAttributeToFilter('visibility', \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH);
//$collection->addAttributeToFilter('status', array('eq' => 1));
if (isset($resolution_type)) {
$collection->addFieldToFilter('resolution_type', array('eq' => $this->eavConfig->getAttribute('catalog_product', 'resolution_type')->getSource()->getOptionId($resolution_type)));
//$collection->addAttributeToFilter('resolution_type', array('eq' => $resolution_type));
}

//$collection->addStoreFilter($storeid);*/

		return $collection;
	}

}

