<?php

namespace Alogic\Customtheme\Plugin;

class InvoicePlugin {
	public function afterRegister(\Magento\Sales\Model\Order\Invoice\Item $subject, $result) {

		$writer = new \Laminas\Log\Writer\Stream(BP . '/var/log/invoicePlugin.log');
		$logger = new \Laminas\Log\Logger();
		$logger->addWriter($writer);
		$logger->info("Done");
		//$result->setCurrentSellerId($result->getOrderItem()->getCurrentSellerId());
		return $result;
	}
}

/*class ShipmentSavePlugin {
private $orderRepository;
public function __construct(\Magento\Sales\Api\OrderRepositoryInterface $orderRepository) {
$this->orderRepository = $orderRepository;
}
public function afterRegister(\Magento\Sales\Model\Order\Shipment $shipment) {
try {
$order = $this->orderRepository->get($shipment->getOrderId());
$order->addStatusHistoryComment('Order Complete', \Magento\Sales\Model\Order::STATE_COMPLETE);
} catch (\Exception $e) {

}
}
}*/