<?php
namespace Alogic\Customtheme\Block;
class Orders extends \Magento\Framework\View\Element\Template {
	protected $_registry;
	protected $transactions;
	protected $orderRepository;
	public function __construct(
		\Magento\Backend\Block\Template\Context $context,
		\Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
		\Magento\Sales\Api\Data\TransactionSearchResultInterfaceFactory $transactions,
		\Magento\Framework\Registry $registry,
		array $data = []
	) {
		$this->_registry = $registry;
		$this->transactions = $transactions;
		$this->orderRepository = $orderRepository;
		parent::__construct($context, $data);
	}

	public function _prepareLayout() {
		parent::_prepareLayout();
		$this->pageConfig->getTitle()->set(__('Custom Pagination'));
		if ($this->getCustomCollection()) {
			$pager = $this->getLayout()->createBlock(
				'Magento\Theme\Block\Html\Pager',
				'custom.history.pager'
			)->setAvailableLimit([5 => 5, 10 => 10, 15 => 15, 20 => 20])
				->setShowPerPage(true)->setCollection(
				$this->getCollection()
			);
			$this->setChild('pager', $pager);
			$this->getCollection()->load();
		}
		return $this;
	}
	public function getPagerHtml() {
		return $this->getChildHtml('pager');
	}
	public function getCurrentCategory() {
		return $this->_registry->registry('current_category');
	}
	public function getOrder($orderId) {
		return $this->orderRepository->get($orderId);
	}
	public function getCollection() {
		$collection = $this->transactions->create();
	}
	public function getAllTransactions() {
		//$transactions = $this->transactions->create()->addOrderIdFilter($orderId);
		$transactions = $this->transactions->create()->setOrder(
			'created_at',
			'desc'
		)->getItems();
		return $transactions;
	}
	public function getCurrentProduct() {
		return $this->_registry->registry('current_product');
	}

}
?>