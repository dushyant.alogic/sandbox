<?php
namespace Alogic\Customtheme\App\Router;
class NoRouteHandler implements \Magento\Framework\App\Router\NoRouteHandlerInterface {
	public function process(\Magento\Framework\App\RequestInterface $request) {
		$requestValue = trim($request->getPathInfo(), '/');
		$productreq = explode('/', $requestValue);
		$last_url = end($productreq);
		$last_url_array = explode('-', $last_url);
		$i = 0;
		$searchString = '';

		foreach ($last_url_array as $value) {
			if ($i > 3) {
				break;
			}

			$searchString .= '-' . $value;
			$i++;
		}

		$request->setParam('q', ltrim($searchString, '-'));

		$request->setModuleName('catalogsearch')->setControllerName('result')->setActionName('index');
		return true;
	}
}
