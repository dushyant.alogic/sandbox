<?php

namespace Alogic\Customtheme\Observer;
use Magento\Framework\DB\TransactionFactory;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Service\InvoiceService;

class OrderObserver implements ObserverInterface {
	/**
	 * @var \Magento\Sales\Model\OrderFactory
	 */
	protected $orderModel;
	protected $invoiceService;
	protected $transactionFactory;
	/**
	 * @var \Magento\Sales\Model\Order\Email\Sender\OrderSender
	 */
	protected $orderSender;

	/**
	 * @param \Magento\Sales\Model\OrderFactory $orderModel
	 * @param \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender
	 *
	 * @codeCoverageIgnore
	 */
	public function __construct(
		\Magento\Sales\Model\OrderFactory $orderModel,
		InvoiceService $invoiceService,
		TransactionFactory $transactionFactory,
		\Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender

	) {
		$this->invoiceService = $invoiceService;
		$this->transactionFactory = $transactionFactory;
		$this->orderModel = $orderModel;
		$this->orderSender = $orderSender;

	}

	public function execute(\Magento\Framework\Event\Observer $observer) {

		$orderIds = $observer->getEvent()->getOrderIds();

		if (count($orderIds)) {
			$writer = new \Laminas\Log\Writer\Stream(BP . '/var/log/successorders.log');
			$logger = new \Laminas\Log\Logger();
			$logger->addWriter($writer);

			$logger->info("----- Order Id  " . $orderIds[0]);

			$order = $this->orderModel->create()->load($orderIds[0]);
			if (!$order->getId()) {
				throw new \Magento\Framework\Exception\LocalizedException(__('The order no longer exists.'));
			}
			if (!$order->canInvoice()) {
				throw new \Magento\Framework\Exception\LocalizedException(
					__('The order does not allow an invoice to be created.')
				);
			}

			$invoice = $this->invoiceService->prepareInvoice($order);
			if (!$invoice) {
				throw new \Magento\Framework\Exception\LocalizedException(__('We can\'t save the invoice right now.'));
			}
			if (!$invoice->getTotalQty()) {
				throw new \Magento\Framework\Exception\LocalizedException(
					__('You can\'t create an invoice without products.')
				);
			}
			$invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_ONLINE);
			$invoice->register();
			$invoice->getOrder()->setCustomerNoteNotify(false);
			$invoice->getOrder()->setIsInProcess(true);
			$order->addStatusHistoryComment('Automatically INVOICED', false);
			$transactionSave = $this->transactionFactory->create()->addObject($invoice)->addObject($invoice->getOrder());
			$transactionSave->save();
			$logger->info("----- Order Id  Invoiced" . $order->getId());
			//$this->orderSender->send($order, true);

		}

	}
}
