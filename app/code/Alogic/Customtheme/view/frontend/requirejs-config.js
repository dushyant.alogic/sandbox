var config = {
	paths: {
        customSlider: 'Alogic_Customtheme/js/jquery.custom-scrollbar.min'
    },
    map: {
        '*': {
            "Magento_Paypal/template/payment/paypal-express.html": 
            'Alogic_Customtheme/template/payment/paypal-express.html',

             "Magento_SalesRule/template/payment/discount.html": 
            'Alogic_Customtheme/template/payment/discount.html',

            "Magento_Checkout/template/shipping-address/address-renderer/default.html": 
            'Alogic_Customtheme/template/shipping-address/address-renderer/default.html',

            /*"Magento_Catalog/js/related-products":"Alogic_Customtheme/js/related-products",
            "ajaxQty": 'Alogic_Customtheme/js/cartQtyUpdate',
            "Magento_Checkout/template/shipping.html": 'Alogic_Customtheme/template/shipping.html',
            "Magento_Checkout/template/shipping-address/shipping-method-list.html": 
            'Alogic_Customtheme/template/shipping-address/shipping-method-list.html',
            "Magento_Checkout/template/shipping-address/shipping-method-item.html": 
            'Alogic_Customtheme/template/shipping-address/shipping-method-item.html',
            "Magento_Checkout/template/summary/totals.html": 
            'Alogic_Customtheme/template/summary/totals.html',
            "Magento_Tax/template/checkout/summary/shipping.html": 
            'Alogic_Customtheme/template/checkout/summary/shipping.html' ,
            "Magento_Paypal/template/payment/paypal-express.html": 
            'Alogic_Customtheme/template/payment/paypal-express.html',
            "Magento_Payment/template/payment/cc-form.html": 
            'Alogic_Customtheme/template/payment/cc-form.html',

            "Magento_Checkout/template/cart/shipping-estimation.html": 
            'Alogic_Customtheme/template/checkout/cart/shipping-estimation.html',

            "Magento_Checkout/template/cart/shipping-rates.html": 
            'Alogic_Customtheme/template/checkout/cart/shipping-rates.html',

            "Magento_Persistent/template/remember-me.html": 
            'Alogic_Customtheme/template/remember-me.html',

            "PayPal_Braintree/template/payment/paypal.html": 
            'Alogic_Customtheme/template/payment/paypal.html',

            "PayPal_Braintree/template/payment/form.html": 
            'Alogic_Customtheme/template/payment/form.html',

            "Magento_Tax/template/checkout/summary/grand-total.html": 
            'Alogic_Customtheme/template/checkout/summary/grand-total.html',
            "Magento_Tax/template/checkout/summary/subtotal.html": 
            'Alogic_Customtheme/template/checkout/summary/subtotal.html',
            
            "Magento_Checkout/template/registration.html": 
            'Alogic_Customtheme/template/registration.html'*/
            
            }
        }
    };
