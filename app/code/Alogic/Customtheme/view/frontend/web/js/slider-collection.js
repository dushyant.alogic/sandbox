require([
        'jquery',
        'rokanthemes/owl'
    ], function ($) {
        $(document).ready(function ($) {
  $(".js-product-grid").owlCarousel({
    lazyLoad:true,
    autoPlay : false,
    items : 3,
    itemsDesktop : [1199,3],
    itemsDesktopSmall : [980,3],
    itemsTablet: [768,3],
    itemsMobile : [479,1],
    slideSpeed : 500,
    paginationSpeed : 500,
    rewindSpeed : 500,
    navigation : true,
    stopOnHover : true,
    pagination :false,
    scrollPerPage:true,
  });
});
    });
