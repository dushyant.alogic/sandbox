<?php
namespace Alogic\Productswitcher\Plugin\Swatches\Block\Product\Renderer;

class Configurable {
	public function afterGetJsonConfig(\Magento\Swatches\Block\Product\Renderer\Configurable $subject, $result) {

		$jsonResult = json_decode($result, true);
		$jsonResult['skus'] = [];
		$jsonResult['barcodes'] = [];
		$jsonResult['name'] = [];

		foreach ($subject->getAllowProducts() as $simpleProduct) {
			$jsonResult['skus'][$simpleProduct->getId()] = $simpleProduct->getSku();
			$jsonResult['barcodes'][$simpleProduct->getId()] = $simpleProduct->getBarcodeEan();
			$jsonResult['names'][$simpleProduct->getId()] = $simpleProduct->getName();
		}
		$jsonResult['optionStock'] = $this->getOptionStocks($subject);
		$result = json_encode($jsonResult);
		return $result;
	}

	protected function getOptionStocks($subject) {
		$stocks = [];
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$stockObject = $objectManager->get('Magento\CatalogInventory\Api\StockRegistryInterface');

		foreach ($subject->getAllowProducts() as $product) {
			$productStockObj = $stockObject->getStockItem($product->getId());
			$isInStock = $productStockObj['is_in_stock'];
			$stocks[$product->getId()] = ['stockStatus' => $isInStock];
		}
		return $stocks;
	}
}