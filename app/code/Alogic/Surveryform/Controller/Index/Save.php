<?php
/**
 * @category   Alogic
 * @package    Alogic_Surveryform
 * @author     dushyanta.joshi@gmail.com
 * @copyright  This file was generated by using Module Creator(http://code.vky.co.in/magento-2-module-creator/) provided by VKY <viky.031290@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Alogic\Surveryform\Controller\Index;

use Magento\Framework\App\Action\Context;
use Alogic\Surveryform\Model\SurveryformFactory;

class Save extends \Magento\Framework\App\Action\Action
{
	/**
     * @var Surveryform
     */
    protected $_surveryform;

    public function __construct(
		Context $context,
        SurveryformFactory $surveryform
    ) {
        $this->_surveryform = $surveryform;
        parent::__construct($context);
    }
	public function execute()
    {
        $data = $this->getRequest()->getParams();
    	$surveryform = $this->_surveryform->create();
        $surveryform->setData($data);
        if($surveryform->save()){
            $this->messageManager->addSuccessMessage(__('You saved the data.'));
        }else{
            $this->messageManager->addErrorMessage(__('Data was not saved.'));
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('surveryform/index/index');
        return $resultRedirect;
    }
}
