<?php

namespace Alogic\Wheretobuy\Controller\Index;

use Alogic\Resellers\Model\ResellersFactory;
use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Framework\App\Action\Action {
	/**
	 * @var Resellers
	 */
	protected $_resellers;
	protected $resultPageFactory;
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		//\Intesols\Distributor\Model\DistributorFactory $distributorFactory,
		\Alogic\Distributors\Model\DistributorFactory $distributorFactory,
		\Intesols\Distributor\Helper\Data $helperData,
		ResultFactory $resultFactory,
		ResellersFactory $resellers,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory) {
		$this->distributorFactory = $distributorFactory;
		$this->_resellers = $resellers;
		$this->helperData = $helperData;
		$this->resultPageFactory = $resultPageFactory;
		$this->resultFactory = $resultFactory;
		parent::__construct($context);
	}

	public function execute() {

		$resultPage = $this->resultFactory->create(ResultFactory::TYPE_LAYOUT);

		$block = $resultPage->getLayout()->getBlock('alogic_wheretobuy_block_main');

		$store_country_code = $this->helperData->getCountryCode();
		$params = $this->getRequest()->getParams();

		$resellers = $this->_resellers->create();
		$distributorCollection = $resellers->getCollection();
		$distributorCollection->addFieldToFilter('status', '1');

		if (isset($params['search']) && $params['search'] != '') {
			$distributorCollection = $distributorCollection->addFieldToFilter('name', array('like' => '%' . $params['search'] . '%'));
		}
		$distributorCollection->setOrder('position', 'ASC')->load();
		/*echo $distributorCollection->getSelect();*/
		$avil_country = [];
		foreach ($distributorCollection as $distributor) {

			$countries = explode(',', $distributor['country']);
			foreach ($countries as $country) {
				if ($country != '') {
					$avil_country[] = $country;
				}
			}

		}
		$avil_country = array_values(array_unique($avil_country));

		$changeSelection = 0;

		if (isset($params['country']) && $params['country']) {
			$selectedCountry = $params['country'];
		} else {
			$selectedCountry = $store_country_code;
		}
		$thisReseller = [];
		$thisDistributor = [];
		$thisNationalRetailer = [];

		foreach ($distributorCollection as $distributor) {

			if ((strpos($distributor['country'], $selectedCountry) || strpos($distributor['country'], $selectedCountry) === 0) && $distributor['type'] == 1 && $distributor['status'] == 1) {
				$thisReseller[] = $distributor->getData();
			} elseif ((strpos($distributor['country'], $selectedCountry) || strpos($distributor['country'], $selectedCountry) === 0) && $distributor['type'] == 2 && $distributor['status'] == 1) {
				$thisDistributor[] = $distributor->getData();
			} elseif ((strpos($distributor['country'], $selectedCountry) || strpos($distributor['country'], $selectedCountry) === 0) && $distributor['type'] == 3 && $distributor['status'] == 1) {
				$thisNationalRetailer[] = $distributor->getData();
			}
		}
		$data = [
			'distributors' => $thisDistributor,
			'resellers' => $thisReseller,
			'nationalRetailers' => $thisNationalRetailer,
		];
		if (isset($params['partner'])) {
			if ($params['partner'] == '1') {
				$data['resellers'] = [];
				$data['nationalRetailers'] = [];
			} elseif ($params['partner'] == '2') {
				$data['distributors'] = [];
				$data['nationalRetailers'] = [];
			} elseif ($params['partner'] == '3') {
				$data['distributors'] = [];
				$data['resellers'] = [];
			}
		}
		$totalRecords = count($data['resellers']) + count($data['nationalRetailers']) + count($data['distributors']);
		if ($totalRecords == 0) {
			$data = [];
		}
		$block->setData('buyers', $data);

		return $resultPage;
	}
}
