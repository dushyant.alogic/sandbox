<?php
namespace Alogic\Wheretobuy\Block;

/**
 * Class Main
 */
class Main extends \Magento\Framework\View\Element\Template {
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		//\Intesols\Distributor\Model\DistributorFactory $distributorFactory,
		\Alogic\Distributors\Model\DistributorFactory $distributorFactory,
		\Intesols\Distributor\Helper\Data $helperData) {
		$this->distributorFactory = $distributorFactory;
		$this->helperData = $helperData;

		parent::__construct($context);
	}
	public function _prepareLayout() {}

	public function GetSellers($value = '') {

		$store_country_code = $this->helperData->getCountryCode();
		$distributors = $this->distributorFactory->create();
		$distributorCollection = $distributors->getCollection();
		if (isset($params['search']) && $params['search'] != '') {
			$distributorCollection = $distributorCollection->addFieldToFilter('name', array('like' => '%' . $params['search'] . '%'));
		}

		$distributorCollection->setOrder('position', 'ASC')->load();
		$avil_country = [];
		foreach ($distributorCollection as $distributor) {

			$countries = explode(',', $distributor->getCountry());
			foreach ($countries as $country) {
				if ($country != '') {
					$avil_country[] = $country;
				}
			}

		}
		$avil_country = array_values(array_unique($avil_country));

		$changeSelection = 0;

		if (isset($params['country']) && $params['country']) {
			$selectedCountry = $params['country'];
		} else {
			$selectedCountry = $store_country_code;
		}
		$thisReseller = [];
		$thisDistributor = [];
		$thisNationalRetailer = [];

		foreach ($distributorCollection as $distributor) {

			if ((strpos($distributor->getCountry(), $selectedCountry) || strpos($distributor->getCountry(), $selectedCountry) === 0) && $distributor->getType() == 1 && $distributor->getStatus() == 1) {
				$thisReseller[] = $distributor->getData();
			} elseif ((strpos($distributor->getCountry(), $selectedCountry) || strpos($distributor->getCountry(), $selectedCountry) === 0) && $distributor->getType() == 2 && $distributor->getStatus() == 1) {
				$thisDistributor[] = $distributor->getData();
			} elseif ((strpos($distributor->getCountry(), $selectedCountry) || strpos($distributor->getCountry(), $selectedCountry) === 0) && $distributor->getType() == 3 && $distributor->getStatus() == 1) {
				$thisNationalRetailer[] = $distributor->getData();
			}
		}

		return $data = [
			'distributors' => $thisDistributor,
			'resellers' => $thisReseller,
			'nationalRetailers' => $thisNationalRetailer,
		];
	}
}
