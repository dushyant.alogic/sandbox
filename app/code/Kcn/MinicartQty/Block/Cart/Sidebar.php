<?php
namespace Kcn\MinicartQty\Block\Cart;
use Magento\CatalogInventory\Api\Data\StockItemInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;

class Sidebar extends \Magento\Checkout\Block\Cart\Sidebar {
	protected $_coreRegistry = null;
	protected $_blogHelper;
	protected $_post;
	protected $_category;
	protected $productRepository;
	protected $checkoutSession;
	protected $httpContext;
	protected $storeManager;
	protected $formKey;
	protected $_stockItemRepository;
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
		\Magento\Customer\Model\Session $customerSession,
		\Magento\Checkout\Model\Session $checkoutSession,
		\Magento\Catalog\Helper\Image $imageHelper,
		\Magento\Framework\Data\Form\FormKey $formKey,
		\Magento\Customer\CustomerData\JsLayoutDataProviderPoolInterface $jsLayoutDataProvider,
		StockRegistryInterface $stockRegistry,
		\Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		array $data = []
	) {
		$this->productRepository = $productRepository;
		$this->checkoutSession = $checkoutSession;
		$this->storeManager = $storeManager;
		$this->formKey = $formKey;
		$this->stockRegistry = $stockRegistry;
		$this->_stockItemRepository = $stockItemRepository;
		parent::__construct($context, $customerSession, $checkoutSession, $imageHelper, $jsLayoutDataProvider, $data);
	}
	/**
	 * get form key
	 *
	 * @return string
	 */
	public function getFormKey() {
		return $this->formKey->getFormKey();
	}
	public function getStockItem($productId) {
		return $this->_stockItemRepository->get($productId);
	}
	public function getCacheLifetime() {
		return null;
	}

	/**
	 * get stock status
	 *
	 * @param int $productId
	 * @return bool
	 */
	public function getStockStatus($productId) {
		/** @var StockItemInterface $stockItem */
		$stockItem = $this->stockRegistry->getStockItem($productId);
		$isInStock = $stockItem ? $stockItem->getIsInStock() : false;
		return $isInStock;
	}
	public function getCrosssellProducts() {

		$quote = $this->getQuoteData();
		$crossSells = [];
		foreach ($quote->getAllItems() as $_item) {
			$crossSells[] = $this->getCrosssellProductsList($_item->getProductId());
		}
		return $crossSells;
	}
	public function getAjaxCrosssellProducts() {

		$quote = $this->getQuoteData();
		$crossSells = [];
		foreach ($quote->getAllItems() as $_item) {
			$crossSells[] = $this->getCrosssellProductsList($_item->getProductId());
		}
		return $crossSells;
	}
	/**
	 * @param \Magento\Catalog\Model\Product $product
	 * @return string
	 */
	public function getProductPrice(\Magento\Catalog\Model\Product $product) {
		$priceRender = $this->getPriceRender();

		$price = '';
		if ($priceRender) {
			$price = $priceRender->render(
				\Magento\Catalog\Pricing\Price\FinalPrice::PRICE_CODE,
				$product,
				[
					'include_container' => true,
					'display_minimal_price' => true,
					'zone' => \Magento\Framework\Pricing\Render::ZONE_ITEM_LIST,
					'list_category_page' => true,
				]
			);
		}

		return $price;
	}
	/**
	 * Specifies that price rendering should be done for the list of products
	 * i.e. rendering happens in the scope of product list, but not single product
	 *
	 * @return \Magento\Framework\Pricing\Render
	 */
	protected function getPriceRender() {
		return $this->getLayout()->getBlock('product.price.render.default')
			->setData('is_product_list', true);
	}
	/**
	 * get cross sell products
	 *
	 * @return array
	 */
	public function getCrosssellProductsList($productId) {
		//$productId = 4451;
		$crossSellProduct = [];
		try {
			$product = $this->productRepository->getById($productId);
			$crossSell = $product->getCrossSellProducts();

			if (count($crossSell)) {
				foreach ($crossSell as $productItem) {
					$crossSellProduct[] = $this->productRepository->getById($productItem->getId());
				}
			}
		} catch (\Exception $exception) {
			//$this->logger->error($exception->getMessage());
		}

		return $crossSellProduct;
	}
	public function getQuoteData() {
		$this->checkoutSession->getQuote();
		if (!$this->hasData('quote')) {
			$this->setData('quote', $this->checkoutSession->getQuote());
		}
		return $this->_getData('quote');
	}
	public function getCartOutOfStock() {
		$quote = $this->getQuoteData();
		foreach ($quote->getAllVisibleItems() as $_item) {
			$status = $this->getStockStatus($_item->getProductId());
			if ($status == false) {
				return false;
			}

		}
		return true;
	}
	/**
	 * Get update cart item url
	 *
	 * @return string
	 * @codeCoverageIgnore
	 * @SuppressWarnings(PHPMD.RequestAwareBlockMethod)
	 */
	public function getUpdateItemQtyUrl() {
		return $this->getUrl('minicartqty/sidebar/updateItemQty', ['_secure' => $this->getRequest()->isSecure()]);
	}
}
