<?php
/**
 * Copyright © OpenGento, All rights reserved.
 * See LICENSE bundled with this library for license details.
 */
declare(strict_types=1);

namespace Opengento\CountryStore\Model\Resolver;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\HTTP\PhpEnvironment\Request;
use Magento\Store\Model\ScopeInterface;
use Opengento\CountryStore\Api\CountryRepositoryInterface;
use Opengento\CountryStore\Api\CountryResolverInterface;
use Opengento\CountryStore\Api\Data\CountryInterface;
use Opengento\CountryStore\Model\CountryResolver;

final class HttpHeaderValue implements CountryResolverInterface
{
    private const CONFIG_PATH_COUNTRY_HTTP_HEADER_NAME = 'country/resolver/http_header_name';

    public const RESOLVER_CODE = 'httpHeaderValue';

    private $request;

    private $scopeConfig;

    private $resolverFactory;

    private $countryRepository;

    public function __construct(
        Request $request,
        ScopeConfigInterface $scopeConfig,
        ResolverFactory $resolverFactory,
        CountryRepositoryInterface $countryRepository
    ) {
        $this->request = $request;
        $this->scopeConfig = $scopeConfig;
        $this->resolverFactory = $resolverFactory;
        $this->countryRepository = $countryRepository;
    }

    public function getCountry(): CountryInterface
    {
        $countryCode = (string) $this->request->getServerValue(
            $this->scopeConfig->getValue(self::CONFIG_PATH_COUNTRY_HTTP_HEADER_NAME, ScopeInterface::SCOPE_WEBSITE)
        );

        return $countryCode
            ? $this->countryRepository->get($countryCode)
            : $this->resolverFactory->get(CountryResolver::DEFAULT_COUNTRY_RESOLVER_CODE)->getCountry();
    }
}
