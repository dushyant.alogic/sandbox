<?php
namespace Kiwicommerce\Shippingbar\Block\Cart;
use Magento\Framework\View\Element\Template;
use Magento\SalesRule\Api\RuleRepositoryInterface;

class Sidebar extends Template {
	protected $_storeManager;
	protected $rule;
	protected $ruleRepository;
	/**
	 * @var \Kiwicommerce\Jobs\Helper\Data
	 */
	private $helper;

	/**
	 * Sidebar constructor.
	 * @param Template\Context $context
	 * @param \Kiwicommerce\Jobs\Helper\Data $helper
	 * @param array $data
	 */
	public function __construct(
		Template\Context $context,
		\Kiwicommerce\Shippingbar\Helper\Data $helper,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\SalesRule\Model\Rule $rule,
		RuleRepositoryInterface $ruleRepository,
		array $data = []
	) {
		parent::__construct($context, $data);
		$this->_storeManager = $storeManager;
		$this->ruleRepository = $ruleRepository;
		$this->rule = $rule;
		$this->helper = $helper;
	}
/**
 * Get website identifier
 *
 * @return string|int|null
 */
	public function getWebsiteId() {
		return $this->_storeManager->getStore()->getWebsiteId();
	}
	public function getConfigForShippingBar() {

		$websiteId = $this->getWebsiteId();
		if ($websiteId == 1 || $websiteId == 2) {
			$rule_id = 2;
		} elseif ($websiteId == 5) {
			$rule_id = 10;
		} elseif ($websiteId == 4) {
			$rule_id = 9;
		} elseif ($websiteId == 3) {
			$rule_id = 8;
		}

		$salesRule = $this->rule->load($rule_id);
		//$salesRule->getIsActive();

		//To get conditions

		$ser = $salesRule->getConditionsSerialized();

		$abovePrice = json_decode($ser)->conditions[0]->value;

		return $abovePrice;
		return $this->helper->getPriceForShippingBar();
	}
}