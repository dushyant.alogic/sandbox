<?php
/**
 * PL Development.
 *
 * @category    PL
 * @author      Linh Pham <plinh5@gmail.com>
 * @copyright   Copyright (c) 2016 PL Development. (http://www.polacin.com)
 */
namespace PL\Commweb\Model;

class Direct extends \Magento\Payment\Model\Method\Cc
{
    const METHOD_CODE                       = 'commweb_direct';

    protected $_code                    	= self::METHOD_CODE;

    protected $_isGateway                   = true;

    protected $_canCapture                  = true;

    protected $_canCapturePartial           = true;

    protected $_canRefund                   = true;

    protected $_canUseCheckout = true;

    protected $_canUseInternal = true;

    protected $plLogger;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \PL\Commweb\Logger\Logger $plLogger,
        array $data = array()
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $moduleList,
            $localeDate,
            null,
            null,
            $data
        );
        $this->plLogger = $plLogger;
    }

    public function init()
    {
        $configArray = [];
        $configArray["certificateVerifyPeer"] = $this->getConfigData('ssl_disable');
        $configArray["certificateVerifyHost"] = 2;
        $configArray["gatewayUrl"] = $this->getConfigData('gateway_url');
        $configArray["merchantId"] = $this->getMerchantid();
        $configArray["apiUsername"] = "merchant.".$this->getMerchantid();
        $configArray["password"] = $this->getApiPassword();
        return $configArray;
    }

    public function getMerchantid()
    {
        return $this->getConfigData('merchant_id');
    }

    public function getApiPassword()
    {
        return $this->getConfigData('api_password');
    }

    public function capture(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        $this->plLogger->debug("hahah1: ");
        $order = $payment->getOrder();
        $billing = $order->getBillingAddress();

        $request = [];
        $request['order.id'] =  $order->getIncrementId();
        $request['transaction.id'] = $order->getIncrementId();
        $request['apiOperation'] = 'PAY';
        $request['sourceOfFunds.type'] = 'CARD';
        $request['sourceOfFunds.provided.card.number'] = $payment->getCcNumber();
        $request['sourceOfFunds.provided.card.expiry.month']= sprintf('%02d', $payment->getCcExpMonth());
        $request['sourceOfFunds.provided.card.expiry.year'] = substr($payment->getCcExpYear(), 2, 2);
        $request['sourceOfFunds.provided.card.securityCode'] = $payment->getCcCid();
        $request['order.amount'] = $amount;
        $request['order.currency'] = strtoupper($order->getBaseCurrencyCode());
        $request['merchant'] = $this->getMerchantid();
        $request['apiPassword'] = $this->getApiPassword();
        $request['apiUsername'] = 'merchant.'.$request['merchant'];

        $query_string = '';
        foreach ($request as $name=>$value) {
            $query_string.= $name."=".$value."&";
        }
        $dataFields = rtrim($query_string,"&");
        $merchantObj = new \PL\Commweb\Model\Api\Configuration($this->init());
        $parserObj = new \PL\Commweb\Model\Api\Connection($merchantObj);
        $response = $parserObj->SendTransaction($merchantObj, $dataFields);
        if ($this->getConfigData('debug')) {
            $this->plLogger->debug($response);
        }
        $pairArray = explode("&", $response);
        $responseArray = [];
        foreach ($pairArray as $pair) {
            $param = explode("=", $pair);
            $responseArray[urldecode($param[0])] = urldecode($param[1]);
        }
        if (array_key_exists("result", $responseArray)) {
            $result = $responseArray["result"];
           if ($result == 'SUCCESS') {
               $payment
                   ->setTransactionId($responseArray['transaction.id'])
                   ->setLastTransId($responseArray['transaction.id'])
                   ->setParentTransactionId($responseArray['transaction.id'])
                   ->setIsTransactionClosed(0);
           } else {
               throw new \Magento\Framework\Exception\LocalizedException(__('Transaction was not successful'));
           }
        } else {
            throw new \Magento\Framework\Exception\LocalizedException($pairArray['response.acquirerMessage']);
        }

        return $this;
    }
}