<?php
/**
 * PL Development.
 *
 * @category    PL
 * @author      Linh Pham <plinh5@gmail.com>
 * @copyright   Copyright (c) 2016 PL Development. (http://www.polacin.com)
 */
namespace PL\Commweb\Model;

class HostedCheckout extends \Magento\Payment\Model\Method\AbstractMethod
{
    const METHOD_CODE = 'commweb_hostedcheckout';

    protected $_code = self::METHOD_CODE;

    protected $_infoBlockType = 'PL\Commweb\Block\Info\HostedCheckout';

    protected $_canAuthorize = true;

    /**
     * @var bool
     */
    protected $_canRefund = false;

    /**
     * @var bool
     */
    protected $_canRefundInvoicePartial = false;

    /**
     * @var bool
     */
    protected $_canUseInternal = false;

    /**
     * @var bool
     */
    protected $_isInitializeNeeded = true;

    /**
     * @var
     */
    protected $encryptor;

    /**
     * @var
     */
    protected $request;

    /**
     * @var
     */
    protected $urlBuilder;

    /**
     * @var
     */
    protected $plLogger;

    /**
     * @var
     */
    protected $jsonHelper;

    /**
     * @var
     */
    protected $orderSender;

    /**
     * @var
     */
    protected $invoiceSender;

    /**
     * @var \PL\Commweb\Helper\Data
     */
    protected $mpgsHelper;

    protected $checkoutSession;

    public function __construct(
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\UrlInterface $urlBuilder,
        \PL\Commweb\Helper\Data $mpgsHelper,
        \PL\Commweb\Logger\Logger $plLogger,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender,
        \Magento\Sales\Model\Order\Email\Sender\InvoiceSender $invoiceSender,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data
        );
        $this->urlBuilder = $urlBuilder;
        $this->mpgsHelper = $mpgsHelper;
        $this->plLogger = $plLogger;
        $this->request = $request;
        $this->encryptor = $encryptor;
        $this->jsonHelper = $jsonHelper;
        $this->orderSender = $orderSender;
        $this->invoiceSender = $invoiceSender;
        $this->checkoutSession = $checkoutSession;
    }

    public function validate()
    {
        /** @noinspection PhpDeprecationInspection */
        parent::validate();
        $paymentInfo = $this->getInfoInstance();
        /** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */
        if ($paymentInfo instanceof \Magento\Sales\Model\Order\Payment) {
            $paymentInfo->getOrder()->getBaseCurrencyCode();
        } else {
            $paymentInfo->getQuote()->getBaseCurrencyCode();
        }
        return $this;
    }

    public function getMerchantid()
    {
        return $this->getConfigData('merchant_id');
    }

    public function getApiPassword()
    {
        return $this->getConfigData('api_password');
    }

    public function getCheckoutRedirectUrl()
    {
        return $this->urlBuilder->getUrl(
            'commweb/hostedcheckout/redirect',
            ['_secure' => $this->request->isSecure()]
        );
    }

    public function getCheckoutSuccessUrl()
    {
        return $this->urlBuilder->getUrl(
            'commweb/hostedcheckout/success',
            ['_secure' => $this->request->isSecure()]
        );
    }

    public function getCheckoutCancelUrl()
    {
        return $this->urlBuilder->getUrl(
            'commweb/hostedcheckout/cancel',
            ['_secure' => $this->request->isSecure()]
        );
    }

    public function getGatewayUrl($nvp=false)
    {
        if ($nvp) {
            return $this->getConfigData('gateway_url').'/api/nvp';
        } else {
            return $this->getConfigData('gateway_url');
        }
    }

    /**
     * @param string $paymentAction
     * @param object $stateObject
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function initialize($paymentAction, $stateObject)
    {
        if ($paymentAction == 'order') {
            $order = $this->getInfoInstance()->getOrder();
            $order->setCustomerNoteNotify(false);
            $order->setCanSendNewEmailFlag(false);
            $comment = __('Redirecting to the payment gateway. Order #%1', $order->getIncrementId());
            $order->setCustomerNote($comment);
            $stateObject->setIsNotified(false);
            $stateObject->setState(\Magento\Sales\Model\Order::STATE_PENDING_PAYMENT);
            $stateObject->setStatus(\Magento\Sales\Model\Order::STATE_PENDING_PAYMENT);
        }
    }


    public function getHostedCheckoutSessionId()
    {
        $paymentInfo = $this->getInfoInstance();
        $order = $paymentInfo->getOrder();

        $configArray = [];
        $configArray["certificateVerifyPeer"] = $this->getConfigData('ssl_disable');
        $configArray["certificateVerifyHost"] = 2;
        $configArray["gatewayUrl"] = $this->getGatewayUrl(true);
        $configArray["merchantId"] = $this->getMerchantid();
        $configArray["apiUsername"] = "merchant.".$this->getMerchantid();
        $configArray["password"] = $this->getApiPassword();

        $request = [];
        $request['apiOperation'] = 'CREATE_CHECKOUT_SESSION';
        $request['apiPassword'] = $configArray["password"];
        $request['apiUsername'] = $configArray["apiUsername"];
        $request['merchant'] = $configArray["merchantId"];
        $request['order.id'] =  $order->getIncrementId();
        $request['order.amount'] = $order->getGrandTotal();
        $request['order.currency'] = $order->getBaseCurrencyCode();
        $query_string = '';
        foreach ($request as $name=>$value) {
            $query_string.= $name."=".$value."&";
        }
        $dataFields = rtrim($query_string,"&");
        $merchantObj = new \PL\Commweb\Model\Api\Configuration($configArray);
        $parserObj = new \PL\Commweb\Model\Api\Connection($merchantObj);
        $response = $parserObj->SendTransaction($merchantObj, $dataFields);
        $pairArray = explode("&", $response);
        $responseArray = [];
        foreach ($pairArray as $pair) {
            $param = explode("=", $pair);
            $responseArray[urldecode($param[0])] = urldecode($param[1]);
        }
        if (array_key_exists("session.id", $responseArray)) {
            return $responseArray["session.id"];
        } else {
            throw new \Magento\Framework\Exception\LocalizedException(__('Gateway Error'));
        }
    }

    public function retrieveOrder($order)
    {
        //$order->getBaseGrandTotal();
        ///$order->getBaseCurrencyCode();

        $configArray = [];
        $configArray["certificateVerifyPeer"] = $this->getConfigData('ssl_disable');
        $configArray["certificateVerifyHost"] = 2;
        $configArray["gatewayUrl"] = $this->getGatewayUrl(true);
        $configArray["merchantId"] = $this->getMerchantid();
        $configArray["apiUsername"] = "merchant.".$this->getMerchantid();
        $configArray["password"] = $this->getApiPassword();

        $request = [];
        $request['apiOperation'] = 'RETRIEVE_ORDER';
        $request['apiPassword'] = $configArray["password"];
        $request['apiUsername'] = $configArray["apiUsername"];
        $request['merchant'] = $configArray["merchantId"];
        $request['order.id'] =  $order->getIncrementId();
        if ($this->getConfigData('debug')) {
            $this->plLogger->debug(print_r($request, 1));
        }
        $query_string = '';
        foreach ($request as $name=>$value) {
            $query_string.= $name."=".$value."&";
        }
        $dataFields = rtrim($query_string,"&");
        $merchantObj = new \PL\Commweb\Model\Api\Configuration($configArray);
        $parserObj = new \PL\Commweb\Model\Api\Connection($merchantObj);
        $response = $parserObj->SendTransaction($merchantObj, $dataFields);
        if ($this->getConfigData('debug')) {
            $this->plLogger->debug($response);
        }
        $pairArray = explode("&", $response);
        $responseArray = [];
        foreach ($pairArray as $pair) {
            $param = explode("=", $pair);
            $responseArray[urldecode($param[0])] = urldecode($param[1]);
        }
        if ($this->getConfigData('debug')) {
            $this->plLogger->debug(print_r($responseArray, 1));
        }
        return $responseArray;
    }

    public function acceptTransaction($order, $response)
    {
        if ($order->getId()) {
            $additionalData = $this->jsonHelper->jsonEncode($response);
            $order->getPayment()->setTransactionId($response['id']);
            $order->getPayment()->setLastTransId($response['id']);
            $order->getPayment()->setAdditionalInformation('payment_additional_info', $additionalData);
            $order->setStatus(\Magento\Sales\Model\Order::STATE_PROCESSING);
            $note = __('Approved the payment online. Order ID: "%1"', $response['id']);
            $order->setState(\Magento\Sales\Model\Order::STATE_PROCESSING);
            $order->addStatusHistoryComment($note);
            $order->save();
            $this->orderSender->send($order);
            if (!$order->hasInvoices() && $order->canInvoice()) {
                $invoice = $order->prepareInvoice();
                if ($invoice->getTotalQty() > 0) {
                    $invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_ONLINE);
                    $invoice->setTransactionId($order->getPayment()->getTransactionId());
                    $invoice->register();
                    $invoice->addComment(__('Automatic invoice.'), false);
                    $invoice->save();
                    $this->invoiceSender->send($invoice);
                }
            }
        }
    }

    public function cancelTransaction($order)
    {

        if ($order->getId()) {
            $order->getPayment()->setTransactionId($order->getIncrementId());
            $order->getPayment()->setLastTransId($order->getIncrementId());
            $note = __('Payment Gateway was declined. Transaction ID: "%1"', $order->getIncrementId());
            $order->setState(\Magento\Sales\Model\Order::STATE_CANCELED);
            $order->setStatus(\Magento\Sales\Model\Order::STATE_CANCELED);
            $order->setCustomerNoteNotify(false);
            $order->addStatusHistoryComment($note);
            $order->cancel()->save();
        }
    }

}