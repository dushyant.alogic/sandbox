<?php
/**
 * PL Development.
 *
 * @category    PL
 * @author      Linh Pham <plinh5@gmail.com>
 * @copyright   Copyright (c) 2016 PL Development. (http://www.polacin.com)
 */
namespace PL\Commweb\Controller\Hostedcheckout;

class Success extends \PL\Commweb\Controller\Hostedcheckout
{
    protected $hostedCheckout;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \PL\Commweb\Helper\Data $mpgsHelper,
        \PL\Commweb\Logger\Logger $plLogger,
        \PL\Commweb\Model\HostedCheckout $hostedCheckout
    ) {
        parent::__construct(
            $context,
            $orderFactory,
            $checkoutSession,
            $storeManager,
            $mpgsHelper,
            $plLogger
        );
        $this->hostedCheckout = $hostedCheckout;
    }

    public function execute()
    {
        $incrementId = $this->checkoutSession->getLastRealOrderId();
        $order = $this->orderFactory->create()->loadByIncrementId($incrementId);
        $result = $this->hostedCheckout->retrieveOrder($order);
        $params = [];
        $params['sourceOfFunds.provided.card.brand'] =  $result['sourceOfFunds.provided.card.brand'];
        $params['sourceOfFunds.provided.card.nameOnCard'] =  $result['sourceOfFunds.provided.card.nameOnCard'];
        $params['sourceOfFunds.provided.card.number'] =  $result['sourceOfFunds.provided.card.number'];
        $params['status'] = $result['status'];
        $params['id'] = $result['id'];
        if (isset($result['result']) && $result['result'] == 'SUCCESS') {
            $this->hostedCheckout->acceptTransaction($order, $params);
            $this->_redirect('checkout/onepage/success');
        } else {
            $this->messageManager->addError(__('You have cancelled the order. Please try again'));
            $this->_redirect('checkout/cart');
        }
    }
}