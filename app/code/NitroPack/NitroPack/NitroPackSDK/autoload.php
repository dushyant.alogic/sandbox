<?php
require_once dirname(__FILE__) . "/vendor/autoload.php";

spl_autoload_register(function ($class) {
    $file = str_replace("\\", DIRECTORY_SEPARATOR, $class) . ".php";
    $path = __DIR__ . DIRECTORY_SEPARATOR . $file;
    if (file_exists($path)) {
        @file_put_contents(__DIR__.'/autoload.txt', "Found: $path" . PHP_EOL, FILE_APPEND);
        require_once $path;
    } else {
        @file_put_contents(__DIR__.'/autoload.txt', "Not found: $path" . PHP_EOL, FILE_APPEND);
    }
});
