<?php
namespace NitroPack\NitroPack\Controller\Adminhtml\Connect;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

use NitroPack\NitroPack\Controller\Adminhtml\StoreAwareAction;
use NitroPack\NitroPack\Api\NitroServiceInterface;
use NitroPack\NitroPack\Helper\AdminFrontendUrl;

use NitroPack\Url as NitropackUrl;

//use \NitroPack\SDK\NitroPack;

class Save extends StoreAwareAction {

	protected $nitro;
	protected $urlHelper;
	protected $resultJsonFactory;
	protected $request;

	protected $siteId = null;
	protected $siteSecret = null;
	protected $errors = array();

	public function __construct(
		Context $context,
		NitroServiceInterface $nitro,
		AdminFrontendUrl $urlHelper
	) {
		parent::__construct($context, $nitro);
		$this->nitro = $nitro;
		$this->resultJsonFactory = $this->_objectManager->create(JsonFactory::class);
		$this->urlHelper = $urlHelper;
		$this->request = $this->getRequest();
	}

	public function nitroExecute() {
		if ($this->validateSiteCredentials()) {
			try {
				$this->saveSettings();
				$this->nitro->reload($this->getStore()->getCode());

				$urls = $this->getWebhookUrls();

				foreach ($urls as $type => $url) {
					$this->nitro->getApi()->setWebhook($type, $url);
				}

				$eventUrl = $this->nitro->integrationUrl('extensionEvent');
				$this->nitro->nitroEvent('connect', $eventUrl, $this->store);
				$eventSent = $this->nitro->nitroEvent('enable_extension', $eventUrl, $this->store);

				return $this->resultJsonFactory->create()->setData(array(
					'connected' => true,
					'redirect' => $this->getUrlWithStore('NitroPack/settings/index', array(
						'store' => $this->store->getId()
					)),
					'event' => $eventSent
				));
			} catch (\NitroPack\SDK\ChallengeVerificationException $e) {
				$this->nitro->disconnect($this->getStore()->getCode());
				$errorMessage = $e->getMessage();
				return $this->resultJsonFactory->create()->setData(array(
					'connected' => false,
					'errors' => $errorMessage
				));
			} catch (\NitroPack\SDK\ChallengeProcessingException $e) {
				$this->nitro->disconnect($this->getStore()->getCode());
				$errorMessage = $e->getMessage();
				return $this->resultJsonFactory->create()->setData(array(
					'connected' => false,
					'errors' => $errorMessage
				));
			} catch (\NitroPack\SDK\ConfigFetcherException $e) {
				$this->nitro->disconnect($this->getStore()->getCode());
				$errorMessage = $e->getMessage();
				return $this->resultJsonFactory->create()->setData(array(
					'connected' => false,
					'errors' => $errorMessage
				));
			} catch (\Exception $e) {
				$errorMessage = $e->getMessage();
				return $this->resultJsonFactory->create()->setData(array(
					'connected' => false,
					'errors' => $errorMessage
				));
			}

		} else {
			return $this->resultJsonFactory->create()->setData(array(
				'connected' => false,
				'errors' => implode(',',$this->errors)
			));
		}
	}

	protected function validateSiteCredentials() {
		$siteId = trim($this->request->getPostValue('nitro_site_id', ""));
		$siteSecret = trim($this->request->getPostValue('nitro_site_secret', ""));

		if (!$siteId) {
			$this->errors['nitro_site_id'] = 'Site ID cannot be blank';
		}

		if (!$siteSecret) {
			$this->errors['nitro_site_secret'] = 'Site secret cannot be blank';
		}

		if (!preg_match("/^([a-zA-Z]{32})$/", $siteId)) {
			$this->errors['nitro_site_id'] = 'Not a valid Site ID';
		}

		if (!preg_match("/^([a-zA-Z0-9]{64})$/", trim($siteSecret))) {
			$this->errors['nitro_site_secret'] = 'Not a valid Site secret';
		}

		$result = empty($this->errors);
		if ($result) {
			$this->siteId = $siteId;
			$this->siteSecret = $siteSecret;
		}

		return $result;
	}

	protected function saveSettings() {
		$this->nitro->setSiteId($this->siteId);
		$this->nitro->setSiteSecret($this->siteSecret);
		$this->nitro->persistSettings($this->store->getCode());
	}

	protected function getWebhookUrls() {
		$urls = array(
			'config'      => new NitropackUrl($this->urlHelper->getUrl('NitroPack/Webhook/Config')),
			'cache_clear' => new NitropackUrl($this->urlHelper->getUrl('NitroPack/Webhook/CacheClear')),
			'cache_ready' => new NitropackUrl($this->urlHelper->getUrl('NitroPack/Webhook/CacheReady'))
		);

		return $urls;
	}

}
?>
