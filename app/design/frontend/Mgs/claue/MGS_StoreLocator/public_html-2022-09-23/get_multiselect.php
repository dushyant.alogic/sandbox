<?php
require './app/bootstrap.php';
$bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $_SERVER);
$_objectManager = $bootstrap->getObjectManager();
$state = $_objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
$registry = $_objectManager->get('Magento\Framework\Registry');
$registry->register('isSecureArea', true);

//Store id of exported products, This is useful when we have multiple stores.
$store_id = 1;
$eavConfigAttribute = $_objectManager->get('\Magento\Catalog\Model\ResourceModel\Eav\Attribute');

$attributeInfo = $eavConfigAttribute->getCollection();

$all_attributes = [];
$eavConfig = $_objectManager->get('\Magento\Eav\Model\Config');
foreach ($attributeInfo as $attributes) {
	$attribute_code = $attributes->getAttributeCode();

	$attribute = $eavConfig->getAttribute('catalog_product', $attribute_code);

	$attribute_type = $attribute->getFrontendInput();

	if ($attribute_type == 'multiselect') {
		$options = $attribute->getSource()->getAllOptions();

		foreach ($options as $option) {
			if (is_object($option['label'])) {
				$all_attributes[$attribute_code][] = $option['label']->getText();
			} else {
				$all_attributes[$attribute_code][] = $option;
			}

		}
	}

	// You can get all fields of attribute here
}

// $fp = fopen("export.csv", "w+");
// $data = array();
// foreach ($all_attributes as $attribute_code => $attribute) {
// 	foreach ($attribute as $value) {
// 		$data[] = $attribute_code;
// 		$data[] = $value['label'];
// 		fputcsv($fp, $data);
// 	}

// }
$fp = fopen("export_multiple.csv", "w+");

foreach ($all_attributes as $attribute_code => $attribute) {
	$nAttributes = [];
	$nAttributes[] = $attribute_code;
	echo "<pre>";
	$valuesString = '';
	foreach ($attribute as $value) {

		$valuesString = $valuesString . ',' . $value['label'];
	}

	$nAttributes[] = ltrim($valuesString, ', ,');
	fputcsv($fp, $nAttributes);
}
// Loop through file pointer and a line
// foreach ($nAttributes as $fields) {
// 	fputcsv($fp, $fields);
// }

fclose($fp);

echo "<pre>";
print_r($all_attributes);
