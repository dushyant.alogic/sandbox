<?php
/**
 * PL Development.
 *
 * @category    PL
 * @author      Linh Pham <plinh5@gmail.com>
 * @copyright   Copyright (c) 2016 PL Development. (http://www.polacin.com)
 */
namespace PL\Commweb\Model\Api;

class Connection {
	protected $curlObj;

	function __construct($merchantObj) {
		// initialise cURL object/options
		$this->curlObj = curl_init();
		$this->ConfigureCurlProxy($merchantObj);
		$this->ConfigureCurlCerts($merchantObj);
	}

	function __destruct() {
		curl_close($this->curlObj);
	}

	public function FormRequestUrl($merchantObj) {
		$gatewayUrl = $merchantObj->GetGatewayUrl();
		$gatewayUrl .= "/version/" . $merchantObj->GetVersion();
		$merchantObj->SetGatewayUrl($gatewayUrl);
		return $gatewayUrl;
	}

	public function SendTransaction($merchantObj, $request) {
		curl_setopt($this->curlObj, CURLOPT_POSTFIELDS, $request);
		curl_setopt($this->curlObj, CURLOPT_URL, $this->FormRequestUrl($merchantObj));
		curl_setopt($this->curlObj, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen($request)));
		curl_setopt($this->curlObj, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded;charset=UTF-8"));
		curl_setopt($this->curlObj, CURLOPT_RETURNTRANSFER, TRUE);
		if ($merchantObj->GetDebug()) {
			curl_setopt($this->curlObj, CURLOPT_HEADER, TRUE);
			curl_setopt($this->curlObj, CURLINFO_HEADER_OUT, TRUE);
		}
		$response = curl_exec($this->curlObj);
		if ($merchantObj->GetDebug()) {
			$requestHeaders = curl_getinfo($this->curlObj);
			$response = $requestHeaders["request_header"] . $response;
		}
		if (curl_error($this->curlObj)) {
			$response = "cURL Error: " . curl_errno($this->curlObj) . " - " . curl_error($this->curlObj);
		}
		return $response;
	}

	protected function ConfigureCurlProxy($merchantObj) {
		if ($merchantObj->GetProxyServer() != "") {
			curl_setopt($this->curlObj, CURLOPT_PROXY, $merchantObj->GetProxyServer());
			curl_setopt($this->curlObj, $merchantObj->GetProxyCurlOption(), $merchantObj->GetProxyCurlValue());
		}

		if ($merchantObj->GetProxyAuth() != "") {
			curl_setopt($this->curlObj, CURLOPT_PROXYUSERPWD, $merchantObj->GetProxyAuth());
		}
	}
	protected function ConfigureCurlCerts($merchantObj) {
		if ($merchantObj->GetCertificatePath() != "") {
			curl_setopt($this->curlObj, CURLOPT_CAINFO, $merchantObj->GetCertificatePath());
		}

		curl_setopt($this->curlObj, CURLOPT_SSL_VERIFYPEER, $merchantObj->GetCertificateVerifyPeer());
		curl_setopt($this->curlObj, CURLOPT_SSL_VERIFYHOST, $merchantObj->GetCertificateVerifyHost());
	}
}
