<?php

namespace Amasty\AdvancedReview\Model\OptionSource\Widget;

use Magento\Framework\Option\ArrayInterface;

class Type implements ArrayInterface
{
    const RANDOM = 0;
    const RECENT = 1;

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::RECENT, 'label'=> __('Recent')],
            ['value' => self::RANDOM, 'label'=> __('Random')]
        ];
    }
}
