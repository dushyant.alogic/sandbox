<?php

declare (strict_types = 1);

namespace Alogic\Categorybanner\Plugin\Cms\Model\Page\DataProvider\AfterGetData;

use Alogic\Categorybanner\Model\BannerUploader;
use Magento\Cms\Model\Page\DataProvider;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class ModifyBannerDataPlugin
 */
class ModifyBannerDataPlugin {
	/**
	 * @var StoreManagerInterface
	 */
	private $storeManager;

	/**
	 * ModifyBannerDataPlugin constructor.
	 * @param StoreManagerInterface $storeManager
	 */
	public function __construct(StoreManagerInterface $storeManager) {
		$this->storeManager = $storeManager;
	}

	/**
	 * @param DataProvider $subject
	 * @param $loadedData
	 * @return array
	 * @throws \Magento\Framework\Exception\NoSuchEntityException
	 */
	public function afterGetData(
		DataProvider $subject,
		$loadedData
	) {
		/** @var array $loadedData */
		if (is_array($loadedData) && count($loadedData) == 1) {
			foreach ($loadedData as $key => $item) {
				if (isset($item['cms_banner']) && $item['cms_banner']) {
					$imageArr = [];
					$imageArr[0]['name'] = 'Image';
					$imageArr[0]['url'] = $this->storeManager->getStore()
						->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) .
					BannerUploader::IMAGE_PATH . DIRECTORY_SEPARATOR . $item['cms_banner'];
					$loadedData[$key]['cms_banner'] = $imageArr;
				}
			}
		}

		return $loadedData;
	}
}