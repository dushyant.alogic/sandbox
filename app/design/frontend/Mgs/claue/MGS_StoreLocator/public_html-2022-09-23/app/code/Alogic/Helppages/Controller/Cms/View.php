<?php
namespace Alogic\Helppages\Controller\Cms;
class View extends \Magento\Framework\App\Action\Action {

	protected $resultPageFactory;
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory) {
		$this->resultPageFactory = $resultPageFactory;
		parent::__construct($context);
	}

	public function execute() {
		$resultPageFactory = $this->resultPageFactory->create();
		$identifier = '';
		if ($identifier = $this->getRequest()->getParam('page')) {
			$block = $resultPageFactory->getLayout()->getBlock('alogic_helppages_block_main');
			$block->setData('identifier', $identifier);
		}

		// Add page title
		$resultPageFactory->getConfig()->getTitle()->set(__('Help'));

		return $resultPageFactory;

	}
	protected function getFaqId() {
		return $this->getRequest()->getParam('id');
	}
}
