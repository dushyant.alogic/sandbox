<?php

namespace Amasty\ReviewersDiscounts\Test\Unit\Model\Condition;

use Amasty\ReviewersDiscounts\Model\Condition\ReviewedProducts;
use Amasty\ReviewersDiscounts\Model\ResourceModel\Review as ReviewResource;
use Amasty\ReviewersDiscounts\Test\Unit\Traits\ObjectManagerTrait;
use Amasty\ReviewersDiscounts\Test\Unit\Traits\ReflectionTrait;
use Magento\Framework\DataObject;
use Magento\Framework\Model\AbstractModel;

/**
 * Class ReviewedProductsTest
 *
 * @see ReviewedProducts
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * phpcs:ignoreFile
 */
class ReviewedProductsTest extends \PHPUnit\Framework\TestCase
{
    use ObjectManagerTrait;
    use ReflectionTrait;

    /**
     * @var ReviewedProducts
     */
    private $model;

    public function setUp()
    {
        $this->model = $this->getObjectManager()->getObject(ReviewedProducts::class);
    }

    /**
     * @covers ReviewedProducts::validate
     *
     * @dataProvider validateDataProvider
     *
     * @throws \ReflectionException
     */
    public function testValidate($value, $operator, $customerId, $valueForValidate, $isQuote, $expectedResult)
    {
        $this->model->setData('value', $value);
        $this->model->setData('operator', $operator);

        $modelForValidate = $this->createPartialMock(AbstractModel::class, ['getCustomerId', 'getQuote']);
        if ($isQuote) {
            $quote = $this->getObjectManager()->getObject(DataObject::class, ['data' => ['customer_id' => $customerId]]);
            $modelForValidate->expects($this->any())->method('getQuote')->willReturn($quote);
        } else {
            $modelForValidate->expects($this->any())->method('getCustomerId')->willReturn($customerId);
        }

        $reviewResource = $this->createPartialMock(ReviewResource::class, ['getReviewedProducts']);
        $reviewResource->expects($this->any())->method('getReviewedProducts')->willReturn($valueForValidate);

        $this->setProperty($this->model, 'reviewResource', $reviewResource, ReviewedProducts::class);

        $this->assertEquals($expectedResult, $this->model->validate($modelForValidate));
    }

    public function testGetValueParsed()
    {
        $this->model->setData('value','x');

        $this->assertEquals(['x'], $this->model->getValueParsed());
    }

    public function testIsArrayOperatorType()
    {
        $this->assertEquals(true, $this->model->isArrayOperatorType());
    }

    /**
     * Data provider for validate test
     * @return array
     */
    public function validateDataProvider()
    {
        return [
            [
                'value' => 'Test1',
                'operator' => '==',
                'customer_id' => 1,
                'reviewed_products' => ['Test1'],
                'is_quote' => true,
                'expected_result' => true
            ],
            [
                'value' => ['Test1', 'Test3'],
                'operator' => '==',
                'customer_id' => 1,
                'reviewed_products' => ['Test4'],
                'is_quote' => false,
                'expected_result' => false
            ],
            [
                'value' => ['Test1', 'Test3'],
                'operator' => '()',
                'customer_id' => 1,
                'reviewed_products' => ['Test4', 'Test3'],
                'is_quote' => true,
                'expected_result' => true
            ],
            [
                'value' => ['Test1', 'Test3', 'Test4'],
                'operator' => '()',
                'customer_id' => 1,
                'reviewed_products' => ['Test4', 'Test3'],
                'is_quote' => true,
                'expected_result' => true
            ],
            [
                'value' => ['x'],
                'operator' => '>=',
                'customer_id' => null,
                'reviewed_products' => ['x'],
                'is_quote' => true,
                'expected_result' => false
            ]
        ];
    }
}
