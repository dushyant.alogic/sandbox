<?php
/**
 * @category   Alogic
 * @package    Alogic_Resellers
 * @author     dushyant.joshi@alogic.co
 * @copyright  This file was generated by using Module Creator(http://code.vky.co.in/magento-2-module-creator/) provided by VKY <viky.031290@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Alogic\Resellers\Block\Adminhtml\Items\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;

class Main extends Generic implements TabInterface {
	protected $_wysiwygConfig;

	public function __construct(
		\Magento\Backend\Block\Template\Context $context,
		\Magento\Framework\Registry $registry,
		\Magento\Framework\Data\FormFactory $formFactory,
		\Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
		array $data = []
	) {
		$this->_wysiwygConfig = $wysiwygConfig;
		parent::__construct($context, $registry, $formFactory, $data);
	}

	/**
	 * {@inheritdoc}
	 */
	public function getTabLabel() {
		return __('Item Information');
	}

	/**
	 * {@inheritdoc}
	 */
	public function getTabTitle() {
		return __('Item Information');
	}

	/**
	 * {@inheritdoc}
	 */
	public function canShowTab() {
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function isHidden() {
		return false;
	}

	/**
	 * Prepare form before rendering HTML
	 *
	 * @return $this
	 * @SuppressWarnings(PHPMD.NPathComplexity)
	 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
	 */
	protected function _prepareForm() {
		$model = $this->_coreRegistry->registry('current_alogic_resellers_items');
		/** @var \Magento\Framework\Data\Form $form */
		$form = $this->_formFactory->create();
		$form->setHtmlIdPrefix('item_');
		$fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Item Information')]);
		if ($model->getId()) {
			$fieldset->addField('id', 'hidden', ['name' => 'id']);
		}
		$fieldset->addField(
			'name',
			'text',
			['name' => 'name', 'label' => __('Name'), 'title' => __('Name'), 'required' => true]
		);

		$options = ['1' => __('Reseller'), '2' => __('Distributor'), '3' => __('National Retailer')];
		$fieldset->addField(
			'type',
			'select',
			['name' => 'type', 'label' => __('Type'), 'title' => __('Type'), 'options' => $options, 'required' => true]
		);
		$fieldset->addField(
			'country',
			'multiselect',
			[
				'name' => 'country',
				'label' => __('Country'),
				'id' => 'country',
				'title' => __('Country'),
				'values' => $this->getCountries(),
				'class' => 'country',
				'required' => false,
			]
		);

		$fieldset->addField(
			'search_string',
			'text',
			['name' => 'search_string', 'label' => __('Search String'), 'title' => __('Search String'), 'required' => true]
		);
		$fieldset->addField(
			'logo_url',
			'text',
			['name' => 'logo_url', 'label' => __('URL'), 'title' => __('URL'), 'required' => true]
		);
		$fieldset->addField(
			'image',
			'image',
			[
				'name' => 'image',
				'label' => __('Logi'),
				'title' => __('Logo'),
				'required' => false,
			]
		);
		$fieldset->addField(
			'position',
			'text',
			['name' => 'position', 'label' => __('Position'), 'title' => __('Position'), 'required' => true]
		);
		$fieldset->addField(
			'status',
			'select',
			['name' => 'status', 'label' => __('Status'), 'title' => __('Status'), 'options' => [0 => 'Disable', 1 => 'Enable'], 'required' => true]
		);

		$form->setValues($model->getData());
		$this->setForm($form);
		return parent::_prepareForm();
	}
	/**
	 * Get Grid row status type labels array.
	 * @return array
	 */
	public function getCountries() {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		//$countryHelper = $objectManager->get('Magento\Directory\Model\Config\Source\Country');
		$countryFactory = $objectManager->get('\Magento\Directory\Model\ResourceModel\Country\CollectionFactory')->create()->loadByStore();

		//$options = ['1' => __('Reseller'),'2' => __('Distributor')];
		$options = $countryFactory->toOptionArray();
		return $options;
	}
}
