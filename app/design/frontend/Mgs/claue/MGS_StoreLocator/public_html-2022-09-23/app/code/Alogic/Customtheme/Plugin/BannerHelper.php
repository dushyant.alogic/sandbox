<?php
namespace Alogic\Customtheme\Plugin;

class BannerHelper {
	protected $storeManager;

	public function __construct(
		\Magento\Backend\Block\Template\Context $context,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		array $data = []
	) {
		$this->storeManager = $storeManager;

	}
	public function getStoreId() {
		return $this->storeManager->getStore()->getId();
	}
	public function afterGetBannerCollection($subject, $result) {
		$storeId = $this->getStoreId();
		$result->addFieldToFilter('store_ids', $storeId);
		$result->addFieldToFilter('banner_type', 1);
		$result->addOrder('sortorder', 'ASC');
		return $result;
	}

}