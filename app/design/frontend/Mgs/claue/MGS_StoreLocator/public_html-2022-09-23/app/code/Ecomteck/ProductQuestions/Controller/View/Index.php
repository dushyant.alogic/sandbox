<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_ProductQuestions
 * @copyright   Copyright (c) 2019 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */

namespace Ecomteck\ProductQuestions\Controller\View;

/**
 * Customer questions controller
 */
class Index extends \Magento\Framework\App\Action\Action {
	/**
	 * Customer session model
	 *
	 * @var \Magento\Customer\Model\Session
	 */
	protected $resultPageFactory;

	/**
	 *
	 * @param \Magento\Framework\App\Action\Context $context
	 * @param \Magento\Customer\Model\Session $customerSession
	 */
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory
	) {
		$this->resultPageFactory = $resultPageFactory;
		parent::__construct($context);
	}

	public function execute() {
		$resultPageFactory = $this->resultPageFactory->create();
		/** @var Template $block */
		$block = $resultPageFactory->getLayout()->getBlock('ecomm_productquestions_block_main');
		$id = $this->getQuestionId();
		if ($id) {
			try {
				$model = $this->_objectManager->create('Ecomteck\ProductQuestions\Model\Question')->load($id);
			} catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
				$this->messageManager->addError(__('This Question no longer exists.'));
				$this->_redirect('*/*/*');
				return;
			}
		} else {
			/** @var \MW\EasyFaq\Model\Faq $model */
			$model = $this->_objectManager->create('Ecomteck\ProductQuestions\Model\Question');
		}
		$answers = $resultPageFactory->getLayout()
			->createBlock('Ecomteck\ProductQuestions\Block\Product\View\ListView')
			->getAnswerList($model);

		$model->setAnswers($answers);

		$block->setData('item', $model);
		// Add page title
		$resultPageFactory->getConfig()->getTitle()->set(__('Help'));

		return $resultPageFactory;

	}

	protected function getQuestionId() {
		return $this->getRequest()->getParam('id');
	}
}
