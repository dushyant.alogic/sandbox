<?php

function setCustomAttribute($product, $code, $value)
{
    $product->setData($code, $value);
    $product->getResource()->saveAttribute($product, $code);
}
