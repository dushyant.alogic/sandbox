<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2021 Amasty (https://www.amasty.com)
 * @package Amasty_Preorder
 */


declare(strict_types=1);

namespace Amasty\Preorder\Model\Product\RetrieveNote;

use Magento\Catalog\Api\Data\ProductInterface;

interface GetAttributeValueInterface
{
    public function execute(ProductInterface $product): string;
}
