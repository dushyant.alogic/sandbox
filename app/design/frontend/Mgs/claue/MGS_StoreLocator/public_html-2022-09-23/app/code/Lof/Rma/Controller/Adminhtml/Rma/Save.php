<?php
/**
 * LandOfCoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://www.landofcoder.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category   LandOfCoder
 * @package    Lof_Rma
 * @copyright  Copyright (c) 2020 Landofcoder (http://www.LandOfCoder.com/)
 * @license    http://www.LandOfCoder.com/LICENSE-1.0.html
 */


namespace Lof\Rma\Controller\Adminhtml\Rma;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
use Lof\Rma\Controller\Adminhtml\Rma;

class Save extends Rma {
    /**
     * Authorization level of a basic admin session
     */
    const ADMIN_RESOURCE = 'Lof_Rma::rma_rma_save';

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var \Lof\Rma\Model\AttachmentRepository
     */
    protected $attachmentRepository;

    /**
     * Save constructor.
     *
     * @param \Lof\Rma\Helper\Data                               $dataHelper
     * @param \Lof\Rma\Model\RmaFactory                          $rmaFactory
     * @param \Lof\Rma\Model\ItemFactory                         $itemFactory
     * @param \Lof\Rma\Model\AttachmentFactory                   $AttachmentFactory
     * @param \Magento\Sales\Model\OrderFactory                  $orderFactory
     * @param \Magento\Framework\Event\ManagerInterface          $eventManager
     * @param \Lof\Rma\Api\Repository\RmaRepositoryInterface     $rmaRepository
     * @param \Lof\Rma\Api\Repository\MessageRepositoryInterface $messageRepository
     * @param \Magento\Framework\Registry                        $registry
     * @param \Magento\Framework\Api\SearchCriteriaBuilder       $searchCriteriaBuilder
     * @param \Lof\Rma\Model\AttachmentRepository                $attachmentRepository
     * @param \Magento\Backend\App\Action\Context                $context
     * @param \Lof\Rma\Helper\Help                               $helper
     */
    public function __construct(
        \Lof\Rma\Helper\Data $dataHelper,
        \Lof\Rma\Model\RmaFactory $rmaFactory,
        \Lof\Rma\Model\ItemFactory $itemFactory,
        \Lof\Rma\Model\AttachmentFactory $AttachmentFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Lof\Rma\Api\Repository\RmaRepositoryInterface $rmaRepository,
        \Lof\Rma\Api\Repository\MessageRepositoryInterface $messageRepository,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Lof\Rma\Model\AttachmentRepository $attachmentRepository,
        \Magento\Backend\App\Action\Context $context,
        \Lof\Rma\Helper\Help $helper
    )
    {
        $this->rmaFactory            = $rmaFactory;
        $this->itemFactory           = $itemFactory;
        $this->orderFactory          = $orderFactory;
        $this->attachmentFactory     = $AttachmentFactory;
        $this->messageRepository     = $messageRepository;
        $this->rmaRepository         = $rmaRepository;
        $this->eventManager          = $eventManager;
        $this->registry              = $registry;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->attachmentRepository  = $attachmentRepository;
        $this->dataHelper            = $dataHelper;
        $this->helper                = $helper;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        if ($data = $this->getRequest()->getParams()) {
            $request_rma_id = isset($data['rma_id']) && $data['rma_id'] ? (int) $data['rma_id'] : $this->getRequest()->getParam('id');
            $rma            = $this->rmaFactory->create();
            if ($request_rma_id) {
                $rma->load($request_rma_id);
            }
            if ( ! $request_rma_id) {
                if ($this->dataHelper->validate($data)) {
                    return $resultRedirect->setPath('*/*/add',
                        ['order_id' => $data['order_id'], '_current' => true]);
                }
            }

            try {
                $user    = $this->_auth->getUser();
                $rmadata = $data;
                unset($rmadata['items']);

                if (empty($rmadata['return_address'])) {
                    unset($rmadata['return_address']);
                }
                $itemdata = $data['items'];
                foreach ($itemdata as $k => $item) {
                    if ( ! (int) $item['reason_id']) {
                        unset($item['reason_id']);
                    }
                    if ( ! (int) $item['resolution_id']) {
                        unset($item['resolution_id']);
                    }
                    if ( ! (int) $item['condition_id']) {
                        unset($item['condition_id']);
                    }
                    $itemdata[$k] = $item;
                }

                unset($rmadata['rma_id']);

                /** @var \Magento\Sales\Model\Order $parent_rma_order */
                $parent_rma_order_id = (int) $rmadata['order_id'];
                $parent_rma_order    = $this->orderFactory->create()->load((int) $rmadata['order_id']);

                $rma->setCustomerId($parent_rma_order->getCustomerId());
                $rma->setStoreId($parent_rma_order->getStoreId());

                if ( ! $rma->getUserId()) {
                    $rma->setUserId($user->getId());
                }

                $rma->addData($rmadata);
                //Delete attachment if requested
                if ($candidate = $rma->getData('return_label')) {
                    if (isset($candidate['delete']) && $candidate['delete'] == '1') {
                        $searchCriteria = $this->searchCriteriaBuilder
                            ->addFilter('item_id', $rma->getRmaId())
                            ->addFilter('item_type', 'return_label')
                            ->addFilter('name', $candidate['value']);
                        $items4Delete   = $this->attachmentRepository->getList($searchCriteria->create())->getItems();
                        foreach ($items4Delete as $i4d) {
                            $this->attachmentRepository->delete($i4d);
                        }
                    }
                }
                $rma->save();

                //Update children RMA
                $parent_rma_id  = $rma->getId();
                $child_rma_list = $rma->getListChildRma($rma->getId());
                if ($child_rma_list) { //Update list children rma
                    foreach ($child_rma_list as $_rma) {
                        $tmprmadata = $rmadata;
                        unset($tmprmadata['order_id']);
                        unset($tmprmadata['rma_id']);

                        $child_rma_order = $this->orderFactory->create()->load((int) $_rma->getOrderId());
                        $_rma->setCustomerId($child_rma_order->getCustomerId());
                        $_rma->setStoreId($child_rma_order->getStoreId());
                        if ( ! $_rma->getUserId()) {
                            $_rma->setUserId($user->getId());
                        }
                        $_rma->addData($tmprmadata);
                        $_rma->save();
                    }
                }

                $this->registry->register('current_rma', $rma);

                foreach ($itemdata as $item) {
                    if (isset($item['qty_requested']) && $item['qty_requested']) {
                        $items = $this->itemFactory->create();
                        if (isset($item['item_id']) && $item['item_id']) {
                            $items->load((int) $item['item_id']);
                        }
                        $rma_id = (isset($item['rma_id']) && $item['rma_id']) ? $item['rma_id'] : 0;
                        if ($item['order_id'] != $parent_rma_order_id) {
                            if ( ! $rma_id) {
                                $rma_id = $this->createChildRma($item, $data, $parent_rma_id, $user->getId());
                            }
                        } else {
                            $rma_id = $parent_rma_id;
                        }
                        unset($item['item_id']);
                        $items->addData($item)
                              ->setRmaId($rma_id);
                        $items->save();
                    }
                }

                if (
                    (isset($data['reply']) && $data['reply'] != '') ||
                    ( ! empty($_FILES['attachment']) && ! empty($_FILES['attachment']['name'][0]))
                ) {
                    $message = $this->messageRepository->create();
                    $message->setRmaId($rma->getId())
                            ->setText($data['reply'], false);
                    if (isset($data['internalcheck']) && $data['internalcheck'] == '1') {
                        $message->setIsVisibleInFrontend(false)
                                ->setIsCustomerNotified(false)
                                ->setUserId($user->getId());
                    } else {
                        $message->setIsVisibleInFrontend(true)
                                ->setIsCustomerNotified(true)
                                ->setUserId($user->getId());
                    }
                    $this->messageRepository->save($message);

                    $rma->setLastReplyName($user->getName())
                        ->setIsAdminRead($user instanceof \Magento\User\Model\User);
                    $this->rmaRepository->save($rma);

                    $this->eventManager->dispatch(
                        'rma_add_message_after',
                        ['rma' => $rma, 'message' => $message, 'user' => $user, 'params' => $data]
                    );
                }

                $this->eventManager->dispatch('rma_update_rma_after', ['rma' => $rma, 'user' => $user]);

                $this->messageManager->addSuccessMessage(__('RMA was successfully saved'));
                $this->backendSession->setFormData(false);
                if ((isset($data['reply']) && $data['reply'] != '') || $this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $rma->getId()]);
                }

                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                $this->backendSession->setFormData($data);
                if ($request_rma_id) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $request_rma_id]);
                } else {
                    return $resultRedirect->setPath(
                        '*/*/add',
                        ['order_id' => $this->getRequest()->getParam('order_id')]
                    );
                }
            }
        }
        $this->messageManager->addErrorMessage(__('Unable to find rma to save'));

        return $resultRedirect->setPath('*/*/');
    }

    /**
     * @param     $rma_item
     * @param     $post_data
     * @param int $parent_rma_id
     * @param int $user_id
     * @return mixed
     * @throws \Exception
     */
    protected function createChildRma($rma_item, $post_data, $parent_rma_id = 0, $user_id = 0)
    {
        $order_id = (int) $rma_item['order_id'];
        if ( ! isset($this->_childRma[$order_id])) {
            $order   = $this->orderFactory->create()->load((int) $order_id);
            $rmaData = $post_data;
            if (isset($rmaData['street2']) && $rmaData['street2'] != '') {
                $rmaData['street'] .= "\n" . $rmaData['street2'];
                unset($rmaData['street2']);
            }
            unset($rmaData['items']);
            $rma = $this->rmaFactory->create();
            if (isset($rmaData['street2']) && $rmaData['street2'] != '') {
                $rmaData['street'] .= "\n" . $rmaData['street2'];
                unset($rmaData['street2']);
            }
            $rmaData['order_id']     = $order_id;
            $rmaData['customername'] = $order->getCustomerName();
            $rmaData['email']        = $order->getCustomerEmail();
            /** @var \Magento\Sales\Model\Order $order */
            $order = $this->orderFactory->create()->load($order_id);
            $rma->setCustomerId($order->getCustomerId());
            $rma->setStoreId($order->getStoreId());
            $rma->setUserId($user_id);
            $rma->setStatusId($this->helper->getConfig($store = null, 'rma/general/default_status'));
            $rma->setParentRmaId($parent_rma_id);
            $rma->addData($rmaData);
            $rma->save();
            $this->_childRma[$order_id] = $rma;
            $this->eventManager->dispatch('rma_update_child_rma_after', ['rma' => $rma]);
        }

        return $this->_childRma[$order_id]->getId();
    }
}
