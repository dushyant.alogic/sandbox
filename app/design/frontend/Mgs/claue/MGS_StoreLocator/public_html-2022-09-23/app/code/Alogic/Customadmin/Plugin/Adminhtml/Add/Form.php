<?php
namespace Alogic\Customadmin\Plugin\Adminhtml\Add;
use Magento\Store\Model\System\Store;
use Mageplaza\BannerSlider\Model\Config\Source\Type;

class Form extends \Mageplaza\BannerSlider\Block\Adminhtml\Banner\Edit\Tab\Banner {
	/**
	 * Type options
	 *
	 * @var Type
	 */
	protected $typeOptions;

	/**
	 * @var Store
	 */
	protected $_systemStore;

	public function __construct(
		Type $typeOptions,
		Store $systemStore,
		\Magento\Framework\Registry $registry,
		array $data = []
	) {
		$this->typeOptions = $typeOptions;
		$this->_coreRegistry = $registry;
		$this->_systemStore = $systemStore;
	}
	public function beforeSetForm(\Mageplaza\BannerSlider\Block\Adminhtml\Banner\Edit\Tab\Banner $object, $form) {
		$banner = $this->_coreRegistry->registry('mpbannerslider_banner');
		$fieldset = $form->getElement('base_fieldset');
		$fieldset->addField('sortorder', 'text', [
			'name' => 'sortorder',
			'label' => __('Position'),
			'title' => __('Position'),
			'required' => false,
		]);
		$fieldset->addField('banner_type', 'select', [
			'name' => 'banner_type',
			'label' => __('Banner Type111'),
			'title' => __('Banner Type111'),
			'values' => [1 => 'Slider', 2 => 'New Arrival'],
		]);
		/** @var RendererInterface $rendererBlock */
		//$rendererBlock = $this->getLayout()->createBlock(Element::class);
		$fieldset->addField('store_ids', 'select', [
			'name' => 'store_ids',
			'label' => __('Store Views'),
			'title' => __('Store Views'),
			'required' => true,
			'values' => $this->_systemStore->getStoreValuesForForm(false, true),
		]);
		/*$typeBanner = $fieldset->addField('type', 'select', [
			'name' => 'type',
			'label' => __('Type'),
			'title' => __('Type'),
			'values' => $this->typeOptions->toOptionArray(),
		]);*/
		$form->addValues($banner->getData());
		return [$form];
	}
}