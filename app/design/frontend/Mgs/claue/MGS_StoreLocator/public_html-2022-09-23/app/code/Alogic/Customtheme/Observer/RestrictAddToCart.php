<?php

namespace Alogic\Customtheme\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Message\ManagerInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Store\Model\StoreManagerInterface;

class RestrictAddToCart implements ObserverInterface
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * RestrictAddToCart constructor.
     *
     * @param ProductRepository $productRepository
     * @param ManagerInterface $messageManager
     * @param CheckoutSession $checkoutSession
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ProductRepository $productRepository,
        ManagerInterface $messageManager,
        CheckoutSession $checkoutSession,
        StoreManagerInterface $storeManager
    ) {
        $this->productRepository = $productRepository;
        $this->messageManager = $messageManager;
        $this->checkoutSession = $checkoutSession;
        $this->storeManager = $storeManager;
    }

    public function execute(EventObserver $observer)
    {
      
        try {
            $productId = $observer->getRequest()->getParam('product');
            

            $storeId = $this->storeManager->getStore()->getId();
            /** @var \Magento\Catalog\Model\Product $product */
            $product = $this->productRepository->getById($productId, false, $storeId);
            /** @var \Magento\Quote\Model\Quote\Item $item */
            $item = $this->checkoutSession->getQuote()->getItemByProduct($product);
            //$logger->info('qty '. $item->getQty());
            if ($item) {
                
                $itemId = $item->getId();
              //  $this->messageManager->addErrorMessage($item->getName());
            }

        } catch (\Exception $e) {
            //$this->messageManager->addErrorMessage($e->getMessage());
        }

        //set false if you not want to add product to cart
        //$observer->getRequest()->setParam('product', false);
    }
}
