<?php

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
	ComponentRegistrar::MODULE,
	'Alogic_CustomShipping',
	__DIR__
);
