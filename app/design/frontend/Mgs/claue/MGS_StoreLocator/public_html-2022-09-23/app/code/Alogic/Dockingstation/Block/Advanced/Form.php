<?php
namespace Alogic\Dockingstation\Block\Advanced;
use Magento\CatalogSearch\Helper\Data as CatalogSearchHelper;
use Magento\CatalogSearch\Model\Advanced;
use Magento\Catalog\Model\Product\Attribute\Repository;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute;
use Magento\Directory\Model\CurrencyFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class Main
 */
class Form extends \Magento\CatalogSearch\Block\Advanced\Form {
	protected $attribute;
	protected $attributeRepository;
/**
 * Currency factory
 *
 * @var CurrencyFactory
 */
	protected $_currencyFactory;

	/**
	 * Catalog search advanced
	 *
	 * @var Advanced
	 */
	protected $_catalogSearchAdvanced;

	/**
	 * @param Context $context
	 * @param Advanced $catalogSearchAdvanced
	 * @param CurrencyFactory $currencyFactory
	 * @param array $data
	 * @param CatalogSearchHelper|null $catalogSearchHelper
	 */
	public function __construct(
		Context $context,
		Advanced $catalogSearchAdvanced,
		CurrencyFactory $currencyFactory,
		array $data = [],
		Attribute $attribute,
		Repository $attributeRepository,
		? CatalogSearchHelper $catalogSearchHelper = null
	) {
		$this->_catalogSearchAdvanced = $catalogSearchAdvanced;
		$this->_currencyFactory = $currencyFactory;
		$this->attributeRepository = $attributeRepository;
		$data['catalogSearchHelper'] = $catalogSearchHelper ??
		ObjectManager::getInstance()->get(CatalogSearchHelper::class);
		parent::__construct($context, $catalogSearchAdvanced, $currencyFactory, $data, $catalogSearchHelper);
	}

	public function getAttributeOption($attributeCode) {

		$options = $this->attributeRepository->get($attributeCode)->getOptions();
		return $options;
	}

}

