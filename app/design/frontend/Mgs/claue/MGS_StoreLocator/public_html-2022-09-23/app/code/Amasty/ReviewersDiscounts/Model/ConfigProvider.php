<?php

declare(strict_types=1);

namespace Amasty\ReviewersDiscounts\Model;

use Amasty\Base\Model\ConfigProviderAbstract;

class ConfigProvider extends ConfigProviderAbstract
{
    const DISCOUNT_RULES_PATH = 'amasty_advancedreview/discount_rules/enabled';

    /**
     * @var string
     */
    protected $pathPrefix = '';

    /**
     * @return bool
     */
    public function getIsRulesEnabled()
    {
        return $this->isSetGlobalFlag(self::DISCOUNT_RULES_PATH);
    }
}
