<?php
/**
 * Copyright © 2016 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace MageWorx\Downloads\Controller\Download;

use MageWorx\Downloads\Helper\Download as DownloadHelper;
use MageWorx\Downloads\Model\Attachment;
use Magento\Framework\App\ResponseInterface;

class Link extends \MageWorx\Downloads\Controller\Download
{
    /**
     * Download link action
     *
     * @return void|ResponseInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id', 0);

        $attachment = $this->_objectManager->create(
            'MageWorx\Downloads\Model\Attachment'
        )->load(
            $id
        );

        if (!$attachment->getId() || !$attachment->getContentType()) {
            $this->messageManager->addNotice(__("We can't find the link or file you requested."));
            return $this->_redirect($this->getHelperStoreUrl()->getStoreBaseUrl());
        }

        if ($this->isDownloadsEnable($attachment)) {
            $resource     = '';
            $resourceType = '';
            try {
                if ($attachment->isFileContent()) {
                    $resource = $this->_objectManager->get(
                        'Magento\Downloadable\Helper\File'
                    )->getFilePath(
                        $this->_getLink()->getBasePath(),
                        $attachment->getFilename()
                    );
                    $resourceType = DownloadHelper::LINK_TYPE_FILE;
                    $this->_processDownload($resource, $resourceType);
                    $attachment->setDownloads($attachment->getDownloads() + 1);
                    $attachment->save();
                    exit(0);
                } elseif ($attachment->isUrlContent()) {
                    $attachment->setDownloads($attachment->getDownloads() + 1);
                    $attachment->save();

                    if (strpos($attachment->getUrl(), '://') === false) {
                        $url = $this->getHelperStoreUrl()->getUrl(ltrim($attachment->getUrl(), '/'));
                    } else {
                        $url = $attachment->getUrl();
                    }
                    return $this->_redirect($url);
                }
            } catch (\Exception $e) {
                $this->messageManager->addError(__('Something went wrong while getting the requested content.'));
            }
        } elseif ($attachment->getDownloadsLimit() && $attachment->getDownloadsLeft() == 0) {
            $this->messageManager->addNotice(__('The link is not available.'));
        } else {
            $this->messageManager->addError(__('Something went wrong while getting the requested content.'));
        }
        return $this->_redirect($this->getHelperStoreUrl()->getStoreBaseUrl());
    }

    /**
     * Return customer session object
     *
     * @return \Magento\Customer\Model\Session
     */
    protected function _getCustomerSession()
    {
        return $this->_objectManager->get('Magento\Customer\Model\Session');
    }

    /**
     * Return helper object
     *
     * @return \MageWorx\Downloads\Helper\StoreUrl
     */
    protected function getHelperStoreUrl()
    {
        return $this->_objectManager->get('MageWorx\Downloads\Helper\StoreUrl');
    }

    /**
     * Check if downloads enable
     *
     * @param \MageWorx\Downloads\Model\Attachment
     * @return boolean
     */
    protected function isDownloadsEnable($attachment)
    {
        if ($attachment->getIsActive() == Attachment::STATUS_ENABLED
            && (!$attachment->getDownloadsLimit() || $attachment->getDownloadsLeft() > 0)
            && in_array($this->_getCustomerSession()->getCustomerGroupId(), $attachment->getCustomerGroupIds())
        ) {
            return true;
        }
        return false;
    }
}
