<?php
/**
 * @category   Alogic
 * @package    Alogic_Productregistration
 * @author     dushyantjoshia@gmail.com
 * @copyright  This file was generated by using Module Creator(http://code.vky.co.in/magento-2-module-creator/) provided by VKY <viky.031290@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Alogic\Productregistration\Controller\Index;

use Alogic\Productregistration\Model\ProductregistrationFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Area;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\Image\AdapterFactory;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Store\Model\App\Emulation;
use \Twilio\Rest\Client as TwilioClient;

class Save extends \Magento\Framework\App\Action\Action {
	/**
	 * @var Productregistration
	 */
	protected $_productregistration;
	protected $uploaderFactory;
	protected $adapterFactory;
	protected $timezone;
	protected $filesystem;
	protected $_appState;
	protected $transportBuilder;
	protected $_storeManager;
/**
 * @var Emulation
 */
	protected $_appEmulation;
	public function __construct(
		Context $context,
		ProductregistrationFactory $productregistration,
		UploaderFactory $uploaderFactory,
		AdapterFactory $adapterFactory,
		TimezoneInterface $timezone,
		Emulation $appEmulation,
		TransportBuilder $transportBuilder,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\App\State $_appState,
		Filesystem $filesystem
	) {
		$this->_productregistration = $productregistration;
		$this->uploaderFactory = $uploaderFactory;
		$this->adapterFactory = $adapterFactory;
		$this->filesystem = $filesystem;
		$this->timezone = $timezone;
		$this->_appEmulation = $appEmulation;
		$this->_appState = $_appState;
		$this->transportBuilder = $transportBuilder;
		$this->_storeManager = $storeManager;
		parent::__construct($context);
	}
	public function getStore() {
		return $this->_storeManager->getStore();
	}
	public function sendOTP($postData) {

// Your Account SID and Auth Token from twilio.com/console
		$account_sid = 'AC8650459c1549876a9c23d22c4551670a';
		$auth_token = '24918d441c3d6a8cbaa5bf37c39d1567';
		$serviceId = 'VAb3b754073f26c513a29412105ea08dd6';
// In production, these should be environment variables. E.g.:
		// $auth_token = $_ENV["TWILIO_AUTH_TOKEN"]

// A Twilio number you own with SMS capabilities
		$twilio_number = "+12184234227";
		//echo "<pre>";
		//print_r($postData);

		$client = new TwilioClient($account_sid, $auth_token);

		/*$verification = $client->verify->v2->services($serviceId)
			->verifications
			->create("+918879301376", "sms");*/

		$verification_check = $client->verify->v2->services($serviceId)
			->verificationChecks
			->create("804212", // code
				["to" => "+918879301376"]
			);

		if ($verification_check->status) {
			echo $verification_check->status;
			die();
		}
		/*$client->messages->create(
			// Where to send a text message (your cell phone?)
			$postData['phone'],
			array(
				'from' => $twilio_number,
				'body' => 'ping me when you receive this message, Dushyant!',
			)
		);*/

	}
	public function setPhoneNumber(string $phoneNumber): void{
		$sanitizedPhoneNumber = $this->sanitizePhoneNumber($phoneNumber);
		$this->validatePhoneNumber($sanitizedPhoneNumber);
		$this->phoneNumber = $sanitizedPhoneNumber;
	}

	private function sanitizePhoneNumber(string $phoneNumber) {
		// remove + sign if provided in string
		return str_replace('+', '', $phoneNumber);
	}
	private function validatePhoneNumber(string $phoneNumber) {
		if (!preg_match('/[1-9]\d{1,14}$/', $phoneNumber)) {
			throw new \InvalidArgumentException(
				'Please provide phone number in E164 format without the \'+\' symbol'
			);
		}
	}

	public function execute() {
		$data = $this->getRequest()->getParams();
		$singleData = [];
		$emailData = [];
		$productregistration = $this->_productregistration->create();
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$imageFile = $objectManager->create('Alogic\Productregistration\Helper\Data')->uploadFile('file', $productregistration);
		if (!$imageFile->getFile()) {
			throw new \Magento\Framework\Exception\LocalizedException(__('The wrong file is specified.'));
		}
		$singleData['store'] = $this->getStore()->getCode();
		$singleData['file'] = $imageFile->getFile();
		$singleData['confirm'] = isset($data['confirm']) ? 1 : 0;
		$singleData['name'] = $data['firstname'] . ' ' . $data['lastname'];
		$singleData['firstname'] = $data['firstname'];
		$singleData['lastname'] = $data['lastname'];
		$singleData['email'] = $data['email'];
		$singleData['phone'] = $data['phone'];
		$singleData['form_key'] = $data['form_key'];
		$dateTimeZone = $this->timezone->date(new \DateTime($data['date_purchase']))->format('Y/m/d H:i:s');
		$singleData['date_purchase'] = $dateTimeZone;
		$singleData['invoice_no'] = $data['invoice_no'];

		//$result_data = array_filter($data['product_name']);
		//for ($i = 1; $i < count($data['product']) + 1; $i++) {
		$nData = [];
		foreach ($data['product'] as $pkey => $product) {
			foreach ($product as $key => $value) {
				if ($key == 'product_name') {
					if ($value == '') {
						unset($data['product'][$pkey]);

					}

				}

			}

		}

		foreach ($data['product'] as $product) {

			$j = 0;
			foreach ($product as $key => $value) {

				if ($key == 'product_name') {
					if ($value == '') {
						continue;
					}

				}
				if ($key == 'serialnumber') {
					foreach ($value as $val) {
						if ($j == 0) {
							$serKey = 'serialnumber';
						} else {
							$serKey = 'serialnumber' . ($j + 1);
						}
						if (isset($val[$j])) {
							$singleData[$serKey] = $val;
						}

						$j++;
					}
				} else {

					$singleData[$key] = $value;
				}

			}

			$productregistration->setData($singleData);

			if ($productregistration->save()) {
				$this->messageManager->addSuccessMessage(__('You have successfully registered your product. Please check your email for confirmation.'));
				$emailData[] = $singleData;

			} else {
				$this->messageManager->addErrorMessage(__('Data was not saved.'));
			}
		}
		$this->notifyCustomer($emailData);
		//}

		$resultRedirect = $this->resultRedirectFactory->create();
		$resultRedirect->setPath('productregistration');
		return $resultRedirect;
	}

	public function notifyCustomer($emailVars) {
		//echo "<pre>";
		//print_r($emailVars);

		$storeId = $this->getStore()->getId(); //34;
		$this->_appEmulation->startEnvironmentEmulation($storeId);
		$productHtml = '';
		$personal = [];
		$i = 0;
		foreach ($emailVars as $products) {
			$i++;
			$productHtml .= '<table width="100%">';
			$productHtml .= "<tr>";

			$productHtml .= "<td style='text-align:left;font-weight: bold;background: #f0ece8;padding: 6px;border-radius: 3px;' colspan='2'> Product $i : </td></tr>";
			foreach ($products as $key => $product) {

				if (in_array($key, ['lastname', 'file', 'form_key', 'confirm', 'store'])) {
					continue;
				}
				if (in_array($key, ['firstname', 'name', 'phone', 'email', 'invoice_no', 'date_purchase'])) {

					$personal[$key] = $product;

				} else {
					$key = ucwords(str_replace('_', ' ', $key));
					if ($key == 'Qty') {
						$key = 'Quantity';
					}
					if (stripos($key, 'serialnumber') !== false) {
						if ($key == 'Serialnumber') {
							$key = 'Serial Number';
						} elseif ($key == 'Serialnumber2') {
							$key = 'Serial Number 2';
						} elseif ($key == 'Serialnumber3') {
							$key = 'Serial Number 3';
						} elseif ($key == 'Serialnumber4') {
							$key = 'Serial Number 4';
						} elseif ($key == 'Serialnumber5') {
							$key = 'Serial Number 5';
						} elseif ($key == 'Serialnumber6') {
							$key = 'Serial Number 6';
						} elseif ($key == 'Serialnumber7') {
							$key = 'Serial Number 7';
						}
					}
					$productHtml .= "<tr>";

					$productHtml .= "<td style='text-align:left;font-weight: bold;' width='25%'> $key: </td> <td style='text-align:left;'>$product</td> ";
					$productHtml .= "</tr>";
				}

			}
			$productHtml .= "<tr>";
			$productHtml .= "<td style=''>&nbsp;</td> <td style='text-align:left;'>&nbsp;</td> ";
			$productHtml .= "</tr>";
			$productHtml .= "</table>";

		}

		// this is an example and you can change template id,fromEmail,toEmail,etc as per your need.
		$templateId = 52; // template id
		$fromEmail = 'support@alogic.co'; // sender Email id
		$fromName = 'Support'; // sender Name
		$toEmail1 = 'dushyant.joshi@alogic.co'; //'product.registration@alogic.co'; //; // receiver email id
		$toEmail2 = $personal['email'];
		try {
			// template variables pass here
			$templateVars = [
				'firstname' => $personal['firstname'],
				'name' => $personal['name'],
				'email' => $personal['email'],
				'phone' => $personal['phone'],
				'invoice_no' => $personal['invoice_no'],
				'date_purchase' => $personal['date_purchase'],
				'html' => $productHtml,
			];

			$from = ['email' => $fromEmail, 'name' => $fromName];
			//$this->inlineTranslation->suspend();

			$storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
			$templateOptions = [
				'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
				'store' => $storeId,
			];
			$mediaDirectory = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
			$destinationPath = $mediaDirectory->getAbsolutePath('alogic/productregistration');
			//echo $destinationPath . $emailVars[0]['file'];

			$transport = $this->transportBuilder->setTemplateIdentifier($templateId, $storeScope)
				->setTemplateOptions($templateOptions)
				->setTemplateVars($templateVars)
				->setFrom($from)
				->addTo($toEmail1)
				->addTo($toEmail2)
				->addAttachment(file_get_contents($destinationPath . $emailVars[0]['file']), basename($destinationPath . $emailVars[0]['file']), mime_content_type($destinationPath . $emailVars[0]['file']))
				->getTransport();
			$transport->sendMessage();
			//$this->inlineTranslation->resume();
		} catch (\Exception $e) {
			echo $e->getMessage();
			die();
			//$this->_logger->info($e->getMessage());
		}

		//$this->logger->info('notifying =>' . $item->getEmail());
	}

}
