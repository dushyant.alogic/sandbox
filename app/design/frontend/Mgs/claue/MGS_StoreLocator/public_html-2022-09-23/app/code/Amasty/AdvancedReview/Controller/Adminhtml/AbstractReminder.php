<?php

namespace Amasty\AdvancedReview\Controller\Adminhtml;

use Magento\Backend\App\Action;

abstract class AbstractReminder extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Amasty_AdvancedReview::reminder';
}
