

require(["jquery"], function ($) {
  $(document).ready(function ($) {
    $('.js-spec-inner-heading').click(function () {
      let $this = $(this);

      if ($this.next().hasClass('selected')) {
        $this.removeClass('active');
        $this.next().removeClass('selected');
        $this.next().slideUp(1000);
      } else {
        // $this.parent().parent().find('.spec-inner .spec-inner-data').removeClass('selected');
        // $this.parent().parent().find('.spec-inner .spec-inner-data').slideUp(500);
        $this.addClass('active');
        $this.next().addClass('selected');
        $this.next().slideDown(1000);
      }

    });
    let windowView = window.outerWidth;
      if (windowView < 768) {
        $('#tab-label-reviews').click(function () {

          let $this = $(this);

          if ($this.next().hasClass('selected')) {

            $this.removeClass('nactive');
            $this.addClass('pactive');
            console.log("remove2", $this.attr('class'));
            $this.next().removeClass('selected');
            $this.next().slideUp(500);
          } else {
            // $this.parent().parent().find('.spec-inner .spec-inner-data').removeClass('selected');
            // $this.parent().parent().find('.spec-inner .spec-inner-data').slideUp(500);
            $this.addClass('nactive');
            $this.removeClass('pactive');
            $this.next().addClass('selected');
            $this.next().slideDown(500);
          }

        });
        $('#tab-label-questions').click(function () {

          let $this = $(this);

          if ($this.next().hasClass('selected')) {

            $this.removeClass('nactive');
            $this.addClass('pactive');

            $this.next().removeClass('selected');
            $this.next().slideUp(500);
          } else {
            // $this.parent().parent().find('.spec-inner .spec-inner-data').removeClass('selected');
            // $this.parent().parent().find('.spec-inner .spec-inner-data').slideUp(500);
            $this.addClass('nactive');
            $this.removeClass('pactive');
            $this.next().addClass('selected');
            $this.next().slideDown(500);
          }
            $('#description').show();
        });
        $('#tab-label-specification').click(function () {

          let $this = $(this);

          if ($this.next().hasClass('selected')) {

            $this.removeClass('nactive');
            $this.addClass('pactive');

            $this.next().removeClass('selected');
            $this.next().slideUp(500);
          } else {
            // $this.parent().parent().find('.spec-inner .spec-inner-data').removeClass('selected');
            // $this.parent().parent().find('.spec-inner .spec-inner-data').slideUp(500);
            $this.addClass('nactive');
            $this.removeClass('pactive');
            $this.next().addClass('selected');
            $this.next().slideDown(500);
          }
            $('#description').show();
        });
      }
  });
});

