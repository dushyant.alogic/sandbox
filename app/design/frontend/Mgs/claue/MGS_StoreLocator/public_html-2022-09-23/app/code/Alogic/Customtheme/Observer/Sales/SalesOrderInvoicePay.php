<?php
namespace Alogic\Customtheme\Observer\Sales;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;

class SalesOrderInvoicePay implements ObserverInterface {
	protected $zendClient;
	protected $configWriter;
/**
 * @var \Psr\Log\LoggerInterface
 */
	protected $logger;
/**
 * @var StoreManagerInterface
 */
	private $storeManager;
	public function __construct(
		\Zend\Http\Client $zendClient,
		StoreManagerInterface $storeManager,
		LoggerInterface $logger,
		WriterInterface $configWriter

	) {
		$this->configWriter = $configWriter;
		$this->logger = $logger;
		$this->zendClient = $zendClient;
		$this->storeManager = $storeManager;

	}

/**
 * @param EventObserver $observer
 * @return $this
 */
	public function execute(EventObserver $observer) {
		$shipment = $observer->getEvent()->getShipment();
		/** @var \Magento\Sales\Model\Order $order */
		$order = $shipment->getOrder();
		$invoiceList = $order->getInvoiceCollection();

		$store_code = $this->getStoreCodeById($order->getStoreId());

		//$this->logger->info(print_r($invoiceList, true));
		//if (count($invoiceList->getItems()) > 1) {
		foreach ($invoiceList as $invoice) {
			$invoiceId = $invoice->getEntityId();
			$this->logger->info($invoiceId);
		}
		$url = 'https://sandbox.alogic.co/rest/' . $store_code . '/V1/invoices/' . $invoiceId . '/emails';

		$this->logger->info($url);
		//}

		try
		{
			$this->zendClient->reset();
			$this->zendClient->setUri($url);
			$this->zendClient->setMethod(\Zend\Http\Request::METHOD_POST);
			$this->zendClient->setHeaders([
				'Content-Type' => 'application/json',
				'Accept' => 'application/json',
				'Authorization' => 'Bearer 4l2pf8yldjfm32skpghl1nk3m2oilsyv',
			]);
			/*$this->zendClient->setParameterPost([
				'yourparameter1' => 'yourvalue1',
			]);*/
			//$this->logger->info("Sent");
			//$this->enableDisableEmail(1);
			$this->zendClient->send();
			$response = $this->zendClient->getResponse();
			//$this->enableDisableEmail(0);
			$this->logger->info(print_r($response->getBody(), true));
		} catch (\Zend\Http\Exception\RuntimeException $runtimeException) {
			echo $runtimeException->getMessage();
		}

	}
	public function enableDisableEmail($value) {
		//for all websites
		$websiteId = $this->storeManager->getStore()->getWebsiteId();
		$scope = "default"; //stores, default, websites and by scope codes.

		$this->configWriter->save('sales_email/invoice/enabled', $value, $scope,
			$websiteId);

		return $this;
	}
	/**
	 * Get Store code by id
	 *
	 * @param int $id
	 *
	 * @return string|null
	 */
	public function getStoreCodeById(int $id) {
		try {
			$storeData = $this->storeManager->getStore($id);
			$storeCode = (string) $storeData->getCode();
		} catch (LocalizedException $localizedException) {
			$storeCode = null;
			//$this->logger->error($localizedException->getMessage());
		}
		return $storeCode;
	}
}
