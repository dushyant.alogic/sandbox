<?php

namespace Amasty\AdvancedReview\Model\ResourceModel\Reminder;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init(
            \Amasty\AdvancedReview\Model\Reminder::class,
            \Amasty\AdvancedReview\Model\ResourceModel\Reminder::class
        );
        $this->_idFieldName = 'entity_id';
    }
}
