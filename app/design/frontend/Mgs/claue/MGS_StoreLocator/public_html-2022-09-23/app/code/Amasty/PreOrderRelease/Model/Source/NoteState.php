<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2021 Amasty (https://www.amasty.com)
 * @package Amasty_PreOrderRelease
 */


declare(strict_types=1);

namespace Amasty\PreOrderRelease\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

class NoteState implements OptionSourceInterface
{
    const HIDDEN = 0;
    const REPLACED_WITH_DEFAULT = 1;

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => self::HIDDEN,
                'label' => __('Hidden')
            ],
            [
                'value' => self::REPLACED_WITH_DEFAULT,
                'label' => __('Replaced with default note')
            ]
        ];
    }
}
