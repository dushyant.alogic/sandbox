<?php
/**
 * @category   Newsroom
 * @package    Newsroom_Presskit
 * @author     dushyantjoshia@gmail.com
 * @copyright  This file was generated by using Module Creator(http://code.vky.co.in/magento-2-module-creator/) provided by VKY <viky.031290@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Newsroom\Presskit\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Newsroom\Presskit\Block\PresskitView;
use Newsroom\Presskit\Model\ResourceModel\Image\CollectionFactory;

class Download extends \Magento\Framework\App\Action\Action {
	protected $_presskitview;
	public $filesystem;

	public $file;

	private $directory;

	public $driver;
	private $collectionFactory;
	public $fileFactory;
	public function __construct(
		Context $context,
		\Magento\Framework\Filesystem $filesystem,
		\Magento\Framework\Filesystem\Io\File $file,
		\Magento\Framework\Filesystem\Driver\File $driver,
		CollectionFactory $collectionFactory,
		\Magento\Framework\App\Response\Http\FileFactory $fileFactory,
		PresskitView $presskitview
	) {
		$this->_presskitview = $presskitview;
		$this->filesystem = $filesystem;
		$this->file = $file;
		$this->driver = $driver;
		$this->fileFactory = $fileFactory;
		$this->collectionFactory = $collectionFactory;
		parent::__construct($context);
	}

	public function execute() {
		/*if(!$this->_presskitview->getSingleData()){
			    		throw new NotFoundException(__('Parameter is incorrect.'));
		*/

		$presskit_id = $this->getRequest()->getParam('presskit_id');
		$this->exportPresskit($presskit_id);
		$this->_view->loadLayout();
		$this->_view->getLayout()->initMessages();
		$this->_view->renderLayout();
	}
	public function exportPresskit($presskitId) {
		$this->directory = $this->filesystem->getDirectoryWrite(DirectoryList::TMP);
		$this->directory->create();
		$mediapath = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath();
		$collection = $this->collectionFactory->create();
		$collection->addFieldToFilter('presskit_id', $presskitId);

		$fileList = [];

		foreach ($collection as $item) {

			$fileList[] = $mediapath . $item->getImages();

		}
		$packagePath = $this->createPackage($fileList);

/*		if (empty($fileList)) {
throw new LocalizedException(__('No template for export'));
}

$packagePath = $this->createPackage($fileList);
$this->removeFiles($fileList);

return $packagePath;*/
	}
	public function createPackage($fileList) {
		if (!class_exists('\ZipArchive')) {
			return;
		}

		$zipFile = $this->directory->getAbsolutePath('presskit' . time() . '.zip');
		$zip = new \ZipArchive();
		$zip->open($zipFile, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

		foreach ($fileList as $filePath) {
			$rootPath = explode('/', $filePath);
			unset($rootPath[count($rootPath) - 1]);
			$rootPath = implode('/', $rootPath);

			$relativePath = substr($filePath, strlen($rootPath) + 1);
			$zip->addFile($filePath, $relativePath);
		}

		$zip->close();
		$zipPath = $zipFile;
		if (empty($zipPath)) {
			throw new LocalizedException(__('Unable to export.'));
		}
		return $this->fileFactory->create(
			'presskit.zip',
			[
				'value' => $zipPath,
				'type' => 'filename',
				'rm' => true,
			],
			DirectoryList::TMP
		);
	}
}
