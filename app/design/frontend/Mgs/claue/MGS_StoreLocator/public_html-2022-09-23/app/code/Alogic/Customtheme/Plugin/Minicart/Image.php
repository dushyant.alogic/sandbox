<?php

namespace Alogic\Customtheme\Plugin\Minicart;

class Image {

	public function aroundGetItemData($subject, $proceed, $item) {

		$result = $proceed($item);

		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$product = $objectManager->create('Magento\Catalog\Model\Product')->load($result['product_id']);

		$helperImport = $objectManager->get('\Magento\Catalog\Helper\Image');

		if ($product->getThumbnail()) {
			$imageUrl = $helperImport->init($product, 'product_thumbnail_image')
				->setImageFile($product->getThumbnail()) // image,small_image,thumbnail
				->resize(78)
				->getUrl();
			$result['product_image']['src'] = $imageUrl;
		} else {
			$imageUrl = $helperImport->init($product, 'product_base_image')
				->setImageFile($product->getImage()) // image,small_image,thumbnail
				->resize(78)
				->getUrl();
			$result['product_image']['src'] = $imageUrl;
		}
		return $result;
	}
}
