<?php
/**
 * LandOfCoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://www.landofcoder.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   LandOfCoder
 * @package    Lof_Rma
 * @copyright  Copyright (c) 2020 Landofcoder (http://www.LandOfCoder.com/)
 * @license    http://www.LandOfCoder.com/LICENSE-1.0.html
 */

namespace Lof\Rma\Model;

use Lof\Rma\Api\Repository\CustomerRmaRepositoryInterface;
use Lof\Rma\Api\Data\RmaSearchResultsInterfaceFactory;
use Lof\Rma\Api\Data\RmaInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Lof\Rma\Model\ResourceModel\Rma as ResourceRma;
use Lof\Rma\Model\ResourceModel\Rma\CollectionFactory as RmaCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

class CustomerRmaRepository implements CustomerRmaRepositoryInterface
{

    protected $resource;

    protected $rmaFactory;

    protected $rmaCollectionFactory;

    protected $searchResultsFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $dataRmaFactory;

    private $storeManager;


    /**
     * @param ResourceRma $resource
     * @param RmaFactory $rmaFactory
     * @param RmaInterfaceFactory $dataRmaFactory
     * @param RmaCollectionFactory $rmaCollectionFactory
     * @param RmaSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ResourceRma $resource,
        RmaFactory $rmaFactory,
        RmaInterfaceFactory $dataRmaFactory,
        RmaCollectionFactory $rmaCollectionFactory,
        RmaSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->rmaFactory = $rmaFactory;
        $this->rmaCollectionFactory = $rmaCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataRmaFactory = $dataRmaFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function save($customerId, \Lof\Rma\Api\Data\RmaInterface $rma)
    {
        if(!$customerId){
            throw new NoSuchEntityException(__('You should login with your account.'));
        }
        try {
            $rmaCustomerId = $rma->getCustomerId();
            if($customerId == $rmaCustomerId){
                $rma->getResource()->save($rma);
            }else {
                throw new CouldNotSaveException(__(
                    'Could not save the rma, because wrong Customer ID'
                ));
            }
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the rma: %1',
                $exception->getMessage()
            ));
        }
        return $rma;
    }

    /**
     * {@inheritdoc}
     */
    public function saveBundle($customerId, \Lof\Rma\Api\Data\RmaInterface $rma)
    {
        if(!$customerId){
            throw new NoSuchEntityException(__('You should login with your account.'));
        }
        try {
            $rmaCustomerId = $rma->getCustomerId();
            if($customerId == $rmaCustomerId){
                $rma->getResource()->save($rma);
            }else {
                throw new CouldNotSaveException(__(
                    'Could not save the bundle rma, because wrong Customer ID'
                ));
            }
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the bundle rma: %1',
                $exception->getMessage()
            ));
        }
        return $rma;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($customerId, $rmaId)
    {
        if(!$customerId){
            throw new NoSuchEntityException(__('You should login with your account.'));
        }
        $rma = $this->rmaFactory->create();
        $rma->getResource()->load($rma, $rmaId);
        
        if (!$rma->getId()) {
            throw new NoSuchEntityException(__('rma with id "%1" does not exist.', $rmaId));
        }
        $rmaCustomerId = $rma->getCustomerId();
        if($customerId != $rmaCustomerId){
            throw new NoSuchEntityException(__('rma with id "%1" does not exist for this Customer.', $rmaId));
        }
        return $rma;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        $customerId, 
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        if(!$customerId){
            throw new NoSuchEntityException(__('You should login with your account.'));
        }
        $collection = $this->rmaCollectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'store_id') {
                    $collection->addStoreFilter($filter->getValue(), false);
                    continue;
                }
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        //Add filter for this customer ID
        $collection->addFieldToFilter("main_table.customer_id", $customerId);

        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setItems($collection->getItems());
        return $searchResults;
    }
}
