<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_BannerSlider
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Alogic\Customadmin\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Type
 * @package Alogic\Customadmin\Model\Config\Source
 */
class Bannertype implements ArrayInterface {
	const SLIDER = '1';
	const NEW_ARRIVAL = '2';
	/**
	 * to option array
	 *
	 * @return array
	 */
	public function toOptionArray() {
		$options = [
			[
				'value' => self::SLIDER,
				'label' => __('Slider'),
			],
			[
				'value' => self::NEW_ARRIVAL,
				'label' => __('New Arrival'),
			],
		];

		return $options;
	}
}
