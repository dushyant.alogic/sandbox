<?php
namespace Alogic\Customtheme\Plugin\Product\ProductList;
class Toolbar {
	public function aroundSetCollection(
		\Magento\Catalog\Block\Product\ProductList\Toolbar $subject,
		\Closure $proceed,
		$collection
	) {
		$currentOrder = $subject->getCurrentOrder();
		$direction = $subject->getCurrentDirection();
		if ($currentOrder == "newest_product") {

			$collection->getSelect()->order('created_at ' . $direction);
		} elseif ($currentOrder == 'high_to_low') {
			$collection->setOrder('price', 'desc');
		} elseif ($currentOrder == 'new_old') {
			$collection->setOrder('created_at', 'DESC');
		} elseif ($currentOrder == 'price_asc') {
			$collection->setOrder('price', 'asc');
		} elseif ($currentOrder == 'price_desc') {
			$collection->setOrder('price', 'desc');
		} else {
			//$collection->setOrder('price', 'asc');
		}
		return $proceed($collection);
	}
}
