<?php
/**
 * LandOfCoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://www.landofcoder.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category   LandOfCoder
 * @package    Lof_Rma
 * @copyright  Copyright (c) 2020 Landofcoder (http://www.LandOfCoder.com/)
 * @license    http://www.LandOfCoder.com/LICENSE-1.0.html
 */

namespace Lof\Rma\Controller\Adminhtml\Rma;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
use Lof\Rma\Controller\Adminhtml\Rma;

class MassDelete extends Rma {
    /**
     * Authorization level of a basic admin session
     */
    const ADMIN_RESOURCE = 'Lof_Rma::rma_rma_delete';

    /**
     * MassDelete constructor.
     *
     * @param \Lof\Rma\Model\RmaFactory           $rmaFactory
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Lof\Rma\Model\RmaFactory $rmaFactory,
        \Magento\Backend\App\Action\Context $context
    )
    {
        $this->rmaFactory = $rmaFactory;

        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        $ids = $this->getRequest()->getParam('rma_id');
        if ( ! is_array($ids)) {
            $this->messageManager->addError(__('Please select rma(s)'));
        } else {
            try {
                foreach ($ids as $id) {
                    /** @var \Lof\Rma\Model\Rma $rma */
                    $rma            = $this->rmaFactory->create()
                                                       ->setIsMassDelete(true)
                                                       ->load($id);
                    $child_rma_list = $rma->getListChildRma($id);
                    $rma->delete();

                    if ($child_rma_list) {
                        foreach ($child_rma_list as $_rma) {
                            $_rma->delete();
                        }
                    }
                }
                $this->messageManager->addSuccess(
                    __('A total of %1 record(s) have been deleted.', count($ids))
                );
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }

        return $resultRedirect->setPath('*/*/index');
    }
}
