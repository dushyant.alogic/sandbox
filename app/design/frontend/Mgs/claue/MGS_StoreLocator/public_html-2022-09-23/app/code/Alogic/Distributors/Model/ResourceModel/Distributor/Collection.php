<?php

namespace Alogic\Distributors\Model\ResourceModel\Distributor;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {
	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct() {
		$this->_init('Alogic\Distributors\Model\Distributor', 'Alogic\Distributors\Model\ResourceModel\Distributor');
	}
}
