<?php
/**
 * LandOfCoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://www.landofcoder.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   LandOfCoder
 * @package    Lof_Rma
 * @copyright  Copyright (c) 2020 Landofcoder (http://www.LandOfCoder.com/)
 * @license    http://www.LandOfCoder.com/LICENSE-1.0.html
 */
namespace Lof\Rma\Block\Adminhtml;


use Magento\Framework\View\Element\Template;

/**
 * Class FacebookSupport
 * @package Lof\FaceSupportLive\Block\Chatbox
 */
class ReportReason extends Template implements \Magento\Widget\Block\BlockInterface
{
    protected $_itemCollectionFactory;
    /**
     * FacebookSupport constructor.
     * @param Template\Context $context
     * @param \Lof\Rma\Model\ResourceModel\Item\Collection $itemCollectionFactory
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Lof\Rma\Model\ResourceModel\Item\Collection $itemCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $my_template = "report/rma/reason/grid/chart.phtml";
        if($this->hasData("template") && $this->getData("template")) {
            $my_template = $this->getData("template");
        } elseif(isset($data['template']) && $data['template']){
            $my_template = $data['template'];
        }
        if($my_template) {
            $this->setTemplate($my_template);
        }
        $this->_itemCollectionFactory = $itemCollectionFactory;
    }

    public function getItemCollection(){
        return $this->_itemCollectionFactory;
    }

    public function filterRmaItemCollection($name = '', $field = 'created_at', $startdate, $endate){
        if(!isset($this->_rmaItemCollection)){
            $resourceCollection =  $this->getItemCollection()->addFieldToFilter(
                        'main_table.reason_id',
                        array('notnull' => true)
                    )
                ->setDateColumnFilter($field)
                ->addDateFromFilter($startdate)
                ->addDateToFilter( $endate)
                ->_getReasonSelectedColumns($name);
            if($name){
                $resourceCollection->addFieldToFilter('product.value', array('like' => '%' . $name . '%'));
            }
            $this->_rmaItemCollection =  $resourceCollection;
        }
        return $this->_rmaItemCollection;
    }

}
   
     

   

    

